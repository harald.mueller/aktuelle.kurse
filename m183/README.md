# M183 - Applikationssicherheit implementieren

Inhaltsverzeichnis

[TOC]


[**Modulidentifikation** ICT CH](https://www.modulbaukasten.ch/module/183/3/de-DE?title=Applikationssicherheit-implementieren)

[weitere TBZ Unterlagen -> https://gitlab.com/ch-tbz-it/Stud/m183/m183](https://gitlab.com/ch-tbz-it/Stud/m183/m183)

## Themenüberblick

[Themenüberblick](https://gitlab.com/ch-tbz-it/Stud/m183/m183/-/tree/main/0%20Themen%C3%BCberblick)



- [Schutzziele "CIA"](https://gitlab.com/ch-tbz-it/Stud/m183/m183/-/blob/main/0%20Themen%C3%BCberblick/Schutzziele.md)
- [Massnahmen](https://gitlab.com/ch-tbz-it/Stud/m183/m183/-/blob/main/0%20Themen%C3%BCberblick/Massnahmen.md)


## Leistungsbeurteilungen (Prüfungen)

- LB1 (40%) Vortrag OWASP (20 min [pro Team zu 2 Personen](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/EXhAQOOfnZhOjjNdW21FNt8B3tPFl2FQ67FC0F_P1XJZJw?e=MLKvVJ) )
- LB2 (60%) Auftrag Penetrationstesting als Projektarbeit zu 2 Personen.<br> Sie machen und zeigen eine [Sicherheitsumsetzung in einer Applikation](https://gitlab.com/ch-tbz-it/Stud/m183/lb2-applikation)


## Ablaufplan 2024/25-Q3 **AP22c** (Fr morgens)

|Tag  | Datum |Thema, Auftrag, Übung |
|---- |----   |----                  |
| 1   | 21.02 | Themeneinstieg, Schutzziele <br> 'Open Web Applicaiton Security Project' ([OWASP](https://gitlab.com/ch-tbz-it/Stud/m183/m183/-/tree/main/1%20OWASP)), [https://owasp.org](https://owasp.org), [Top 10](https://owasp.org/Top10) <br> &#9997; [LB1: Auftrag](https://gitlab.com/harald.mueller/aktuelle.kurse/-/blob/master/m183/README.md#lb1-owasp-top-ten-project-gruppenarbeit)   |
| 2   | 28.02 | LB1: OWASP Auftrag (Fortsetzung), Checkup/Beratung vom Lehrer  |
| 3   | 07.03 | LB1: OWASP Auftrag (Fortsetzung), Checkup/Beratung vom Lehrer  |
| 4   | 14.03 | **LB1**: [Vorträge Teil 1](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/EXhAQOOfnZhOjjNdW21FNt8B3tPFl2FQ67FC0F_P1XJZJw?e=MLKvVJ) <br>[Theorie Sessionhandling](https://gitlab.com/ch-tbz-it/Stud/m183/m183/-/tree/main/2%20Sessionhandling,%20Authentifizierung%20und%20Autorisierung/Sessionhandling) und individuelle Vertiefung <br> &#9997; [Auftrag Sessionhandling](https://gitlab.com/ch-tbz-it/Stud/m183/m183/-/blob/main/2%20Sessionhandling,%20Authentifizierung%20und%20Autorisierung/Sessionhandling/Auftrag.md)<br> <br> Was ist Penetrationstesting [IBM](https://www.ibm.com/de-de/topics/penetration-testing), [Wikipedia](https://de.wikipedia.org/wiki/Penetrationstest_(Informatik)) <br> <br> &#9997; **LB2: Auftrag Penetrationtesting -> Umsetzung in einer Applikation** <br> Projekt entweder selbstgewählt oder diese [Sicherheitsumsetzung in einer Applikation](https://gitlab.com/ch-tbz-it/Stud/m183/lb2-applikation)|
| 5   | 21.03 | **LB1**: [Vorträge Teil 2](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/EXhAQOOfnZhOjjNdW21FNt8B3tPFl2FQ67FC0F_P1XJZJw?e=MLKvVJ) <br>Theorie Benutzerauthentifizierung und -autorisierung <br> Weiterarbeit Penetrationtesting  <br> Check-up/Beratung für Miniprojekt/Penetrationtesting |
| 6   | 28.03 | [Repetition Kryptographie](https://gitlab.com/ch-tbz-it/Stud/m183/m183/-/blob/main/3%20Verschl%C3%BCsselung/README.md) (Symmetrische und asymmetrische Verschlüsselung, Hash-Methoden und digitale Signaturen)  <br> Weiterarbeit Penetrationtesting <br> Check-up/Beratung für Miniprojekt/Penetrationtesting |
| 7   | 04.04 | Logging & Monitoring <br> Weiterarbeit Penetrationtesting <br> Check-up/Beratung für Miniprojekt/Penetrationtesting |
| 8   | 11.04 | Weiterarbeit Penetrationtesting <br> Check-up/Beratung für Miniprojekt/Penetrationtesting  |
| -   | --    | Karfreitag      |
| -   | --    | Osterferien     |
| 9   | 09.05 | [Abgabe gemäss Terminliste](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/ERKNyvshwkpFsnNAmgLYjeQBZisucnRowYXTn8n6ojIjQQ?e=bczYfB)  |

<br>
<br>
<br>

# LB1: OWASP Top Ten Project (Gruppenarbeit)

## Rahmenbedingungen
Das Open Web Applicaiton Security Project (OWASP) ist eine weltweite non-Profit Organisation, die sich zum Ziel setzt, Qualität und Sicherheit von Software zu verbessern. Es ist das Ziel, Entwickler, Designer, Softwarearchitekten für potenzielle Schwachstellen zu sensibilisieren und aufzuzeigen, wie sich diese vermeiden lassen. Die folgenden „[OWASP Top Ten](https://owasp.org/Top10)“ stellen unter Web-Sicherheitsexperten einen aner-kannten Konsens dar, was die derzeit kritischen Lücken in Web-Anwendungen betrifft (stand 2021):
- A01:2021 – [Broken Access Control](https://owasp.org/Top10/A01_2021-Broken_Access_Control/) 
- A02:2021 – [Cryptographic Failures](https://owasp.org/Top10/A02_2021-Cryptographic_Failures/)
- A03:2021 – [Injection](https://owasp.org/Top10/A03_2021-Injection/) 
- A04:2021 – [Insecure Design](https://owasp.org/Top10/A04_2021-Insecure_Design/)
- A05:2021 – [Security Misconfiguration](https://owasp.org/Top10/A05_2021-Security_Misconfiguration/)
- A06:2021 – [Vulnerable and Outdated Components](https://owasp.org/Top10/A06_2021-Vulnerable_and_Outdated_Components/)
- A07:2021 – [Identification and Authentication Failures](https://owasp.org/Top10/A07_2021-Identification_and_Authentication_Failures/) 
- A08:2021 – [Software and Data Integrity Failures](https://owasp.org/Top10/A08_2021-Software_and_Data_Integrity_Failures/)
- A09:2021 – [Security Logging and Monitoring Failures](https://owasp.org/Top10/A09_2021-Security_Logging_and_Monitoring_Failures/) 
- A10:2021 – [Server-Side Request Forgery (SSRF)](https://owasp.org/Top10/A10_2021-Server-Side_Request_Forgery_%28SSRF%29/)

## Auftrag
1. Wählen Sie OWASP Top 10 Themen aus (Anzahl Gruppenmitglieder = Anzahl zu wählender Themen), die Sie in diesem Auftrag bearbeiten möchten (jedes Thema muss von der Klasse **mind. einmal ausgearbeitet werden!**)
2. Analysieren Sie die ausgewählten Themen mit Hilfe der OWASP-Seite https://owasp.org/Top10/ (und allfälligen weiterführenden Quellen / Internetrecherche)
3.	Erklären Sie in eigenen Worten, was sich hinter der Abkürzung CWE versteckt und wie CWE mit den OWASP Top 10 zusammenhängen.
4.	Beschreiben Sie den Unterschied der OWASP Top 10 Risk und OWASP Proactive Control (https://owasp.org/www-project-proactive-controls/)
5.	Die ausgewählten Themen werden wie folgt ausgearbeitet:
      - Beschreibung der theoretischen Hintergründe und der Bedrohung sowie mögliche Folgen 
      - Schwachstelle mit konkretem Codebeispiel vorstellen und erläutern 
      - Massnahme wie die Sicherheitslücke geschlossen werden kann, an einem konkreten Codebeispiel 
      - Abgabe von **Dokumentation und Code Beispiele** via Teams-Aufgabe

## Inhalt der Dokumentation
- Überblick 
- Erläuterungen zu Aufgabe 3 und 4
- Theoretische Hintergründe 
- Schwachstelle mit Codebeispiel 
- Massnahme mit Codebeispiel 
- Resultate, Erkenntnisse 
- Hinweise auf weitere Unterlagen, Übungen, Tutorien (inkl. **verwendeter Quellen**)

## Zeitrahmen
Als Vorbereitung stehen 7 Lektionen während der Schule zur Verfügung. Die Vorstellung soll die Problemstellung (Angriffspunkt, Auswirkung, Technologie) und Lösung (Technologie und sinnvolle Gegenmassnahmen) anhand von praktischen Beispielen (Live Demo der Codebeispiele – keine PowerPoint), aufzeigen. Die Live Demo darf pro Thema maximal 7 Min dauern und ist in Standardsprache zu halten.

## Resultat
- Vollständiger schriftlicher Theorie Teil mit praktischen Code-Beispielen.
- Live Demo anhand von Beispielen von Sicherheitslücken und geeignete OWASP Gegenmassnahmen.

## Termine, Abgabe
Die konkreten Termine und Abgabemodalitäten werden durch die Lehrperson für jede Moduldurchführung individuell festgelegt und entsprechend kommuniziert.

## Bewertung
[x_ressourcen/Bewertungsraster_OWASP.xlsx](https://gitlab.com/harald.mueller/aktuelle.kurse/-/blob/master/m183/x_ressourcen/Bewertungsraster_OWASP.xlsx)