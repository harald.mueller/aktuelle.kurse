# M141 Datenbanksystem in Betrieb nehmen

Inhaltsverzeichnis

[TOC]


[**Modulidentifikation** ICT CH](https://www.modulbaukasten.ch/module/141/3/de-DE?title=Datenbanksystem-in-betrieb-nehmen)

[weitere TBZ Unterlagen -> https://gitlab.com/ch-tbz-it/Stud/m141/m141](https://gitlab.com/ch-tbz-it/Stud/m141/m141#m141---datenbanksystem-in-betrieb-nehmen)

[Kompetenzmatrix](https://gitlab.com/modulentwicklungzh/cluster-platform/m141/-/tree/master/1_Kompetenzmatrix)


## Leistungsbeurteilungen (Prüfungen)

- Sie führen in diesem Modul ein [Lernportfolio](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio). 
  <br> Vorzugsweise in GitLab oder GitHub. Die Bewertung ist Teil der **LB3**.<br><br>
- Am Tag 5 **LB1 (20%)** mit Inhalt von Tag 3 und Tag 4 (=Lernziele) <br><br>
- Am Tag 8 **LB2 (30%)** mit Inhalt von Tag 5 und Tag 6 (=Lernziele) <br><br>
- Am letzten Tag **LB3 (50%)** die [Abgabe des Lernproduktes]((https://gitlab.com/ch-tbz-it/Stud/m141/m141/-/blob/main/LB3-Praxisarbeit)) inkl. [Lernportfolio](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio)
  <br> (Eintragen in [Terminplan](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/EVE5Ey7ShRdOsIdcA7G49JIBgT8ngEPuOV30f1sumVw5AA?e=NpzeWE))


## Ablaufplan 2024/25-Q3 **PE23d** (Fr nachmittags)

|Tag  | Datum |[KompMatrix](https://gitlab.com/modulentwicklungzh/cluster-platform/m141/-/tree/master/1_Kompetenzmatrix) |Thema, Auftrag, Übung |
|---- |----   |----       |----         |
| 1   | 21.02.| A1, A2    | Einführung, [Intro und Installation](https://gitlab.com/ch-tbz-it/Stud/m141/m141/-/blob/main/1.Tag) <br> RDBMS - Übersicht und Installation von Tools wie XAMPP, Apache-Webserver, MariaDB (Service & Client), phpMyAdmin und MySQL-Workbench |
| 2   | 28.02.| B         | [Konfiguration und Datenimport](https://gitlab.com/ch-tbz-it/Stud/m141/m141/-/blob/main/2.Tag) <br> Die Datei my.ini, Optionen, <br> Importieren von Dump, CSV, JSON, Schema usw.|
| 3   | 07.03.| C         | [Tabellentypen und Transaktionen](https://gitlab.com/ch-tbz-it/Stud/m141/m141/-/blob/main/3.Tag) <br> MyISAM, InnoDB. <br> Das Transaktionen- und Locking-Konzept |
| 4   | 14.03.| C, D      | [Datenbanksicherheit](https://gitlab.com/ch-tbz-it/Stud/m141/m141/-/blob/main/4.Tag) <br> Authentifizierung und Zugang übers Netz |
| 5   | 21.03.| D         | **LB1 (20%)** Inhalt: Tag 3 und 4 <br> [Zugriffsystem](https://gitlab.com/ch-tbz-it/Stud/m141/m141/-/blob/main/5.Tag), <br> Autorisierung, Zugriffsberechtigung DCL |
| 6   | 28.03.| A, C2, D  | [Server-Administration](https://gitlab.com/ch-tbz-it/Stud/m141/m141/-/blob/main/6.Tag) <br> Admin-Tools, Logging, Optimierungen |
| 7   | 04.04.| E         | [Testen](https://gitlab.com/ch-tbz-it/Stud/m141/m141/-/blob/main/7.Tag) <br> Testing, Ablauf und Performanz |
| 8   | 11.04.| A, B, C, D| **LB2 (30%)** Inhalt: Tag 6 und 7 <br> Setup lokaler RDBMS (MySQL/MariaDB), DDL, DML, DCL und Testen <br> <br> Beginn **LB3 (50%)** [Praxisarbeit](https://gitlab.com/ch-tbz-it/Stud/m141/m141/-/blob/main/LB3-Praxisarbeit) <br> Produktive DB (Struktur, Daten und Berechtigungen) von lokaler DB auf die Cloud-DB migrieren (zB AWS) |
| --  | ----  |           | Karfreitag   |
| --  | ----  |           | Osterferien  |
| 9   | 09.05 | D, E      | **LB3 (50%)** [Praxisarbeit](https://gitlab.com/ch-tbz-it/Stud/m141/m141/-/blob/main/LB3-Praxisarbeit) <br> Produktive DB auf der Cloud testen und optimieren der Dokumentation <br> Abgabe gem. Plan (10-12 min) <br><br> **Abgabe LB3** -----> [Terminplan](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/EVE5Ey7ShRdOsIdcA7G49JIBgT8ngEPuOV30f1sumVw5AA?e=NpzeWE)|

<br>
Harald Müller, Feb 2025
