## Tag6 - Multimedia / Steganografie / Einfache Verschlüsselung
 	 	 	 	 	 	 
Mehr Information		Adobe PDF	
	
- 05_Multimedia1.pdf
Aktionen: 05_Multimedia1.pdf	
4.2 M
 	
 	 	 	
- Einführung Farbe, Foto, Video, RGB, CMYK
 	 	 	 	 	 	 
- 05_Multimedia1Loseungen.txt
 
- 05_Testbilder.zip
 	 	 	
- [7 min, J. Arnold, 2015 - Eine Audio-Qualitätsbetrachtung](https://vimeo.com/161034117)
 	 	 	 	 	 	 
- PublicKeyIntro.pdf
	
- Steganografie 1, [3 min, D, YouTube, Erklärvideo Bilderverschlüsselung (Steganographie)](https://www.youtube.com/watch?v=k4W52FCWtvI)
 	 	 	 	 	 	 
- Steganografie 2, [3 min, D, YouTube, Kryptologie-Steganographie](https://www.youtube.com/watch?v=SBTi1CkmHUQ)
 	 	 	 	 	 	 
- Steganografie 3, [4 min, D, YouTube, Steganographie Theorie](https://www.youtube.com/watch?v=EzKbsfGhC4w)
 	 	 	 	 	 	 
- Steganografie 4, [7 min, D, YouTube, Hacking mit Python - Text in Bildern und MP3s verstecken](https://www.youtube.com/watch?v=PK3L62Vkux4)
 	 	 	 	 	 	 
- Steganografie 5, [7 min, D, YouTube, Nachricht in Bild verstecken - Tutorial](https://www.youtube.com/watch?v=XZDvwBQ6hXg)

- Steganografie 6, [7 min, D, YouTube, Verstecke Nachrichten/Dateien in Bildern](https://www.youtube.com/watch?v=XcX3JY30tqU)

- Steganografie 7, [13:13 min, E, YouTube, Secrets Hidden in Images (Steganography, Mike Pound)](https://www.youtube.com/watch?v=TWEXCYQKyDc)