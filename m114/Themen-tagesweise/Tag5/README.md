## Tag 5 - Kompression mit Verlust
	
[M114_Tag5_Repetition_Praeinstruktion.txt](_M114_Tag5_Repetition_Praeinstruktion.txt)	
 	 	 	 	 	 	 
```
M114 Repetition und Pränstruktion
---------------------------------

Wieviele Nullen hat ein Tera?

Wieviel Bit hat ein Tera Byte?

Wieviel Uhr ist es in dezimal?
0 0 1 0 1 1 Std
1 1 1 0 0 0 Min
0 0 0 1 1 0 Sek

Wie gehen die Schritte bei der Huffman Komprimierung? 
Was muss man der Reihe nach machen?

Installieren Sie sich das gratis Grafik-Programm 
"Irfanview" und speichern Sie die beiden gegebenen 
Bildern einmal im Format ".jpg", ".gif" und in ".png". 
Was fällt auf und was könnte dahinter stecken?

```


### Einleitung

Bildformate erklärt | PNG, JPEG, BMP & mehr - Wann welches Format nutzen?
<br>8:16 min, D, Youtube, 09.08.2022
<br>https://www.youtube.com/watch?v=FqPUwN_iqPk

How are Images Compressed from 46 MB to 4 MB (jpeg)?
<br>18:46 min, E, Youtube, 23.12.2021
<br>https://www.youtube.com/watch?v=Kv1Hiv3ox8I


![DCT-Koeffizienten.png](DCT-Koeffizienten.png)	

 	
[DCTDemo.ppt](DCTDemo.ppt)
 
[DCTDemo.zip](DCTDemo.zip)	
	
	
### M114 - Interlace
 	 	 	
Was bedeutet "interlaced" in Grafiken?

https://de.wikipedia.org/wiki/Interlacing_(Grafiken)
 	 	 	 	 	 	 
Frage 1:
```
Bei welchen Bildformaten geht/funktioniert interlaced?


```

Frage 2:
```
Wo werden/wurden die interlaced-Formate verwendet?


```

### M114 JPG-Bilder

M114 - JPG0 - Colourspaces (JPEG Pt0)- Computerphile	 	 	
<br>7:30 min, E, YouTube, 10.4.2015
<br>https://www.youtube.com/watch?v=LFXN9PiOGtY
 	 	 	 	 	 	 	
M114 - JPG1 - JPEG 'files' & Colour (JPEG Pt1)- Computerphile
<br>7:17 min, E, YouTube, 21.4.2019
<br>https://www.youtube.com/watch?v=n_uNPbdenRs

M114 - JPG2 - JPEG DCT, Discrete Cosine Transform (JPEG Pt2)- Computerphile
<br>15:11 min, E, YouTube, 22.5.2015
<br>https://www.youtube.com/watch?v=Q2aEzeMDHMA
 	 	 	 	 	 	 
M114 - JPG3 - The Problem with JPEG - Computerphile
<br>5:36 min, E, YouTube, 9.6.2015
<br>https://www.youtube.com/watch?v=yBX8GFqt6GA

M114 - JPEG - How are Images Compressed from 46 MB to 4 MB
<br>18:46 min, E, Youtube, 23.12.2021
<br>https://www.youtube.com/watch?v=Kv1Hiv3ox8I

M114 - JPG4 - Zusammenfassung
<br>7:30 min, D, YouTube, 23.1.2010
<br>https://www.youtube.com/watch?v=GKjycTURXhI


### Editoren

M114_Software_IrfanView-BildEditor
<br>https://www.irfanview.com/main_what_is_ger.htm
 	 	 	 	 	 	 
M114_Software_OpenShot-VideoEditor
<br>https://www.openshot.org