## M114 - Tag 1  Codierungen / Zahlensysteme

	M114  Prä-Instruktion Tag 1
	---------------------------
	Regeln: 
	Im 1. Schritt: Versuchen zu lösen/beantworten ohne nachschlagen. 
	Vermutungen sind auch gut!
	Nach 15 min: Jetzt können Sie im Recherchen machen.
	-------------------------------------------------------------
```	
	Auf wieviel können Sie mit den Fingern einer Hand zählen?
	
	Was heissen folgende Präfixe (=Vorsilben). 
	Welche Zahlen bezeichnen sie?
	- kilo
	- mega
	- giga
	- milli
	- mikro
	- nano
	
	Was bedeuten diese Abkürzungen ausgeschrieben?
	bezüglich Codierung
	- ASCII
	- ANSI
	- UTF
	bezüglich Farben:
	- RGB
	- CMYK
	bezüglich Dateiformaten
	- PDF
	- GIF
	- JPEG oder JPG

```

### Zeichensprache

3:27 min, WvO https://www.youtube.com/watch?v=dW3FGs8oaKg&t=8s


### Kodierung

Finden Sie heraus, welches Format welche Datei hat. 
Es ist ein Rätsel und am Schluss können Sie mit dem 
herausgefundenen Passwort das "Zitat" lesen.
<br>[Dateien.zip](x_ressourcen/Dateien.zip) 


### Zahlensysteme

[Die Wurzeln der Digitalisierung](Zahlensysteme/DieWurzelnDerDigitalisierung_2019-08-18.pdf)

M114 Binary Count
<br>1:33 min, YouTube, [Binary Count](https://www.youtube.com/watch?v=zELAfmp3fXY)
<br>0:44 min, Youtube, [Binary Counter Twos Complement](https://www.youtube.com/watch?v=pYgWKKvsdOk)

M114 Binär mit den Fingern zählen
<br>04:42 min, D, 2016, YouTube, [Sendung mit der Maus - Von 0 bis 1023 zählen](https://www.youtube.com/watch?v=B_06pe8Bt90&t=29s)
 	 	 

M114 Binärsystem - Dualsystem - ganz einfach erklärt
<br>13:20 min, D, 2018, YouTube [Dualsystem - ganz einfach erklärt](https://www.youtube.com/watch?v=I3-cmqbVF0Y)


Probieren Sie das aus:
<br>So ähnlich sah bei einer **Tankestelle** früher die **Anzeige** aus.
Allerdings kann bei diesem Programm noch die Anzahl Zahlen auf 
einer **Rolle** gewählt werden.
<br>[Tankstellenanzeige.zip](Zahlensysteme/Tankstellenanzeige.zip)


*Übungen*

[Zahlensysteme_Uebungen_AD-DA-Wandler](Zahlensysteme_Uebungen_AD-DA-Wandler)