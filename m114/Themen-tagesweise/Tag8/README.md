## Tag8 - Asymmetrische und hybride Verschlüsselungsverfahren, Digitale Signatur

	
01 - Cäsar-Verschlüsselung
<br>15:42 min, YouTube, D
<br>Verweis: https://www.youtube.com/watch?v=mn-b36ax4PQ
 	 	 	 	 
	
	
02 - Vigenère-Verschlüsselung
<br>11:17 min, YouTube, D
<br>Verweis: https://www.youtube.com/watch?v=u6i4kKzeOWA
 	 	 	 	 	
	
RSA - Verschlüsselung
<br>47 min, 5 Videos in YouTube Playlist, Vorlesung Prof. Chr. Spannagel
<br>Verweis: https://www.youtube.com/playlist?list=PLdVk34QLniSBox_E5IFU9S4zSszFE2RsJ&disable_polymer=true
 	 	 	 	 
	
RSA Verschlüsselung mit Schlüsselgenerierung und Modelldurchlauf- it-archiv.net
<br>8:50 min, D, YouTube
<br>Verweis: https://www.youtube.com/watch?v=gFd_SZZF63I
 	 	 	 	 
	
	
Das RSA-Kryptosystem
18:57 min, D, YouTube, Weiz, 2018
Verweis: https://www.youtube.com/watch?v=mDRMzBlI3U4


	
99 - Alan Turing - Wer war er eigentlich
<br>5:17 min, D, YouTube
<br>Verweis: https://www.youtube.com/watch?v=hGsmzgY_QVo


98 - Wie ein Mathegenie Hitler knackte. ARTE Doku
<br>1:13:06, D, YouTube, 2017
<br>Verweis: https://www.youtube.com/watch?v=ttRDu4wuVTA
 	
