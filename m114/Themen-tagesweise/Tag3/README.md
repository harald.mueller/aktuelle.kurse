## Tag 3 - Komplementbildung / MP3 / Barcode / Unicode

```
M114 Tag3 Lernstandsanalyse 
---------------------------
      X hoch 0 = ?

       1 (dez) =      ???? (bin)
      10 (dez) =      ???? (bin)
     126 (dez) = ???? ???? (bin)
    1298 (dez) = ???? ???? (bin)
	
       4 (dez) =         ? (hex)	 
      15 (dez) =         ? (hex)
      32 (dez) =        ?? (hex)
    1298 (dez) =       ??? (hex)
	
    0001 (bin) =         ? (dez)
    1111 (bin) =         ? (dez)	
 10'1010 (bin) =        ?? (dez)		
101'1001 (bin) =        ?? (dez)

3 (hex) =   ? (dez)  =   ???? (bin)
5 (hex) =   ? (dez)  =   ???? (bin)
A (hex) =   ? (dez)  =   ???? (bin)
F (hex) =   ? (dez)  =   ???? (bin)
		
5AF3 (hex) =    ??'??? (dez)
5AF3 (hex) = ???? ???? (bin) 
--> was fällt hier auf?


wobei 16 hoch 2 = 256
wobei 16 hoch 3 =4096


für die Ehrgeizigen und Schnellen
   23'283 (dez) =      ???? (hex)
1011'1011 (bin) =       ??? (dez)
```

### Code-Aufgaben

[CodesAufgaben_Teil2.pdf](CodesAufgaben_Teil2.pdf)
[Lösungen](CodesLoesungen_Teil2.pdf)


### Die Komplementbildung

Wie funktioniert das? Was sind die Probleme?

[Komplementbildung.pdf](Komplementbildung.pdf)

### MP3

![mp3Fileanalyse.jpg](MP3/mp3Fileanalyse.jpg)

[Anleitung](MP3/MP3_Analyse.pdf)

Audio-File ["Hula-Hoop"](MP3/Hula-Hoop.mp3)


### Barcode 

[Barcode.pdf](Barcode.pdf)

### Unicode

[Unicode.pdf](Unicode.pdf)

[Unicodes-otherSamples.zip](Unicodes-otherSamples.zip)