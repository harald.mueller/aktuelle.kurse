## M114 - Tag 2 - Codierungen und Interprätationen von Daten


[M114-Praeinstruktion-Tag2.txt](_M114-Praeinstruktion-Tag2.txt)

```
M114  Prä-Instruktion Tag 1
---------------------------
Regeln: 
Im 1. Schritt: Nicht nachschlagen. Vermutungen sind auch gut!
Nach 15 min: Jetzt können Sie im Internet Recherchen machen.
-------------------------------------------------------------

Beschreiben Sie, wie ein Morse-Code funktioniert.

Zeigen Sie Ihrem Nachbarn, wie Sie mit 4 Fingern auf 16 zählen.

Was ergibt x hoch 0 ?

Was heisst 2 auf Italienisch?
Was heisst 4 auf Altgriechisch?
Was heisst 6 auf Altgriechisch?
Was heisst 10 auf Italienisch?

Was sind "Tetraden" in der Mathematik?

Welchen ASCII-Dezimalwert hat das @-Zeichen?

```	

Lösungen:
<br>http://www.mathematische-basteleien.de/tetrade.htm
<br>https://www.asciitable.com



### Codierungs-Systeme

M114 Tag2 Digitaluhr am Bahnhof St. Gallen
<br>Verweis: https://www.srf.ch/news/regional/ostschweiz/bahnhof-st-gallen-die-uhr-die-niemand-lesen-kann
 	 	 	 	 	 	 

M114 Tag2 Digitaluhr Erklärung
<br>3:16 min
<br>Verweis: http://www.youtube.com/watch?v=E9I3febbWxY
 	 	 	 	 	 	 



M114 Tag2 Lerne den Morse-Code
<br>2 min
<br>Verweis: https://www.youtube.com/watch?v=_IlZrZ9N4ig


M114 Tag2 Lerne das Flag-Semaphoren-Alphabeth
<br>3 min
<br>Verweis: https://www.youtube.com/watch?v=SulHXA4JbE8 


M114 Lerne das Flaggen-Aplhabeth
<br>2 min
<br>Verweis: https://www.youtube.com/watch?v=nVuKrGOuC2c


- [Tag2a-Codierungen](Tag2a-Codierungen)


- [Tag2b-Demos-von-Schueler](Tag2b-Demos-von-Schueler)


- [Tag2c-Zaehlen-und-Zahlensysteme](Tag2c-Zaehlen-und-Zahlensysteme)


- [Tag2d-Leitungscode-Manchester](Tag2d-Leitungscode-Manchester)



### Binär-Rechner-Tabelle

[binaer-rechner.xlsx](binaer-rechner.xlsx)