# Thema D - sichere Kommunikation


[**Einstieg**](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/D.%20Gesicherte%20Daten%C3%BCbertragung)

## Unter-Themen

- [PKI Public Key Infrastruktur](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/D.%20Gesicherte%20Daten%C3%BCbertragung/D.1%20Public%20Key%20Infrastruktur)
- [Internet und Zertifikate](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/D.%20Gesicherte%20Daten%C3%BCbertragung/D.2%20Internet%20und%20Zertifikate)
- [PGP und OpenPGP](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/D.%20Gesicherte%20Daten%C3%BCbertragung/D.3%20PGP%20und%20OpenPGP)
- [Sichere E-Mails](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/D.%20Gesicherte%20Daten%C3%BCbertragung/D.4%20Sichere%20E-Mails)


## "Hands on"

Möglichkeiten zur Programmierung oder anschaulich machen

(Zeitbedarf 4-6 Lektionen)

- Installationsanleitung inkl. Demo und Test von<br>verschlüsseln und/oder signieren von E-Mails
- Aufzeigen und installieren von s/mime<br>(Verschlüsseluing über Zertifikate)
- Aufzeigen und Funktionsweise der PKI (Public Key Infrastruktur)
- Wie geht PGP (pretty good privacy) an einem Beispiel erklärt
- Man-in-the-Middle Attacke simulieren/zeigen(!)
- Brute-Force Attacke simulieren/zeigen

(!) aufzeigen, wie man das machen könnte, wenn die Übermittlung 
der Nachricht unverschlüsselt ist. Und wenn sie ganz einfach, 
z.B. mit einer einfachen Rotations-Chiffre (Cäsar) verschlüsselt ist
