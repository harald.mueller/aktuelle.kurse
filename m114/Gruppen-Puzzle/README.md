# Unterrichtsmethode

## Gruppen-Puzzle 

**Funktionsweise**

https://www.youtube.com/watch?v=TLai7zWbsBc (1:50, D, 2021)

In der Stammgruppe verschaffen Sie sich ein Überblick 
über das Thema. Sie erarbeiten sich und lernen dabei 
in der Experten-Gruppe (A-E) zusammen ihre Bausteine (A-E) 
zum Thema so, dass Sie es nachher in der Stamm-Gruppe (1-5) 
erklären können. 

Nach dieser Lern- und Trainings-Sequenz gibt es Tests im Klassenverband. 
Und danach trifft sich die Expertengruppe wieder, um Prüfungsfragen und 
(Muster-)Lösungen zu erarbeiten.



- Schritt a.) Alleine / zu Zweit / Alle
	<br>**"Vorwissen aktivieren"** und Überblick verschaffen
	<br>Beantworten von einleitenden Fragen 

- Schritt b.) In der Expertengruppe
	<br>**"Wissens- und Könnens-Aneignung"**
	<br>Unterlagen recherchieren, lesen, Video schauen usw. 

- Schritt c.) In der Expertengruppe
	<br>**"Üben und festigen"**
	<br>Üben Sie in der Gruppe die Kenntnisse/Techniken/Fakten und das Wissen bis alle "sattelfest" sind.
	<br>Erstellen geeigneter Lehr- oder Lern-Unterlagen

- Schritt d.) In der Stammgruppe
	<br>**"Wissens-Transfer"**. 
	<br>Sie sind in Ihrem Gebiet jetzt Experte und erklären den anderen in der Stammgruppe Ihr Thema.
	<br>Ihre Stammgruppe muss nach den Erklärungen soweit sein, dass nachher "random" jemand für jeses Thema ausgewählt werden kann, der/die dann das Thema vorführen/erklären/zeigen kann.

- Schritt e.) In der Stammgruppe
	<br>**"Test"**
	<br>Vor einer zufälligen Gruppe oder im 2-er Team wird jedes Thema "random" von jemandem die entsprechende **Erklärung** oder **Vorführung** gemacht.

- Schritt f.) In der Expertengruppe
	<br>**"Prüfungsfragen erarbeiten"**
	<br>Fragen und Antworten für die grosse Prüfung erarbeiten und aufschreiben. 
	<br>Die Lehrperson macht dann eine Auswahl aus dem Fragen-Set. 

<br>
<br>


|	 | ExpertGrp A | ExpertGrp B | ExpertGrp C | ExpertGrp D | ExpertGrp E|
|--------------------|-----|-----|-----|-----|-----|
| **StammGrp 1** |  #1 |  #2 |  #3 |  #4 |  #5 |
| **StammGrp 2** |  #6 |  #7 |  #8 |  #9 | #10 |
| **StammGrp 3** | #11 | #12 | #13 | #14 | #15 |
| **StammGrp 4** | #16 | #17 | #18 | #19 | #20 |
| **StammGrp 5** | #21 | #22 | #23 | #24 |     |

