# M321 - Verteilte Systeme programmieren

Inhaltsverzeichnis

[TOC]


[**Modulidentifikation** ICT CH](https://www.modulbaukasten.ch/module/321/1/de-DE?title=Verteilte-Systeme-programmieren)

[weitere TBZ Unterlagen -> https://gitlab.com/ch-tbz-it/Stud/m321/m321](https://gitlab.com/ch-tbz-it/Stud/m321/m321)



Als Beispiel eine Web-Applikation mit Frontend, Backend und weiteren Umsystemen (Marktdaten, Referenzdaten, Sensoren, ...)

![VerteilteApplikation.png](x_ressourcen/BeispielStrukturVerteilteApplikation.png)

In diesem Modul soll eine Applikation entworfen und gebaut werden mit der Bedingung, dass die entwickelten Systeme **etwas "können" müssen**, es soll also eine praktische Applikation werden.


## Leistungsbeurteilungen 

Durch die erfolgreiche Absolvierung dieses Moduls erreichen Sie die unter [Kompetenzmatrix](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/blob/main/Kompetenzmatrix.md) angegebenen Kompetenzen. Die erreichten Kompetenzen werden durch [Leistungsbeurteilungen](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/blob/main/Leistungsbeurteilung.md) bewertet.

- LB1:(40%) Praktische Einzelarbeit "Schnittstelle zwischen zwei Systemen (Frontend & Backend) planen, umsetzen und dokumentieren" <br> 
- LB2:(60%) Projektarbeit (Arbeit im Duett oder im Trippel - jede Person entwickelt einen Teil des verteilten Systems, anschliessend werden die Teile zu einem Gesamtsystem zusammengehängt)
	

## Ablaufplan 2024/25-Q3 **WUP23a** (Mi morgens)

|Tag  | Datum |Thema, Auftrag, Übung |
|---- |----   |----                  |
| 1   | 19.02.| Einleitung, [Definition Wikipedia](https://de.wikipedia.org/wiki/Verteiltes_System), [Definition von K. Zettler, Atlassian](https://www.atlassian.com/de/microservices/microservices-architecture/distributed-architecture) <br> [**Aufbau verteilter Systeme**](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/01%20Aufbau%20verteilter%20Systeme) <br>Ein Überblick über die Systemkomponenten und den grundsätzlichen Aufbau bzw. das Zusammenspiel der einzelnen Komponenten. <br> &#9997; Auftrag: [Recherche von Systemkomponenten](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/blob/main/01%20Aufbau%20verteilter%20Systeme/RechercheSystemkomponenten.md) (ca. 1 Lektion) |
| 2   | 26.02.| [**Lokale Entwicklungsumgebung**](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/02%20Lokale%20Entwicklungsumgebung) <br>Informationen dazu wie eine lokale Entwicklungsumgebung für die Entwicklung und das Testing von verteilten Systemen aufgebaut werden kann. |
| 3   | 05.03.| [**Migration zu verteiltem System**](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/03%20Migration%20zu%20verteiltem%20System) <br>Wie eine monolithische Applikation in ein verteiltes System überführt werden kann. Es werden einige mögliche Vorgehensweisen erklärt. <br> &#9997; Auftrag: [Aufteilung Monolith](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/blob/main/03%20Migration%20zu%20verteiltem%20System/Aufteilung-Monolith.md) (ca. 1 Lektion) |
| 4   | 12.03.| [**Datenhaltung**](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/04%20Datenhaltung) <br>Bei verteilten Systemen ist die persistente Datenspeicherung eine besondere Herausforderung. Dieses Kapitel beschäftigt sich damit, wie diese Herausforderung gemeistert werden kann. <br> &#9997; Auftrag: [Cluster Dateisystem](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/blob/main/04%20Datenhaltung/Cluster-Dateisystem.md) (2-3 Lektionen) <br> [Cluster-Dateisystem_Anleitungen](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/04%20Datenhaltung/Cluster-Dateisystem_Anleitungen/ClusterFileSystem) |
| 5   | 19.03.| [**Datenaustausch**](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/05%20Datenaustausch) <br>Ein Überblick über gebräuchliche Datenaustauschprotokolle wird gegeben. Es wird darauf eingegangen wie Schnittstellen zwischen den Systemkomponenten geplant und dokumentiert werden. <br> &#9997; Auftrag: [Schnittstellenbeschreibung](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/blob/main/05%20Datenaustausch/Schnittstellenbeschreibung.md) (ca. 1 Lektion) <br> <br>**LB1** Abgabe Schnittstellenbeschreibung und Dokumentation (Tage 5 und 6) |
| 6   | 26.03.| [**Sicherheitsaspekte**](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/06%20Sicherheitsaspekte) <br>Alle sicherheitsrelevanten Themen sind in diesem Kapitel gebündelt.|
| 7   | 02.04.| [**Skalierung**](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/07%20Skalierung) <br>Bei der Skalierung geht es darum ein verteiltes System an die Anzahl der Nutzer anzupassen. Ebenfalls ist Hochverfügbarkeit (High Availability oder HA) ein Thema.|
| 8   | 09.04.| [**Monitoring und Logging, Testing und Debugging**](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/08%20Monitoring%20und%20Logging,%20Testing%20und%20Debugging) <br>Ist ein verteiltes System am Laufen, dann muss dieses überwacht werden. Treten Fehler oder Performance-Probleme auf muss die Ursache herausgefunden und Massnahmen für die Behebung geplant und umgesetzt werden.|
| 9   | 16.04.| - entfällt gemäss WUP-Terminplan - <br> [**Optionale Inhalte**](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/09%20Optionale%20Inhalte) <br> [Blockchain](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/09%20Optionale%20Inhalte/Blockchain), [Datenkonsistenz](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/09%20Optionale%20Inhalte/Datenkonsistenz), [Docker_Kubernetes](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/09%20Optionale%20Inhalte/Docker_Kubernetes), [Konsensalgorithmen](https://gitlab.com/ch-tbz-it/Stud/m321/m321/-/tree/main/09%20Optionale%20Inhalte/Konsensalgorithmen)  |
| -   | --    | Osterferien     |
|10   | 07.05 | **LB2** Abgabe online gemäss [Terminliste]()  |

<br>
<br>
<br>

&copy; MUH, Feb 2025