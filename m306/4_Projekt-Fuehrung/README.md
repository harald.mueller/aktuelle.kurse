# M306/4 Projekt-Führung

## Praxis 
- Offline-Erfahung "Teambildung" inkl. Video-Aufnahmen <br/>
(--> Struktur-Legen in Gruppen zu 5-7 Personen) <br />
Ressourcen: [Struktur-Lege-Karten](../vorlagen_beispiele/M306_StrukturLegeKarten.docx)

- Tabu-Spiel in Gruppen zu 5-7 Personen "[Sitzungstypen](./M306_4_Projektfuehrung_RollenInGruppen(Sitzungstypen).pdf)" <br/>
(--> Papierstreifen bereitlegen)<br/>


## Theorie
- [Projektführung_Praeinstruktion](./M306_4_Projektfuehrung_Praeinstruktion.txt)


- [Skript (V3.1.4, S.23-32)](./Skript_4_Projekt-Fuehrung(die-Rolle-des-Projektleiters-und-Teambildung).pdf) 
**"Projektführung"** (entlang dem Theoriedokument)
  - Die Rolle des Projektleiters
  - Teambildung _**( --> Offline-Erfahrung )**_
  - Menschenbild/Sitzungstypen _**( --> Tabu-Spiel )**_
  - Sitzungsgestaltung
  - Pareto-Prinzip (80/20-Regel)
  - Eisenhower-Prinzip (wichtig/dringlich)
  - Entscheidungsfolgen-Matrix

## Anwendung
- PodCast 22:18 min, D, 2020-11-25 und  - **_Wie ideale Teams funktionieren_** [mp3 online](https://media.neuland.br.de/file/1810761/c/feed/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.mp3), [mp3 local](./201216_1450_radioWissen_Wie-ideale-Teams-funktionieren.mp3)<br>
[Fragen](./wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.txt), 
<br>[BR Podcast | BAYERN 2 | radioWissen | WIE IDEALE TEAMS FUNKTIONIEREN - GEMEINSAM ANS ZIEL](https://www.br.de/mediathek/podcast/radiowissen/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel/1810761) zum PodCast

## Weiterführendes
- PlayListe [Führung und Kommunikation (Projekte leicht gemacht)](https://www.youtube.com/playlist?list=PLDMn0zv5rxCsWHrZ11C0NFSqUWsxLDNSu)
- z.B. [Vier Ohren Modell (F. Schulz von Thun), 09:40, D, 2022](https://www.youtube.com/watch?v=USEM11RElsQ)
