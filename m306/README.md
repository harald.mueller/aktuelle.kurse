# M306 - Kleinprojekte im eigenen Berufsumfeld abwickeln

**Modulidentifikation**
- Bivo21 (Lehrbeginn ab 2021) [.html](https://www.modulbaukasten.ch/module/306/4/de-DE?title=Kleinprojekte-im-eigenen-Berufsumfeld-abwickeln)
<br>
<br>
![projektmanagement.jpg](projektmanagement.jpg)
<br>
<br>
<br>


[TOC]

<br>
<br>

## Tages- und Themenplan



### Ablaufplan 2024-Q2  AP22d (Fr morgens)

| ~Tag | Datum     | Thema                                                                          | Bemerkungen               |
| ---- | -----     | ----                                                                           | ----                      |
|   1  | Fr 15.11. | [Projekt-Ziele                        ](./1_Projekt-Ziele)                     |                           |
|   2  | Fr 22.11. | [Projekt-Planung                      ](./2_Projekt-Planung)                   | Ecolm-Test P-Zie 7% MNote |
|   -  | -- 29.11. | - fällt aus - Lehrer-Klausurtagung                             |                           |
|   3  | Fr 06.12. | [Projekt-Organisation                 ](./3_Projekt-Organisation)              | Ecolm-Test P-Pla 7% MNote |
|   4  | Fr 13.12. | [Projekt-Führung                      ](./4_Projekt-Fuehrung)                  | Ecolm-Test P-Org 7% MNote |
|   5  | Fr 20.12. | [Projekt-Kontrolle und Vorgangsmodelle](./5_Projekt-Kontrolle-Vorgangsmodelle) | Ecolm-Test P-Füh 7% MNote |
|   -  | -Ferien-  |                                                                                |                           |
|   6  | Fr 10.01. | [Projekt-Risiken und Risikoanalyse    ](./6_Projekt-Risiken-Risikoanalyse)     | Ecolm-Test P-Kon 7% MNote |
|   7  | Fr 17.01. | [Qualitätsicherung                    ](./7_Qualitaetsicherung)                | Gruppen-Projekt 32% MNote |
|   8  | Fr 24.01. | [Abschlussprojekt                     ](./Abschlussprojekt)                    |                           |
|   9  | Fr 31.01. | [Abschlussprojekt                     ](./Abschlussprojekt)                    | Gruppen-Projekt 33% MNote |
|      |           |                                                                                |                           |
|  add on |  1     | [Lastenheft-Pflichtenheft](./add-on1_Lastenheft-Pflichtenheft)                 |                           |

<br>
<br>
<br>[TIME](https://www.youtube.com/watch?v=JwYX52BP2Sk)
<br>[MONEY](https://www.youtube.com/watch?v=cpbbuaIA3Ds)
<br>


![IT-Projectmanagement.jpg](./it-projectmanagement.jpg)

![Projekt-Zeitbedarf.jpg](./M306_Cartoon_Projekt-Zeitbedarf.jpg)

