# M306/3 Projekt-Organisation

- [Projektorganisation_Praeinstruktion](./M306_3_Projektorganisation_Praeinstruktion.txt)

- (6:00 min, D) [Formen der Projektzusammenarbeit](https://www.youtube.com/watch?v=6IX2G0LXk9U&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=6)


## Theorie
- [Skript (V3.14. Seiten 24-31)](../docs) 


## Auftrag / Aufgabe
- Aufgabe Variante A Projektorganisation_u_Projektplanung 
	[docx](../uebungen/M306_3_Aufgabe1_Projektorganisation_u_Projektplanung-v4.docx)
	[pdf](../uebungen/M306_3_Aufgabe1_Projektorganisation_u_Projektplanung.pdf)
- Aufgabe Variante B Projektorganisation "KS Textil AG" 
	[pdf](../uebungen/M306_3_Aufgabe2_ProjektorganisationOrganigramm.pdf)