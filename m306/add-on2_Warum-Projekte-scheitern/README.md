## M306/8 Warum Projekte scheitern

Stand 11.11.2021, MUH


**Auftrag**

Recherchieren Sie weitere Stellen oder Beispiele im Internet 
(oder durchsuchen Sie die Liste unten) über 
"IT-Flops", "gescheiterte IT Projekte" usw.
und versuchen Sie herauszufinden, **_warum_** Projekte scheitern.


Erstellen Sie in der Gruppe zu 3 Personen eine **_Checkliste_** der Gründe oder der Erkenntnisse zur Präsentation in der Klasse. 


**Mini-Film - Comedian GUNKL**
 - [05:20 min, Gunkl: Die Vasa, der Stolz der schwedischen Marine](https://www.youtube.com/watch?v=ObIvBgkEGW4)


**Foliensatz TBZ, W. Steiner** 
 - [Warum Projekte scheitern.pdf](./Warum Projekte scheitern.pdf)


**IT- & Tech-Projekte aus der Zeitung**
 - [2010 Tops und Flops aus 25 Jahren IT-Geschichte](https://www.computerworld.ch/business/forschung/tops-flops-25-jahren-it-geschichte-1306576.html)
 - [2011 Die Flops von (Steve) Jobs](https://www.handelsblatt.com/technik/it-internet/it-ikone-die-flops-von-jobs/4539152.html?ticket=ST-4119864-RAo7aL5VafaLvHidNdDo-cas01.example.org)
 - [Die Tops und Flops der IT-Entscheider](https://www.tecchannel.de/a/die-tops-und-flops-der-it-entscheider,2077805,3)
 - [Insieme, wikipedia.org](https://de.wikipedia.org/wiki/Insieme_(Informatikprojekt))
 - [Was lief schief bei «Insieme»?](https://www.srf.ch/news/schweiz/was-lief-schief-bei-insieme)
 - [SRF Informatikprojekte: Finanzkontrolle schaut Bund auf die Finger](https://www.srf.ch/news/schweiz/informatikprojekte-finanzkontrolle-schaut-bund-auf-die-finger)
 - [2014 Postulat Eder Joachim. IT-Projekte des Bundes. Wie weiter?](https://www.parlament.ch/de/ratsbetrieb/amtliches-bulletin/amtliches-bulletin-die-verhandlungen?SubjectId=25831)
 - [2014 IKT-Grossprojekte des Bundes – Erkenntnisse und Massnahmen](https://www.parlament.ch/centers/eparl/curia/2013/20134062/Bericht%20BR%20D.pdf)
 - [2014 Gescheiterte IT-Projekte](https://www.computerwoche.de/a/gescheiterte-it-projekte,2546218)
 - [2015 Astra schmeisst Trivadis raus](https://www.computerworld.ch/business/politik/astra-schmeisst-trivadis-raus-1338774.html)
 - [2015 Und wieder ein gescheitertes IT-Projekt](https://hub.hslu.ch/informatik/und-wieder-ein-gescheitertes-it-projekt)
 - [2015 Die 19 grössten Flops der IT-Geschichte](https://www.computerworld.ch/business/digitalisierung/19-groessten-flops-it-geschichte-1325031.html)
 - [2016 watson: Korruption beim BAFU: Die lange Liste der IT-Millionengräber des Bundes](https://www.watson.ch/schweiz/articles/651863749-korruption-beim-bafu-die-lange-liste-der-it-millionengraeber-des-bundes)
 - [2019 A decade of failures - 84 biggest flops in tech](https://www.theverge.com/2019/12/20/21029499/decade-fails-flops-tech-science-culture-apple-google-data-kickstarter-2010-2019)
 - [2020 Das neuste Bundesberner IT-Debakel: Aus für «Soprano»](https://www.blick.ch/politik/aus-fuer-programm-soprano-bern-versenkt-millionen-mit-it-debakel-id15959741.html)


**Beraterfirmen**
 - [Warum IT-Projekte scheitern - dieprojektmanager.com](https://dieprojektmanager.com/scheitern-von-it-projekten)
 - [Warum IT-Projekte scheitern - dsmc.ch](https://www.dsmc.ch/file/Publikationen/IT-Projekte.pdf)
 - [Weshalb IT-Projekte scheitern - scopevisio.com](https://www.scopevisio.com/blog/projektmanagement/weshalb-it-projekte-scheitern/)
 - [Die 7 wichtigsten Gründe warum Tech-Projekte scheitern](https://codecontrol.io/de/blog/7-reasons-tech-projects-fail)
 - [Scheitern von IT-Projekten / Zahlen, Daten, Fakten, Studien](https://dieprojektmanager.com/scheitern-von-it-projekten/#:~:text=Die%20meisten%20IT%2DProjekte%20scheitern,Abstimmung%20aller%20am%20Projekt%20Beteiligter.&text=20%25%20aller%20IT%2DProjekte%20werden,oder%20wird%20teurer%20als%20geplant.)
 - [2009: Das Märchen von den gescheiterten IT-Projekten](https://digitalcollection.zhaw.ch/bitstream/11475/5715/1/DasMaerchenvomGescheitertenITProjekt.pdf)
 - [Lessons Learned – Ziele, Workshop, Praxis-Tipps](https://dieprojektmanager.com/lessons-learned-ziele-workshop-praxis-tipps/)


**Andere Projekte**
 - [Leadership in Projekten (Allgemeinen Offiziersgesellschaft Zürich)](https://www.alexandria.unisg.ch/255182/1/Rohner%20-%20Leadership%20in%20Projekten%20-%20Auszug%20aus%20AOG%20Schrift.pdf)
 - [Diese Projekte hätten die Schweiz verändert](https://www.20min.ch/story/diese-projekte-haetten-die-schweiz-veraendert-857693423451)
 - [GESCHEITERTE PROJEKTE - MAN KANN ALLES RICHTIG MACHEN – UND TROTZDEM SCHEITERN](http://projekt-manager.eu/gescheiterte-projekte.html)
 - [Wenige Projekte sind ein Erfolg, viele scheitern – auch in der Schweiz](https://rolandwanner.ch/wenige-projekte-sind-ein-erfolg-viele-scheitern/)

###### - Flughafen BER in Berlin 2006-2020

Der Flughafen Berlin Brandenburg kann als Paradebeispiel für ein außer Kontrolle geratenes gescheitertes Projekt herhalten. Der Spatenstich zur Flughafenbaustelle erfolgte am 5. September 2006. Die Betriebsaufnahme hätte im November 2011 erfolgen sollen. Dieser Termin – und auch alle nachfolgenden Eröffnungstermine – wurden jedoch u. a. wegen technischer Mängel  verschoben. Der Bau des Flughafens wurde im Oktober 2020 abgeschlossen und der Flughafen  konnte nach 14-jähriger Bauzeit eröffnet werden.
Beim Spatenstich im Jahr 2006 war von Kosten von ca. 2 Milliarden Euro die Rede gewesen, mittlerweile werden die Kosten auf ca. 7,3 Milliraden Euro geschätzt. Die medialen Schlagzeilen des Flughafenprojekts sind mittlerweile relativ einheitlich:

-- [ZDF](https://www.zdf.de/nachrichten/heute/bilder/flughafen-berlin-brandenburg-142.html)
-- [FAZ](https://www.faz.net/aktuell/wirtschaft/flughafen-ber-eine-chronik-des-scheiterns-13895339.html)
-- [Spiegel](https://www.spiegel.de/fotostrecke/flughafen-ber-chronik-eines-baudesasters-fotostrecke-169915.html)
-- [Der Tagesspiegel](https://www.tagesspiegel.de/berlin/die-chronik-einer-blamage-1-000-tage-seit-der-nichteroeffnung-des-ber/11429764.html)

###### - Toll-Collect

Der ursprünglich zum 31. August 2003 geplante Starttermin von Toll Collect – dem LKW-Maut System in Deutschland – konnte aufgrund diverser technischer Schwierigkeiten erst mit 16 Monaten Verspätung Anfang 2005 – in technisch reduzierter Form – in Betrieb genommen werden.

Am 29. Juli 2005 ließ das deutsche Bundesministerium Klage gegen das Maut-Konsortium einreichen. 1,6 Milliarden Euro Vertragsstrafen sowie 3,5 Milliarden Euro Einnahmeausfälle werden geltend gemacht. Im Mai 2018 – nach einem 14-jährigen Rechtsstreit – einigte sich der Bund mit den Hauptgesellschaftern des Betreiberkonsortiums Toll-Collect auf eine Zahlung in der Höhe von 3,2 Milliarden Euro an die Bundesrepublik.

###### - Adonis (Austrian Digital Operating Network for Integrated Services)

2002 startete die Firma Mastertalk das Projekt zum Aufbau eines bundesweites Behördenfunknetzes für alle Blaulicht- und Sicherheitsorganisationen in Österreich (Auftragsvolumen ca. 310 Millionen Euro).

Im Juni 2003 löste das BMI (Bundesministerium für Inneres) den Vertrag zur Errichtung von ADONIS aus folgenden Gründen auf: mangellhaftes Projektmanagement, Verzug von Leistungen, deutliche technische Mängel. Mastertalk klagte die Republik Österreich auf Zahlung von 181 Mio. Euro Schadenersatz. 2006 einigten sich die Republik und Mastertalk „gütlich“. Über die Zahlung wurde Stillschweigen vereinbart. Das Projekt wurde 2004 neu ausgeschrieben.

###### - FISCUS (Föderales Integriertes Standardisiertes Computer-Unterstütztes Steuersystem)

FISCUS war als einheitliche Software für die ca. 650 Finanzämter der Länder der Bundesrepublik Deutschland angedacht. Die Entwicklung von FISCUS startete 1993 mit strukturierten Entwicklungsmethodiken. Mitte der 1990er Jahre erfolgte ein Wechsel von der strukturierten Entwicklung hin zur objektorientierten Entwicklung. Man entschied sich das Framework „San Francisco“ von der Firma IBM einzusetzen.

Als IBM die Weiterentwicklung  des Frameworks „San Francisco“ einstellte, ging dem Projekt eine essenzielle technologische Basis verloren.  Nach einer Entwicklungszeit von 13 Jahren und geschätzten Entwicklungskosten von rund 400 Millionen Euro gab es 2006 kein brauchbares Ergebnis. Das Projekt wurde 2006 eingestellt und es wurde ein Nachfolgeprojekt namens KONSENS aufgesetzt.

###### - DIA-Gepäcksystem

Die Eröffnung des neu gebauten Denver International Airport – geplant für Oktober 1993 – verzögerte sich um fast 15 Monate, da die Software zur Steuerung der automatischen Gepäcksortieranlage, nicht rechtzeitig fertig gestellt wurde und es keine Alternativplanung gab. Dadurch entstanden Mehrkosten von 500 Millionen Dollar, die die Stadt Denver tragen musste.
