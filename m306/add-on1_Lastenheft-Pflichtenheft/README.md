### Lastenheft, Pflichtenheft

#### Warum ist das wichtig?
 - 12:48 min, D, 2018 [Lastenheft und Pflichtenheft. Warum brauche ich das als Webdesigner?](https://www.youtube.com/watch?v=-4Tq83wZFw8)

#### Was ist das? 
 - [Lastenheft](https://www.ionos.de/digitalguide/websites/web-entwicklung/lastenheft) 
 Ein Lastenheft bietet eine ganze Reihe an Vorteilen: Sie als **Auftraggeber** bekommen eine klare Vorstellung von Ihrem Projekt. Gleichzeitig können Sie Angebote von Auftragnehmern gut vergleichen – schließlich nutzen alle das Lastenheft zur Aufwandseinschätzung. Schwachstellen werden frühzeitig aufgedeckt und Ihr Projekt wird produktiver umgesetzt.
 
 - [Pflichtenheft](https://www.ionos.de/digitalguide/websites/web-entwicklung/pflichtenheft) 
 Ein Pflichtenheft ist sinnvoll, um bei der Umsetzung eines Projektes Zeit zu sparen. Es hilft **Auftragnehmer** und Auftraggeber bei der Kommunikation und Absprache. Nicht zuletzt lernen Sie viel über das bevorstehende Projekt, wenn Sie ein Pflichtenheft erstellen. Sofern dies klar und detailliert in Anlehnung an das Lastenheft formuliert wird, ist es eine Bereicherung für Ihr Projekt.
 
 - 5:51 min, D, 2016 [Lastenheft und Pflichtenheft - Teil 1](https://www.youtube.com/watch?v=Wim8hgTv_OQ)
 - 4:24 min, D, 2016 [Lastenheft und Pflichtenheft - Teil 2](https://www.youtube.com/watch?v=hlltEc5k4F4)
 - [Lasten- und Pflichtenheft, Peterjohann-Consulting](https://www.peterjohann-consulting.de/lastenheft-und-pflichtenheft)
