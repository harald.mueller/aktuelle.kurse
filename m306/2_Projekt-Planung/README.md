# M306/2 Projekt-Planung

- [Projektplanung_Praeinstruktion](./M306_2_Projektplanung_Praeinstruktion.txt)

<br>
![pjmwunder.jpg](pjmwunder.jpg)
<br>

## Theorie
- [Skript 3.1.4 (Seiten 16-24)](../docs) 
- **Beispiel**: Dokumentenplanung "Hermes" im Kanton Zürich: https://hermes.zh.ch,  [Szenarien](https://hermes.zh.ch/de/pjm-2022/verstehen/hermes-projektmanagement-methodenelemente.html), [Vorlagen](https://hermes.zh.ch/de/pjm-2022/anwenden/vorlagen.html)

## 1 Projekt-Rollen und Zusammenarbeit
- (4:40 min) [Rollen im Projekt - Wer macht was](https://www.youtube.com/watch?v=JQYLg-n_iZE&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=5)

## 2 Planung
- (3:40 min) [Planung macht Sinn. Warum sollte ich eigentlich planen?](https://www.youtube.com/watch?v=9nliP9mefXo&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=10)
- (4:31 min) [Der Planungsprozess. Wie kann ich strukturiert planen?](https://www.youtube.com/watch?v=1wbtRrJiPVA&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=11)

## 3 Strukturplan und Ablaufplan
- (3:25 min) [Der Projektstrukturplan - Wie unterteilen wir unser Projekt- (Teil 1)](https://www.youtube.com/watch?v=mTgMiIMHpW0&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO)
- (4:55 min) [Der Projektstrukturplan - Wie unterteilen wir unser Projekt- (Teil 2)](https://www.youtube.com/watch?v=VWp6W-KAQ-A&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=12)
- (3:16 min) [Der Projektablaufplan (Teil 1) - den fachlichen Ablauf festlegen](https://www.youtube.com/watch?v=dotv2MKuN0M&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=13)
- (3:14 min) [Der Projektablaufplan (Teil 2) - den fachlichen Ablauf festlegen](https://www.youtube.com/watch?v=LlHSf42FDXA&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=14)

## 4 Netzplantechnik
- (25 min) [**Tutorial**: Netzplantechnik und Durchlaufterminierung](https://www.youtube.com/watch?v=HkXlzWYBBAU)
- (15 min) [Entwicklung Netzplan - Projektmanagement](https://www.youtube.com/watch?v=fYoZErkiMTk)
- (11 min) [Netzplantechnik erklärt](https://www.youtube.com/watch?v=bUUrnbjKSbs)

## 5 GANTT-Diagramm, Balken-Diagramm
- (13 min) [Anleitung, wie man in Excel ein "richtiges" GANTT macht](https://www.youtube.com/watch?v=DGxL2ynLexw)
- (15 min) [Excel - Gantt-Diagramm mit Meilensteinen und Ressourcen](https://www.youtube.com/watch?v=vsgI2VOVGuQ)

## 6 Critical Path Method (CPM), Netzplan und GANTT
- (4:33 min) [Vergleich von Netzplan und GANTT Diagramm](https://www.youtube.com/watch?v=PkZhHaxyZR4)
- (3:30 min) [Developing a Basic Gantt Chart - Bar Chart Using Critical Path Method (CPM)](https://www.youtube.com/watch?v=mCXyUiexj0o)

## 7 Lastenheft, Pflichtenheft
- [Was ist ein Lastenheft, was ein Pflichtenheft](https://www.peterjohann-consulting.de/lastenheft-und-pflichtenheft/)
- [Das Pflichtenheft](https://www.ionos.de/digitalguide/websites/web-entwicklung/pflichtenheft)
- (5:51 min) [Lastenheft und Pflichtenheft (Teil 1)](https://www.youtube.com/watch?v=Wim8hgTv_OQ&t=134s)
- (4:24 min) [Lastenheft und Pflichtenheft (Teil 2)](https://www.youtube.com/watch?v=hlltEc5k4F4)
- (13 min) [Lasten- und Pflichtenheft. Warum brauche ich das z.B. als Webdesigner?](https://www.youtube.com/watch?v=-4Tq83wZFw8&t=1s)

## Auftrag / Aufgabe / Übung
- a.) Aufgabe/Übung Projektplanung Quellfassung [**.docx**](../uebungen/M306_2_Uebung-Projektplanung-Quellfassung.docx) / [.pdf](../uebungen/M306_2_Uebung-Projektplanung-Quellfassung.pdf)
- b.) Aufgabe/Übung Projektplanung "Migration Smartphone" [**.docx**](../uebungen/M306_2_Aufgabe_MigrationSmartphone_Projektplanung.docx)
- -- Template für die Aufgabe [**.docx**](../uebungen/M306_2_Aufgabe_Projektplanung_Template.docx)
- -- [**Tutorial**: Netzplantechnik und Durchlaufterminierung (25 min)](https://www.youtube.com/watch?v=HkXlzWYBBAU)
