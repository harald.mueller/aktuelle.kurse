Planung - Vorwissenaktivierung/PräInstruktion. 
(Jeder für sich, schriftlich.  Vermutungen sind auch gut)


1.) Wenn mehrere Personen zur Verfügung stehen, 
    wie kann man als Projektleiter alle beteiligen?
	
	
2.) Ganz generell: Wie kann man eine grosse Aufgabe planen?


3.) Welche Planungsobjekte/-elemente sind zu planen?


4.) Was zeigt ein Gantt-Diagramm und wie könnte man es noch nennen?


5.) Was bedeutet der "Kritische Pfad" in einer Aufgabenplanung?
