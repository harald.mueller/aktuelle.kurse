# M156 Neue Services entwickeln und Einführung planen

[**Modulidentifikation** ICT CH](https://www.modulbaukasten.ch/module/156/2/de-DE?title=Neue-Services-entwickeln-und-Einf%C3%BChrung-planen)

## Unterlagen

https://gitlab.com/ch-tbz-it/Stud/m156

- [Bücher](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Unterlagen/01-Buecher-Compendio?ref_type=heads)
- [ITIL](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Unterlagen/02-ITIL?ref_type=heads)
- [FitSM](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Unterlagen/03-FitSM?ref_type=heads)
- [BPMN](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Unterlagen/04-BPMN?ref_type=heads)
- [SLA](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Unterlagen/05-SLA?ref_type=heads)


[Fallstudien](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Fallstudien?ref_type=heads)


## Bewertung / Modulnote

- LB1: 33% Lernprodukt "Service Management"
- LB2: 33% Offerte als Vorabgabe des IT-Service (aus Fallstudie)
- LB3: 34% Umsetzung des IT-Services inkl. Support usw.


Alle 3 Notenbestandteile sind in Gruppen zu 3 (ausnahmsweise 2 Personen) zu erarbeiten. **In der Regel** bekommen **alle im TEAM die gleiche Note**. Die Lehrperson behält sich jedoch vor, die Team-Arbeiten individuell zu bewerten, sodass im Team unterschiedliche Noten zustande kommen.


## Ablaufplan 2023/24-Q1

| Tag | ST20d    | Thema, Auftrag, Übung |
|---- |----      |----                   |
| 1.) | Freitag 25.08.| [Was ist Service Management? Lernprodukt](lernprodukt-service-management.md)<br>Besprechnung 1 "Service Management"<br>Festlegung und Konzeption des Lernprodukts |
| 2.) | Freitag 01.09.| Arbeit am Lernprodukt<br>Besprechung 2 "Service Management" <br>(Konzeption/Story-Board/Skript des Lernprodukts LP & Team) |
| 3.) | Freitag 08.09.| Arbeit am Lernprodukt<br>Abgabe/Vorführung des Lernprodukts (**LB1**) |
| 4.) | Freitag 15.09.| Input: "Wir entwickeln einen neuen Service" <br>[LIMPIO-Reinigungen](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Fallstudien/LIMPIO-Reinigungen) |
| 5.) | Freitag 22.09.| Input: "Offerte" und Planung<br>Arbeit an der Offerte & Planung |
| 6.) | Freitag 29.09.| Input: "Service" <br>Arbeit an der Offerte & Planung |
| 7.) | Freitag 06.10.| Abgabe "Offerte" (und Planung) als Notengespräch am PC mit Team/Lehrperson (**LB2**)<br>Arbeit an der Umsetzung |
| - - | Ferien   | - - |
| 8.) | Freitag 27.10.| Arbeit an der Umsetzung |
| 9.) | Freitag 03.11.| Arbeit an der Umsetzung |
| 10.)| Freitag 10.11.| Vorführung und Benotung der Umsetzung des Services und Abschluss (**LB3**) |

