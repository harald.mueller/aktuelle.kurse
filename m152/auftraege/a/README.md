# Aufgabe M152-a: Webspace buchen und erste Website erstellen

_**Beginn Ihrer am ende des Moduls abzugebenden Webseite**_

_**Sie können einander helfen, am Schluss haben alle ihre eigene Webseite**_

Zeitbedarf: für 1.) - 5.) ca. 45 min (wenn alles gut geht), sonst vielleicht 90 min. Für den Rest mindestens 120 min (zieht sich über die ganze Dauer des Modules)



1.) Buchen Sie bei (z.B.) https://www.bplaced.net/freestyle, oder bei einem ähnlichen Gratis-Proider, Ihre eigene Subdomain (Angebot "freestyle" bei bplaced.net für EUR 0.--) und nennen Sie Ihr Account "*vornamenachname*", damit Ihr Webspace dann nachher http://vornamenachname.bplaced.net heisst

 
2.) Erstellen Sie / kontrollieren Sie Ihren automatisch dazugehörigen FTP-Account. (Das Passwort ist bei bplaced.net dasselbe, wie Sie für den Space-Account eingegeben hatten)


3.) Erstellen Sie eine erste HTML-Seite. mit dem Namen "index.html" und auf der oberen linken Ecke steht Ihr Vorname und Ihr Nachname (minimalste Anforderung für heute)

 
4.) Laden Sie die Seite index.html auf Ihren neuen WebSpace.


5.) Schreiben Sie ins BSCW den endgültigen URL.

----

6.) Holen Sie sich ein HTML5-Template 
- https://freehtml5.co, https://gettemplates.co ($17)
- https://html5up.net
- https://webflow.com/free-website-templates
- https://nicepage.com/html5-template
- https://speckyboy.com/free-responsive-html5-web-templates

und beginnen Sie sie zu **individualisieren** mit *eigenen Farben* (im CSS), *_eigenen_ Texten* (im HTML), *_eigenen_ Fotos* und *_eigenen_ Logos*.
_**Dieser Teilauftrag Nr. 6 wird sich übers ganze Modul ziehen. Arbeiten Sie immer wieder daran**_
