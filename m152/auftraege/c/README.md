# Aufgabe M152-c: Eigener Styleguide definieren/bestimmen und dokumentieren
Zeitbedarf: ca. 100-120 min


1.) Erstellen und verlinken Sie eine HTML-Seite namens "styleguide.html" (oder "styleguide.php") und definieren Sie Ihren eigenen Styleguide für Ihre Webseite. Das heisst, Sie bestimmen:
 - Schriftart und -grösse
 - Farben-Set
 - Logo und evtl. andere Erkennungsmerkmale
 - Icons & Schaltflächen (Form, Grösse, Verhalten (z.B. hover-Verhalten)
 - Abstände (bei Absätzen, bei Tabellen, um Grafiken herum, ... , ... )
 - Definieren Sie Header und Footer Ihrer Webseite. 
 
  2.) Erstellen und verlinken Sie eine Beispielseite "styleguide_examle.html", die alles was definiert wurde, "richtig" verwenden.


**Bemerkung 1**: Wenn Sie Bootstrap verwenden, machen Sie eine eigene CSS-Datei und überschreiben Sie dort die Defaultfarben der Schaltflächen (die Defaultfarben sind ja langweilig und benutzen alle)


**Bemerkung 2**: Wenn Sie ein vorgefertigtes Template benutzen, machen Sie entsprechend eine Nach-Analyse dieser oben beschriebenen Punkte samt Ihren Ergänzungen, Erweiterungen und Nach-Justierungen. Sie können auch die oftmals im Template angegebenen Styleguide-Seite benutzen und für Ihre Zwecke anwenden und/oder umbauen und nachdefinieren. Benützen Sie dafür das kaskadierende Konzept der CSS (überschreiben mit dem zuletzt Definiertem).
 
**Bemerkung 3**: Wenn Sie schlau sind, werden Sie dann immer die gleichen Header und Footer via PHP (include) immer in der gleichen Form einbinden.



Lassen Sie sich anleiten von diesen Dateien 

- [Beispiele für einen perfekten Styleguide](https://t3n.de/news/styleguide-beispiele-794708/)
- [den-perfekten-styleguide-fuer-eine-website-erstellen](https://www.ionos.de/digitalguide/websites/webdesign/den-perfekten-styleguide-fuer-eine-website-erstellen)
- [Style Guide erstellen leicht gemacht](https://99designs.ch/blog/design-kreativitaet/style-guierstellen)
- [Tools für Styleguides zu erstellen](https://t3n.de/news/styleguide-erstellen-tools-800688)
- [snowflake.ch/ux-design](https://www.snowflake.ch/ux-design)

