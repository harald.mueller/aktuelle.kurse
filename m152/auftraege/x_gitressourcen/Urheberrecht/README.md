## Urheberrecht

- [Bundesgesetz über das Urheberrecht und verwandte Schutzrechte](https://www.admin.ch/opc/de/classified-compilation/19920251/)
- [Eidgenössisches Institut für Geistiges Eigentum (IGE)](https://www.ige.ch/de.html)
- [ige.ch/fotografienschutz](https://www.ige.ch/fotografienschutz)
- [Impressumspflicht Schweiz (WEKA-Artikel 2018)](https://www.weka.ch/themen/marketing-verkauf/online-marketing/e-commerce/article/impressumspflicht-schweiz-so-erstellen-sie-rechtssichere-websites)
- [Urheberrecht (Schweiz)](https://de.wikipedia.org/wiki/Urheberrecht_(Schweiz))
- [Urheberrechtsgesetz (Schweiz)](https://de.wikipedia.org/wiki/Urheberrechtsgesetz_(Schweiz))

