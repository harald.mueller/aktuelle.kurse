## CRC-Cards

[**M326_Tag2_task_defining_requirementsUseCases.pdf**](./M326_Tag2_task_defining_requirementsUseCases.pdf)

[https://echeung.me/crcmaker](https://echeung.me/crcmaker)

- [2:49 min, E, YouTube: Using CRC-Cards](https://www.youtube.com/watch?v=Bxgn6qJ-bYY)
- [8:59 min, E, YouTube: Class Responsibility Collaboration](https://www.youtube.com/watch?v=mbpeonZUhpU)
- [11:06 min, E, YouTube: CRC Card Analysis Example](https://www.youtube.com/watch?v=otKUer13HnA)
- [8:39 min, E, YouTube: How to make CRC 'class responsibility collaborations' cards - explained with example](https://www.youtube.com/watch?v=59tkQ-FwcpA)
