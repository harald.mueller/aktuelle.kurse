[TOC]


# M326 - Objektorientiert entwerfen und implementieren

**Eine objektorientierte Analyse (OOA) in ein objektorientiertes Design (OOD) 
überführen, implementieren, testen und dokumentieren.**

**Moduldefinition**
[.html](https://www.modulbaukasten.ch/module/326/4/de-DE?title=Objektorientiert-entwerfen-und-implementieren)
/ [.pdf](https://modulbaukasten.ch/Module/326_4_Objektorientiert%20entwerfen%20und%20implementieren.pdf)


## Bewertung
| Teile                 | Details      |
| ----                  | ----      | 
| **25% Prüfung** | Über UML und CRC - OnlineTest am 4. Tag |
| **55% Projekt**       | Als Teamarbeit zu 3 Pers.<br>Applikation mit 3-5 fachlichen Klassen (z.B. Bibliothek, Wertschriftendepot, Börsenticker, Kinobuchungssystem usw.) <br>[Benotungsraster, Notengespräch](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/EQV_tUxxzj1FqmflrcTAgNoBtFlr5xF0Q6HppCM4SLjQvQ?e=SIuxdj) |
| **20% Vortrag** | Vortrag 10-12 min über Design Patterns im 2-er-Team <br> Inhalt: A: Theoretisches / B: Code-Demo / C: Live-Demo anhand eines Beispiels <br> - [intro_designPatterns.pdf](./1b_Einfuehrung_Klassen-finden/Input-Design/intro_designPatterns.pdf) <br> - [https://en.wikipedia.org/wiki/Software_design_pattern](https://en.wikipedia.org/wiki/Software_design_pattern) <br> - [Vortragsliste](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/Eavs1GQwwtBCvNONi2wkOY4B7Phm6ce_mLa5gjueM0dDvA?e=kACLXP)   (eintragen bis 9.6.) <br> <br> - Info: [10 Dinge für eine gute Präsentation](https://wb-web.de/material/medien/10-dinge-die-sie-bei-prasentationen-dringend-beachten-sollten.html) / [Wie halte ich einen Vortrag (.pdf)](./WieHalteIchEinenVortrag.pdf) <br> - Bewertungsraster: [**Online**](https://forms.office.com/pages/responsepage.aspx?id=xf0z91USjU23kyvHrKDyFLaxCwQ4iDJAjLw4mZ8X_R5UNkVOSUxKTlNOQlo3V01KUVpEVTNYOEtTSy4u) [Dokument](./Vortragsbewertung.pdf) |



## Termine AP20c (Donnerstag morgens)

| Tag  | Datum | Wichtiges             | Thema       |
| ---- | ----  | ----                  | ----        |
|  1   | 19.5. |                       | Analyse, CRC, Reengineering, Projektdefinition in ausformulierten Sätzen |
|  -   | 26.5. | _fällt aus_           | UML in Selbstinstruktion <br> [Hausaufgabe: (Videos, ca. 1 Std)]((2b_UML-Diagramme)) |
|  2   |  2.6. |                       | [LSA-T2](./M326_Tag2_Lernstandanalyse.txt) <br> [Uebersicht UML](./M326_Tag2_Uebersicht.jpg) <br> Verben-/Substantiven-Methode, [10 Schritte zum OOA-Modell](./1b_Einfuehrung_Klassen-finden/Lösungs_Beispiel_01.pdf) <br> Projekt-Ausarbeitung|
|  3   |  9.6. |                       | [LSA-T3](./M326_Tag3_Lernstandanalyse.txt) <br> Projektarbeit / Implememtierung|
|  4   | 16.6. | UML-/CRC-Test (30 Min)| Projektbearbeitung |
|  5   | 23.6. | Vortrag-Serie-1       | Projektbearbeitung |
|  6   | 30.6. | Vortrag-Serie-2       | Projektbearbeitung |
|  7   |  7.7. | **Projektabgabe**     | |
|  -   | 14.7. | _fällt aus_           | Sporttag (ist pflicht) |


<br>
<br>
<br>
<br>*M326_Tag1-Tag2-Hausaufgabe.jpg*
<br>
![UML Tag1-Tag2-Hausaufgabe](./M326_Tag1-Tag2-Hausaufgabe.jpg)

<br>
<br>
<br>
<br>
<br>
<br>
<br>*M326_Tag2_Uebersicht.jpg*
<br>
![UML Übersicht](./M326_Tag2_Uebersicht.jpg)
