# M450 Applikationen testen

Inhaltsverzeichnis

[TOC]

## Moduldefinition

[**Modulidentifikation** ICT CH](https://www.modulbaukasten.ch/module/450/1/de-DE?title=Applikationen-testen), [450_1.pdf](https://modulbaukasten.ch/Module/450_1_Applikationen%20testen.pdf)

[weitere TBZ Unterlagen -> https://gitlab.com/ch-tbz-it/Stud/m450/m450](https://gitlab.com/ch-tbz-it/Stud/m450/m450)


## Leistungsbeurteilungen (Prüfungen)

- 20% Engagement in den Übungen
- 30% Theorieprüfung [Lehrziele, Inhalte](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/pruefung/README.md)
- 50% Projektvorgehen (Fokus Testing)


## Anforderungen an das Projekt

Projekt für 2 Personen

- [Grundaufbau](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/projekt/grundaufbau.md)
	- [Test Driven Development](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/projekt/tdd.md)
	- [CI/CD Pipelines](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/projekt/ci-cd.md)
	- [Code Reviews](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/projekt/code-reviews.md)

- [Teams-Repos WUP23](teamsWUP23.md)

## Ablaufplan 2024-Q1 (Mi morgens)

|Tag  |WUP23   |Thema, Auftrag, Übung |
|---- |----    |----                  |
| 1 | Mi 21.08.| [Modulstart & Übersicht](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/modulstart), GitLab-Projekt anlegen<br> &#x1F4E5; [Grundlagen zum Testing und "Testing im Vorgehensmodell"](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/grundlagen) <br> &#x1F4DD; [Übungen](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/grundlagen/UEBUNGEN.md) <br> &#x1F4BC; [Aufgaben im Projekt](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/projekt/grundaufbau.md)|
| 2 | Mi 28.08.| &#x1F4E5; [Teststrategie(n)](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/teststrategie)<br> &#x1F4DD; [Übungen](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/teststrategie/UEBUNGEN.md) |
| 3 | Mi 04.09.| &#x1F4E5; [Testlevels](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/test-levels), &#x1F4E5; [Unit-Tests](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/unit-testing)<br> &#x1F4DD; [Übungen](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/unit-testing/UEBUNGEN.md)|
| 4 | Mi 11.09.| &#x1F4E5; [Abhängigkeiten zu Schnittstellen](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/schnittstellen/README.md)<br> &#x1F4DD; [Übungen](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/schnittstellen/UEBUNGEN.md)|
| 5 | Mi 18.09.| &#x1F4E5; [Automatische Tests und Testing-Tools](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/automation-testing/README.md)<br> &#x1F4DD; [Übungen](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/automation-testing/UEBUNGEN.md) <br> &#x1F4BC; [Aufgaben im Projekt](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/projekt/tdd.md) |
| 6 | Mi 25.09.| **Theorieprüfung** ca. 60 min um 08:15 <br><br> &#x1F4E5; [Testkonzept](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/testkonzept/README.md) <br> &#x1F4DD; [Übungen](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/testkonzept/UEBUNGEN.md) |
| 7 | Mi 02.10.| &#x1F4E5; [CI/CD Pipelines](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/ci-cd-pipeline/README.md) <br> &#x1F4E5; [Verteilung/Deployment](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/deployment-environment/README.md)<br> &#x1F4DD; [Übungen](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/ci-cd-pipeline/UEBUNGEN.md) <br> &#x1F4BC; [Aufgaben im Projekt](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/projekt/ci-cd.md) |
| - | Ferien   | - |
| - | Ferien   | - |
| 8 | Mi 23.10.| &#x1F4E5; [Code Reviews](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/code-reviews/README.md)<br> &#x1F4DD; [Übungen](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/code-reviews/UEBUNGEN.md) <br> &#x1F4BC; [Aufgaben im Projekt](https://gitlab.com/ch-tbz-it/Stud/m450/m450/-/blob/main/Unterlagen/projekt/code-reviews.md) |
|10 | Mi 30.10.| &#x1F4BB; **Präsentationen und Notenvergabe** &#x1F3C5; &#x1F44F; |

## Abgabe

[Tagesplan zur Abgabe](https://tbzedu-my.sharepoint.com/:x:/r/personal/harald_mueller_tbz_ch/_layouts/15/Doc.aspx?sourcedoc=%7B79646542-85E2-422C-ABC0-FDDA3EA9658D%7D&file=Tagesplan-zur-Abgabe.xlsx&fromShare=true&action=default&mobileredirect=true)

## Kompetenzraster

![](x-ressources/kompetenzraster.png)



@ Harald Müller, August 2024

