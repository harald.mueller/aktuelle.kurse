# Teamzusammensetzung

Aug.2024 - Nov.2024

| Team | Namen & Repo |
|----  |----          |
| T1   | [Gyekagong, W&auml;lti](https://gitlab.com/TenzinGyekagong/m450-applikationen-testen) |
| T2   | [Frey, Graf](https://gitlab.com/breakfastgre/testris)      |
| T3   | [Carmen M. Marti](https://gitlab.com/ca_marti/m450-testing)  |
| T4   | [Bucher](https://gitlab.com/marbuctbz/m450-testing-uebungen), [Sch&ouml;nenberger](https://gitlab.com/rschoenenber_tbz/m450-applikationen-testen), [ram](https://gitlab.com/rschoenenber_tbz/projekt-applikationen-testen-ram) |
| T5   | [Yesilgueller, Jafari](https://gitlab.com/MorayYesilgueller/m450-testing)|
| T6   | [Frehner, Altenburger](https://gitlab.com/JoaFre/M450_Dokumentation_Joa_Frehner_Timon_Altenburger) |
| T7   | [Bosshard, Evangelista](https://gitlab.com/tbz7623825/m450-applikation-testing)  |

Altenburger	Timon
, Bosshard	Benjamin
, Bucher	Marina
, Evangelista	Alessia
, Frehner	Joa Elia
, Frey	Gregory Lionel
, Graf	Mike Lee
, Gyekagong	Tenzin Kunsel
, Jafari	Nasrin
, Marti	Carmen Maria
, Schoenenberger	Ramona
, Waelti	Sandro Oliver
, Yesilgueller	Moray
