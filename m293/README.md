# m293 Webauftritt erstellen und veröffentlichen

## Unterlagen

- Schweiz 
	- [https://www.modulbaukasten.ch/](https://www.modulbaukasten.ch/)
	- [Moduldefinition](https://www.modulbaukasten.ch/module/04d7591d-6885-eb11-a812-0022486f644f/de-DE?title=Webauftritt-erstellen-und-ver%C3%B6ffentlichen)

- Kanton ZH 
	- [https://gitlab.com/modulentwicklungzh/cluster-api/m293](https://gitlab.com/modulentwicklungzh/cluster-api/m293)
	- [Kompetenzmatrix ZH](https://gitlab.com/modulentwicklungzh/cluster-api/m293/-/tree/master/1_Kompetenzmatrix)

- TBZ 
	- für Lehrpersonen [https://gitlab.com/ch-tbz-it/TE/m293](https://gitlab.com/ch-tbz-it/TE/m293)
	- [Konzept (Miro-Board)](https://miro.com/app/board/uXjVOYAH2vc=/?invite_link_id=469435731667)
	- für Schüler [https://gitlab.com/ch-tbz-it/Stud/m293](https://gitlab.com/ch-tbz-it/Stud/m293)
	- [Modulübersicht/Arbeitsoberfläche MIRO AP21b](https://miro.com/app/board/uXjVONmQNmQ=/?invite_link_id=264004499021)
	- [Modulübersicht/Arbeitsoberfläche MIRO AP21a](https://miro.com/app/board/uXjVONnmuCk=/?invite_link_id=696015461275)


