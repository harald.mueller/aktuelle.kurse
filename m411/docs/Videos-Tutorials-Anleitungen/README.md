## M411 Videos-Tutorials-Anleitungen

### Allgemeines
 - Algorithmen in 3 Minuten erklärt [3:49 min, D, 2019, YouTube](https://www.youtube.com/watch?v=FBUoEumkP2w)
 - Was ist ein Algorithmus - Algorithmen verstehen [8:43 min, D, 2020, YouTube, Channel: Algorithmen verstehen](https://www.youtube.com/watch?v=GoIACw9ARCM)
 - Was sind Algorithmen [4:24 min, D, 2018, YouTube, Channel: iMooX at](https://www.youtube.com/watch?v=Z0WvGofejVg)
 - 7 DATA STRUCTURES you MUST know (as a Software Developer) [7:22 min, E, 2019, YouTube, Channel: Aaron Jack](https://www.youtube.com/watch?v=sVxBVvlnJsM)
 - How I Got Good at Algorithms and Data Structures [11:23 min, E, 2020, YouTube, Channel: Marc White](https://www.youtube.com/watch?v=9-ubSA9GA3o)

### Videoreihen (PlayLists)
 - Algorithmen und Datenstrukturen - leicht erklärt [PlayList #1 bis #49, D, 2020, YouTube (öffentlich, evtl. mit Werbung)](https://www.youtube.com/watch?v=eXjay16RMw0&list=PLNmsVeXQZj7q2hZHyLJS6IeHQIlyEgKqf)
 - Data Structures and Algorithms [PlayList #1 bis #9, E, 2018-2020, YouTube, Channel CS Dojo](https://www.youtube.com/watch?v=bum_19loj9A&list=PLBZBJbE_rGRV8D7XZ08LK6z-4zPoWzu5H)

### Hash Maps
 - Hash Tables and Hash Functions [13:53 min, E, 2017, YouTube, Computer Science](https://www.youtube.com/watch?v=KyUTuwz_b7Q)
 - HashMap Java Tutorial [11:41 min, E, 2019, YouTube, Alex Lee](https://www.youtube.com/watch?v=70qy6_gw1Hc)
 - HashMap Java Tutorial [10:18 min, 2013, D, YouTube, JavaWebAndMore](https://www.youtube.com/watch?v=XnPTJIQhsiY)
 - Java Tutorial - HashMap und TreeMap [10:06 min, D, 2015, YouTube, Morpheus](https://www.youtube.com/watch?v=xeZkdm7tE3s)

### LinkedLists
 - Algorithmen und Datenstrukturen #7 - Listen (Linked Lists und Double Linked Lists) [8:52 min, D, YouTube, 2020, The Morpheus Tutorials](https://www.youtube.com/watch?v=i2v_Ve9PUCw)

### Pagerank (pat. Google-Algorithmus)
 - PageRank (Googles patentierter Algorithmis) [21:36 min, D, 2013, YouTube, Channel: Mathegym](https://www.youtube.com/watch?v=IKX66kchWMQ)
 - PageRank - Intro to Computer Science [5:06 min, E, 2015, YouTube, Channel: Udacity](https://www.youtube.com/watch?v=IKXvSKaI2Ko)
 - PageRank Algorithm - Example [10:10 min, E, 2017, YouTube, Channel: Global Software Support](https://www.youtube.com/watch?v=P8Kt6Abq_rM)
 - PageRanking and Search Engines [9:30 min, E, 2015, YouTube, Channel: Computerphile](https://www.youtube.com/watch?v=v7n7wZhHJj8)
 - The algorithm that started google [13:39 min, E, 2019, YouTube, Channel: Zach Star](https://www.youtube.com/watch?v=qxEkY8OScYY)

### Rekursionen
 - Rekursion (Towers of Hanoi, in Python) [12:17 min, E, 2020, YouTube, Channel: Computerphile](https://www.youtube.com/watch?v=8lhxIOAfDss)
 - Rekursion vs Loops [12:31 min, E, 2017, Channel: Computerphile](https://www.youtube.com/watch?v=HXNhEYqFo0o)
 - Rekursion, What on Earth is this [9:39 min, E, 2014, YouTube, Channel Computerphile](https://www.youtube.com/watch?v=Mv9NEXX1VHc)

### Routenplanungs_Algorithmus (Dijkstra)
 - Routenplaner (Dijkstra Algorithmus) [6:30 min, D, 2019, YouTube, Channel: Algorithmen verstehen](https://www.youtube.com/watch?v=KiOso3VE-vI)
 - Routenplaner (Dijkstra Algorithmus) [8:00 min, D, 2016, YouTube, Channel: Bleeptrack](https://www.youtube.com/watch?v=2poq1Pt32oE)
 - Routenplaner (Dijkstra Algorithmus) [10:42 min, E, 2017, YouTube, Channel: Computerphile](https://www.youtube.com/watch?v=GazC3A4OQTE)
 - [routenplaner_schweizerkarte_staedteverbindungen.zip](./routenplaner_schweizerkarte_staedteverbindungen.zip)

### Sudoku Solver
 - Sudoku Solver (in Python, mit Rekursion) [10:52 min, E, 2020, YouTube, Channel Computerphile]( https://www.youtube.com/watch?v=G_UYXzGuqvM)

### Trees
 - Trees in Python [21:26 min, E, 2020, YouTube, Channel: Computerphile]( https://www.youtube.com/watch?v=7tCNu4CnjVc)