### Aufgabe/Task: Nr. 14

Thema: Page Ranking Algorithmus  
(googles patent algorithm)

Geschätzter Zeitbedarf: 90-120 min

Aufgabenbeschreibung:

Schauen Sie sich 2-3 dieser Videos auf der Liste an und beschreiben Sie
anschliessend im Detail mit eigenen Worten, wie der Algorithmus funktioniert.
Erwartet wird etwa eine A4-Seite (in 11 Pt Schrift)

<https://bscw.tbz.ch/bscw/bscw.cgi/31933102>


- 13:39 min, E, 2019, YouTube, Channel: Zach Star [The algorithm that started google](https://www.youtube.com/watch?v=qxEkY8OScYY)
- 21:36 min, D, 2013, YouTube, Channel: Mathegym [PageRank (Googles patentierter Algorithmis)](https://www.youtube.com/watch?v=IKX66kchWMQ)
- 9:30 min, E, 2015, YouTube, Channel: Computerphile [PageRanking and Search Engines](https://www.youtube.com/watch?v=v7n7wZhHJj8)
- 8:44 m in, E, 2018, YouTube, Channel: Intrigano [Linear Algebra – Introduction to PageRank](https://www.bing.com/videos/search?q=Introduction+to+PageRank+-+Linear+Algebra&&view=detail&mid=E3543A1D26FE2A629604E3543A1D26FE2A629604&&FORM=VRDGAR&ru=%2Fvideos%2Fsearch%3Fq%3DIntroduction%2Bto%2BPageRank%2B-%2BLinear%2BAlgebra%26qpvt%3DIntroduction%2Bto%2BPageRank%2B-%2BLinear%2BAlgebra%26FORM%3DVDRE)
- 5:06 min, E, 2015, YouTube, Channel: Udacity [PageRank - Intro to Computer Science](https://www.youtube.com/watch?v=IKXvSKaI2Ko)
- 10:10 min, E, 2017, YouTube, Channel: Global Software Support [PageRank Algorithm - Example](https://www.youtube.com/watch?v=P8Kt6Abq_rM)


Bewertung: Keine
