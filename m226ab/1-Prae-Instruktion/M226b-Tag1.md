# Präinstruktion M226b Tag 1

## Spielregel
	Beantworten Sie *schriftlich* und alleine für sich,
	die folgenden Fragen. Recherchieren Sie vorerst nicht!
	Vermutungen sind auch gut.
	
	Nach 15 min. können Sie im Internet
	oder in den Unterlagen recherchieren.
	Das Gespräch (Murmelrunde) mit dem/den
	Nachbarn ist in dieser Phase gut, hilfreich und erwünscht!

## Fragen
- Was bedeutet gemeinhin das Wort "dynamisch"? Wo kommt das im Alltag vor?
- Wie wird eine Vererbung im UML-Klassendiagramm gezeichnet und von wo nach wo zeigt der Pfeil?
- Versuchen Sie Wörter zu finden, die mit "poly" beginnen und was heisst wohl diese griechische Vorsilbe?
- Was ist das Schlüsselwort (keyword) in Java für eine Vererbung? 
- Was heisst das Wort "unit" auf deutsch?
- Wie schreiben Sie in Java eine Klassendefinition auf, in der die Klasse "Flixbus" von der Klasse "Reisebusse" erbt?
- Welches der beiden ist die "Oberklasse"?
- Was heisst das Fremdwort "Morph" auf deutsch?
