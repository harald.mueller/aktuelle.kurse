package com.doerzbach;

public class Barometric1000PressureSensorImpl extends PressureSensor{

    @Override
    public String getName() {
        return "Barometer1000";
    }

    @Override
    public void doMeasurement() {
        measurementValue=0.5+0.55*Math.random();
    }
}
