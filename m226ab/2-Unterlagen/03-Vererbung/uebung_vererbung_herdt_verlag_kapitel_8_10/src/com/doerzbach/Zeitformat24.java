package com.doerzbach;

public class Zeitformat24 extends Zeitformat{

    public Zeitformat24(int hours,int minutes){
        setHours(hours);
        setMinutes(minutes);
    }

    @Override
    public void zeitAusgabe() {
        System.out.printf("%02d:%02d\n",getHours(),getMinutes());
    }
}
