package com.doerzbach;

import java.util.Date;

public class Mitarbeiter extends Person {
    private String ahvNummer;
    private java.util.Date anstellungsDatum;

    public Mitarbeiter(String username, String passwort, String nachName, String vorName, String emailAdresse, String telefonNummer, String mobileNummer, String ahvNummer, Date anstellungsDatum, Geschlecht geschlecht ) {
        super(username, passwort, nachName, vorName, emailAdresse, telefonNummer, mobileNummer, geschlecht);
        this.ahvNummer = ahvNummer;
        this.anstellungsDatum = anstellungsDatum;
    }

    @Override
    public String getAnrede() {
        return "Hallo "+getVorName();
    }

    @Override
    public String getPasswordPrompt() {
        return "Gib dein Passwort ein:";
    }
}
