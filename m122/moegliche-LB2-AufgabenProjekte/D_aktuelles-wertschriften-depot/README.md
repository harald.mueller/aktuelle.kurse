# M122 - Aufgabe

2024-05 MUH

## D Aktuelles Wertschriften-Depot

| Punkte |Beschreibung | 
|--------|------------ |
|    3   | Mehrere (mind. 8 verschiedene) aktuelle Kursdaten von Aktien sind heruntergeladen und mit den Assets verrechnet  |
|    2   | Depot-Wert wird ermittelt historisch & aktuell verglichen (Differenzen in %) |
|    3   | Die Depotwerte haben eine Zeitschreibung (man sieht über die verschiedenen Abrufzeiten, wie sich die Werte entwickelten)  |
|  **8** | **Total** |
|    1   | Eingechecked in GitLab, GitHub, BitBucket  |
|        |           |
| **Plagiat**  | Reduktion der Punkte nach Einschätzung des Lehrers, wenn der gleiche Code schon mal gesehen wurde  |
|        |   |

Machen Sie ein (fiktives) Aktien-, Fremdwährungs- 
und Krypto-Depot mit aktuellen Daten, die Sie aus 
Online-APIs abholen sollen.

https://mixedanalytics.com/blog/list-actually-free-open-no-auth-needed-apis
 
Also es soll jemand z.B. 10 Aktien von Novartis, Nestle, ABB, Swiscom usw.
3000 USD und z.B. 0.1 Bitcoins in einem Depot halten. 

Es wurde damals X Franken für die Novartis-Aktien bezahlt 
und Y Franken für die USD und Z Franken für Bitcoins bezahlt. 

Die Frage ist nun: 

- Was ist dieses Depot heute in CHF wert?
- Diese Werte-Entwicklung möchte ich gerne über die Zeit (regelmässig) verfolgen, also den Wert alle x Stunden oder Tage


<hr>

## Bewertung

