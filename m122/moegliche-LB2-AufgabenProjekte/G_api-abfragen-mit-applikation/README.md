# M122 - Aufgabe

2024-04 MUH

## G Applikation mit API-Abfrage

| Punkte | Beschreibung | 
|--------|--------------|
|     1  | Download und Verarbeitung der aktuellen Kurse (mindestens 6 verschiedene) |
|     1  | Gute und die "schöne" (tabellarische) Darstellung der Daten mit `printf()` |
|     3  | Speicherung der "alten" Daten und Vergleich mit den "neuen" Daten über mehrere Abrufe der Daten | 
| **4**  | **Total** | 
|     1  | Eingechecked in GitLab, GitHub, BitBucket  |
|     1  | Bonuspunkt für Farben und Differenz-Prozenten (%) in der Darstellung (rot für "runter", grün für "hoch") seit dem letzten Aufruf |
|        |   |
| **Plagiat**  | Reduktion der Punkte nach Einschätzung des Lehrers, wenn der gleiche Code schon mal gesehen wurde  |
|        |   |



Erstellen Sie eine Applikation nur auf der Console bei der 
ich einen Betrag in CHF dem Skript übergeben kann. 
Und dann will ich eine Umrechnung haben in

- Euro (EUR) und/oder US-Dollar (USD)
- Ethereum (ETH) und/oder Bitcoin (BTC)
- und mind 4 weitere Währungen, Aktien und/oder Crypto-Coins nach Ihrer Wahl

Benutzen Sie dafür die aktuellen Kurse über eine API.



Wenn ich nach einiger Zeit den gleichen Betrag wieder
eingebe, sollten Sie darstellen können, was der Betrag
vorher war und was er jetzt ist. 
(Sie müssen die abgefragten Werte speichern um sie dann 
vergleichen zu können. Weiter sollten Sie die Zeit wissen, 
wann das letzte Mal abgefragt wurde und die Differenz
möchte ich auch angezeigt bekommen.)


Hier einige mögliche APIs zum anbinden:

https://mixedanalytics.com/blog/list-actually-free-open-no-auth-needed-apis/

https://polygon.io/pricing

https://coinmap.org/api/v1/coins/

https://v6.exchangerate-api.com/v6/bb9ddd1855e84ded5fca04e0/latest/USD