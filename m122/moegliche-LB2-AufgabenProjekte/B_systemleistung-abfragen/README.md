# M122 - Aufgabe

2024-07 MUH

## B System-Leistung abfragen

Für eine automatische Systemüberwachung von Servern 
und/oder auch Clients sollen Sie "regelmässig" (cron)
eine Serie von Leistungs-Daten ausgeben.


| Punkte | Beschreibung | 
|-------|--------------|
|     3 | Alle genannten Sytem-Infos   |
|     1 | Ausgabe in die Datei mit "Switch" und richtigem Dateiname | 
|     1 | Regelmässige Ausführung (Abgabe von 3 Log-Einträgen, die das System im Takt erstellt hat)   |
|     1 | "Gute/schöne" Tabellen-Darstellung  mit `printf()`  |
| **6** | **Total** | 
|     1 | Eingechecked in GitLab, GitHub, BitBucket  |
|       | Erweiterungsmöglichkeiten   |
|     1 | Erkennen eines "Schwellwert"es, d.h. wenn ein bestimmter Wert überschritten wird, dann soll rot angezeigt werden, und sonst grün (auch andere Erkennungen möglich)  |
|     2 | Darstellung auf einer Webseite (HTML ->  als index.html in Ihr Verzeichnis, siehe Zugangsdaten oder auf Ihren Desktop auf /mnt/c/Users/nnnnn/Desktop ) für das Monitoring mit den Werten und zusätzlich einer Ampel-Darstellung grün/gelb/rot [**FTP-Zugangsdaten**](../../tools-technics/ftp-zugangsdaten.md)|
|     2 | E-Mail wird (an sich selber) verschickt - dies braucht aber Zusatzinstallation und Konfiguration eines Mailservers |
|     |   |
| **Plagiat**  | Reduktion der Punkte nach Einschätzung des Lehrers, wenn der gleiche Code schon mal gesehen wurde  |
|     |   |


### Aufgabenstellung

Verwenden dafür Bash- Shell-Scripting oder auch PowerShell
und stellen Sie sicher, dass es auf Ihrem System 
ausgeführt werden kann. 


**1.) Formattierte Inhalte**

Formattieren Sie alles in eine gut leserliche **Tabellen-Form**.

Folgendes ist auszugeben:

- 1. Die aktuelle Systemlaufzeit und aktuelle Systemzeit
- 2. Die Grösse des belegten und freien Speichers auf dem Dateisystem
- 3. Der Hostname imd IP-adresse des Systems
- 5. Die Betriebssystemname und -version
- 6. Der Modellname der CPU und die Anzahl der CPU-Cores
- 7. Der gesamte und der genutze Arbeitsspeicher
- Trenner und Abschluss der Tabelle für die nächste Ausgabe

**2.) Dateiausgabe wahlweise** (d.h. mit einem "switch", bzw. einer "option" `-f`)

Gefordert ist die Ausgabe **wahlweise** direkt auf das 
<br>Terminal, bzw. die Console, wie auch in eine Datei.

- Wenn man **keine Option** angibt: Nur die Terminal-Ausgabe ohne Datei.
- Wenn man den **"Switch"** (die Option) `-f` angibt, soll zusätzlich die 
<br>Datei **[YYYY-MM]-sys-[hostname].log** erzeugt werden.
<br>(Immer in die gleiche Datei schreiben. Das nennt man ein **"Log"**)

Tipp: Benutzen Sie für den Timestamp `date '+%Y-%m-%d'` bzw. `date '+%Y-%m'`
und für den Hostnamen `hostname` oder `uname -n` und den Befehl `df ` für Disk-Angaben.


**3.) Regelmässigkeit**

Binden Sie Ihr Skript in die `crontab` ein
und wählen Sie einen geeigneten Ausführungs-Takt.

Tipp: Prüfen Sie, ob Ihr **cron** eingeschaltenist mit folgendem Befehl:

	service cron status

Falls cron nicht läuft, können Sie es damit einschalten:

	sudo service cron start


 
### Resultat

Ihr Resultat sollte so in dieser Art aussehen (schöner ist besser):

| Text | Wert | 
|------|------|
| free disk space | 80 GB |
| free memory  | 07 GB | 
| ...  | ... | 
| ...  | ... | 
| ...  | ... | 

Tipp: Benutzen Sie den [`printf`-Befehl](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_007_001.htm#RxxKap00700104004E721F034268)



