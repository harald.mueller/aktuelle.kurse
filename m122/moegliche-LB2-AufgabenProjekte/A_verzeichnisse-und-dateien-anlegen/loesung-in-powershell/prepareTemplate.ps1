# Erstelle das Template-Verzeichnis und die Dateien

# Verzeichnis anlegen
mkdir _template -ErrorAction Ignore  

#Verzeichnis wechseln
cd .\_template

# Dateien im Verzeichnis anlegen, 
# jedoch nur mit Fake-Inhalt damit mal was da ist.
echo "datei-1.txt"  > datei-1.txt     
echo "datei-2.docx" > datei-2.docx
echo "datei-3.pdf"  > datei-3.pdf
echo "datei-4.xlsx" > datei-4.xlsx

# Verzeichnis zurück wechseln
cd ..

#############################################
# Namensdatei-Vorlage automatisch erstellen #
#############################################

# Ich speichere mal den Dateinamen in eine Variable,
# weil ich das mehrmals brauche

$namensdatei = "M122-BR23a.txt"

# ich mache die Namendatei gleich zur besseren 
# Ordnung in ein entsprechendes Verzeichnis

$verzeichnis_namensdateien = "_namensdateien"
mkdir $verzeichnis_namensdateien -ErrorAction Ignore  

cd $verzeichnis_namensdateien

# Namendatei anlegen und Inhalte reinschreiben
echo "Amherd"           > $namensdatei

# Zwei ">" (">>") hängt das neue Material unten an
echo "Baume-Schneider"  >> $namensdatei
echo "Berset"           >> $namensdatei
echo "Cassis"           >> $namensdatei
echo "Keller-Sutter"    >> $namensdatei
echo "Parmelin"         >> $namensdatei
echo "Roesti"           >> $namensdatei # Umlaute wie das "ö" sind mega schlecht


# Gleich noch eine zweite Namensdatei
$namensdatei = "M122-SR23a.txt"
echo "Burkhart" > $namensdatei
echo "Chiesa"   >> $namensdatei
echo "Jositsch" >> $namensdatei
echo "Noser"    >> $namensdatei

cd ..

