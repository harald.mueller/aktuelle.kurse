# erstellt alle Verzeichnisse und kopiert die 
# Template-Dateien in alle Teilnehmer aller Klassen

$verzeichnis_template = "_template"
$verzeichnis_namensdateien = "_namensdateien"

$allFiles = (dir $verzeichnis_namensdateien).name 

foreach ($file in $allFiles) {
    $fileNameVorPunkt = $file.split(".")[0]
    $klasse = $fileNameVorPunkt
    $fileinhalt = type $verzeichnis_namensdateien/$file
    foreach ($teilnehmer in $fileinhalt) {
        mkdir $verzeichnis_namensdateien/$klasse/$teilnehmer
        copy $verzeichnis_template/*.* $verzeichnis_namensdateien/$klasse/$teilnehmer
    }
}
