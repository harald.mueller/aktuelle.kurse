# M122 - Abläufe mit einer Scriptsprache automatisieren

[**Modulidentifikation (=Lernziele)**<br> https://www.modulbaukasten.ch/module/122/3/de-DE?title=Abl%C3%A4ufe-mit-einer-Scriptsprache-automatisieren](https://www.modulbaukasten.ch/module/122/3/de-DE?title=Abl%C3%A4ufe-mit-einer-Scriptsprache-automatisieren)


## Zusammensetzung der Modulnote (Leistungsbeurteilung):

- `30%` LB1 Bash-Kurs-Prüfung handschriftlich auf Papier 'closed book' und ohne Geräte (Zeitpunkt ist selber wählbar am 3. oder 4. Modul-Tag )
- `70%` LB2 Einzelaufgabe(n) mit vorgegebenen oder auch eigenen Definitionen.

## was Sie beachten

- Sie brauchen keine grafische Oberfläche. Die Maus legen Sie beiseite.
- Viel viel selber ausprobieren und Schritt für Schritt vorgehen. 
- Ohne üben geht gar nichts. Wer viel übt, wird schnell besser!
- Man muss alles mal selber gemacht haben, sonst können Sie nichts.
- Gemeinsames Recherchieren ist erwünscht. 
  (Jedoch müssen lauffähige Skripts von allen einzeln gezeigt werden.)
- Es sind nur Skript-Sprachen erlaubt. Also kein Java, C#, Kotlin usw.
- Alle Projekte (Skripte) haben "gute" Namen. 
  (sowas wie z.B. 'Aufgabe_B.sh' als Skriptname wird zurückgewiesen)
- Alle Variablen haben "gute" Namen!
- Alle Projekte werden auf eine Linux- oder Unix-Console gezeigt. Ausführungen in VSC werden nicht akzeptiert.
- Es werden für die LB2 Programme oder Skripte erwartet, die automatisiert (z.B. über den Scheduler / crontab gestartet) und "alleine" laufen können. Es sind also keine Games, keine Quizzes, keine Dialoge und auch keine Benutzerinteraktionen erwünscht. 
- Alle Skripte werden in GitHub, GitLab oder BitBucket eingecheckt.

## zur Vorbereitung

- Installieren Sie sich eine Linux-Umgebung<br> https://gitlab.com/ch-tbz-it/Stud/m122/-/tree/main/01_Linux_Einf?ref_type=heads#linux-installation

- Danach machen Sie folgende Nachinstallationen

```	
sudo apt-get upgrade
sudo apt-get update
sudo apt-get install tree
sudo apt-get install nano
sudo apt-get install git 
```	

## Lern-Unterlagen


### a.) Allgemeines

- https://de.wikipedia.org/wiki/Bash_(Shell)
- https://www.selflinux.org/selflinux/html/shellprogrammierung.html


### b.) Vorbereitung für die LB1

**Mit Erklärungen und Übungen** (empfohlen):

- [![selbstinstruktion](img/zur-selbstinstruktion.png) &rarr; &rarr; https://gitlab.com/ch-tbz-it/Stud/m122](https://gitlab.com/ch-tbz-it/Stud/m122) <br>und da machen Sie die ganzen Bash-Prüfungsvorbereitungen (Kap. 02-05) durch.<br>Beginnen Sie am Besten bitte mit einer **Installation von Linux** <br>für [**Windows**](https://gitlab.com/ch-tbz-it/Stud/m122/-/blob/main/01_Linux_Einf/README.md#linux-unter-windows) oder für [**Mac-OS**](https://gitlab.com/ch-tbz-it/Stud/m122/-/blob/main/01_Linux_Einf/README.md#unix-unter-macos) oder als eine [**Virtulle Machine**](https://gitlab.com/ch-tbz-it/Stud/m122/-/blob/main/01_Linux_Einf/README.md#eine-vm-installieren) 

- installieren von GIT: [&rarr; Eigene GIT-Umgebung (aus Modul M231)](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/10_Git/03_Eigene%20GIT%20Umgebung.md)
 
### c.) Referenzen, Nachschlagewerke

- https://openbook.rheinwerk-verlag.de/shell_programmierung (empfohlen)<br>
- https://openbook.rheinwerk-verlag.de/linux
- https://www.gnu.org/software/bash/manual/bash.html
- https://devhints.io/bash
- https://dev.to/awwsmm/101-bash-commands-and-tips-for-beginners-to-experts-30je

### d.) Weitere

- https://de.wikipedia.org/wiki/Cron
- [Ablage von Log-Files](https://www.cyberciti.biz/faq/linux-log-files-location-and-how-do-i-view-logs-files/)

<br><hr>


## Moegliche-LB2-Aufgaben und Projekte

  <br><br>[**Mögliche Aufgaben**](moegliche-LB2-AufgabenProjekte):
  
| Projekt | Punkte | Zusatz-<br>Bonus| Alleine-<br>Bonus | Aufgabenstellung |
|----     |----    |----  |----               |----     |
| **A**.) |  6     |    1 |   | [Dateien und Verzeichnisse anlegen](moegliche-LB2-AufgabenProjekte/A_verzeichnisse-und-dateien-anlegen)  |
| **B**.) |  6     |    5 | 1 | [Systemleistung abfragen](moegliche-LB2-AufgabenProjekte/B_systemleistung-abfragen)                      |
| **C**.) |  7     |    4 | 1 | [Emailadressen und Brief erstellen](moegliche-LB2-AufgabenProjekte/C_emailadressen-erzeugen)             |
| **D**.) |  8     |    1 | 1 | [Aktuelles Wertschriften-Depot](moegliche-LB2-AufgabenProjekte/D_aktuelles-wertschriften-depot)          |
| **E**.) |  6     |    1 | 1 | [QR-Rechnungen erzeugen lassen](moegliche-LB2-AufgabenProjekte/E_qr-rechnungen-erzeugen)                 |
| **F**.) |  5     |    6 | 1 | [APIs-Abfragen mit Datendarstellung](moegliche-LB2-AufgabenProjekte/F_api-abfragen-mit-datendarstellung) |
| **G**.) |  5     |    2 | 1 | [API abfragen mit Applikation](moegliche-LB2-AufgabenProjekte/G_api-abfragen-mit-applikation)            |
| **H**.) | 5-8    |    + | 1 | [Automatisierte Installation](moegliche-LB2-AufgabenProjekte/H_automatisierte-Installation)              |  
| **XX**  |  ?     |      | 1 | Eigene Projekte möglich. <br> Lassen Sie sich inspirieren von: [Bundesamt für Statistik BFS](https://www.bfs.admin.ch/bfs/de/home.html) oder [Statistik & Daten Kanton Zürich](https://www.zh.ch/de/politik-staat/statistik-daten.html) oder andere Daten wie z.B. API-Anbindung an Homegate (Wohnungssuche-Filter) oder API-Anbindung an Verkehrsbetriebe, Tram-Haltestellen o.ä.<br><br>Punkte und Umfang sind VORHER!! mit der Lehrperson abzusprechen. |
        
		

## Ablaufplan 2024-Q4

(1. Lehrjahr)


### Klasse <mark>`PE23f`</mark> (MoMo)

Für die maximale LB2-Note ("6.0"), die 70% der Modulnote ausmacht, 
<br>sind [Projektaufgabe(n)](moegliche-LB2-AufgabenProjekte) im Umfang von <mark>**38** Punkten</mark> nötig.

|Tag |Datum|Thema, Auftrag, Übung |
|----|-----|--------------------- |
|  1 | Mo 13.05. | Einführung und Anleitung <br>**Beginn** mit dem Bash-Selbststudium und Installation einer Linux-Umgebung |
|  - | - - -     | Pfingstmontag |
|  2 | Mo 27.05. | **Input** darüber, was an der **Bash-Prüfung** dran kommt.<br> Weiterarbeit mit dem Bash-Selbststudium |
|  3 | Mo 03.06. | Input über **Scheduler/Crontab**.<br> Weiterarbeit mit dem Bash-Selbststudium.<br>Wer will, kann um 10:45 h **Bash-Prüfung (a)** machen |
|  4 | Mo 10.06. | Input **Projektaufgabe(n)**.<br>Beginn der Projektaufgabe(n) oder Bash-Studium.<br>Um 10:45 h **Bash-Prüfung (b)** für den Rest |
|  5 | Mo 17.06. | Input über **FTP** und **eMailing**.<br> Weiterarbeit an der Projektaufgabe(n) |
|  6 | Mo 24.06. | Input über **Textformatierung**.<br>Weiterarbeit an der Projektaufgabe(n) und **Projekt-Abgaben** |
|  7 | Mo 01.07. | Weiterarbeit an der Projektaufgabe(n) und <br>ab 09:00 **Projekt-Abgaben** |
|  8 | Mo 08.07. | Weiterarbeit an der Projektaufgabe(n) und <br>ab 09:00 **Projekt-Abgaben** |

<br>

<hr>


### Klasse <mark>`PE23c`</mark> (DoNa)

Für die maximale LB2-Note ("6.0"), die 70% der Modulnote ausmacht, 
<br>sind [Projektaufgabe(n)](moegliche-LB2-AufgabenProjekte) im Umfang von <mark>**46** Punkten</mark> nötig.

|Tag |Datum|Thema, Auftrag, Übung |
|----|-----|--------------------- |
|  1 | Do 16.05. | Einführung und Anleitung <br>**Beginn** mit dem Bash-Selbststudium und Installation einer Linux-Umgebung |
|  2 | Do 23.05. | **Input** darüber, was an der **Bash-Prüfung** dran kommt.<br> Weiterarbeit mit dem Bash-Selbststudium |
|  3 | Do 30.05. | Input über **Scheduler/Crontab**.<br> Weiterarbeit mit dem Bash-Selbststudium.<br>Wer will, kann um 15:30 h **Bash-Prüfung (a)** machen |
|  4 | Do 06.06. | Input **Projektaufgabe(n)**.<br>Beginn der Projektaufgabe(n) oder Bash-Studium.<br>Um 15:30 h **Bash-Prüfung (b)** für den Rest |
|  5 | Do 13.06. | Input über **FTP** und **eMailing**.<br> Weiterarbeit an der Projektaufgabe(n) |
|  6 | Do 20.06. | Input über **Textformatierung**.<br>Weiterarbeit an der Projektaufgabe(n) und **Projekt-Abgaben** |
|  7 | Do 27.06. | Weiterarbeit an der Projektaufgabe(n) und <br>ab 09:00 **Projekt-Abgaben** |
|  8 | Do 04.07. | Weiterarbeit an der Projektaufgabe(n) und <br>ab 09:00 **Projekt-Abgaben** |
|  9 | Do 11.07. | Weiterarbeit an der Projektaufgabe(n) und <br>ab 09:00 **Projekt-Abgaben** |

<br>

<hr>


### Klassen <mark>`AP23d`, `PE23d`</mark> (Freitag)


Für die maximale LB2-Note ("6.0"), die 70% der Modulnote ausmacht, 
<br>sind [Projektaufgabe(n)](moegliche-LB2-AufgabenProjekte) im Umfang von <mark>**46** Punkten</mark> nötig.

|Tag |Datum|Thema, Auftrag, Übung |
|----|-----|--------------------- |
|  1 | Fr 17.05. | Einführung und Anleitung <br>**Beginn** mit dem Bash-Selbststudium und Installation einer Linux-Umgebung |
|  2 | Fr 24.05. | **Input** darüber, was an der **Bash-Prüfung** dran kommt.<br> Weiterarbeit mit dem Bash-Selbststudium |
|  3 | Fr 31.05. | Input über **Scheduler/Crontab**.<br> Weiterarbeit mit dem Bash-Selbststudium.<br>Wer will, kann um 10:45 h (15:30 h) **Bash-Prüfung (a)** machen |
|  4 | Fr 07.06. | Input **Projektaufgabe(n)**.<br>Beginn der Projektaufgabe(n) oder Bash-Studium.<br>Um 10:45 h (15:30 h) **Bash-Prüfung (b)** für den Rest |
|  5 | Fr 14.06. | Input über **FTP** und **eMailing**.<br> Weiterarbeit an der Projektaufgabe(n) |
|  6 | Fr 21.06. | Input über **Textformatierung**.<br>Weiterarbeit an der Projektaufgabe(n) |
|  7 | Fr 28.06. | Weiterarbeit an der Projektaufgabe(n) und <br>ab 09:00 **Projekt-Abgaben** |
|  8 | Fr 05.07. | Weiterarbeit an der Projektaufgabe(n) und <br>ab 09:00 **Projekt-Abgaben** |
|  9 | Fr 12.07. | Weiterarbeit an der Projektaufgabe(n) und <br>ab 09:00 **Projekt-Abgaben** |

<br>

<hr>
&copy; Harald Müller, Apr 2024

