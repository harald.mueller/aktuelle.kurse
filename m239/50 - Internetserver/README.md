# 50 - Internetserver

- Webmin ist ein Web-basierte Schnittstelle für die Administrierung von Unix. Über einen Web-Browser lassen sich Benutzer-Accounts einrichten, Apache, DNS, etc. einrichten.
<br>Verweis: http://www.webmin.com/standard.html


## 01 -- Webserver-Software

### Apache
- Apache HTTP server benchmarking tool
<br>Verweis: https://bscw.tbz.ch/bscw/bscw.cgi/d22141950/ab%20-%20Apache%20HTTP%20server%20benchmarking%20tool

- Reaktionszeit von Webseiten optimieren	 	
<br>Verweis: http://www.linux-community.de/Internal/Artikel/Print-Artikel/LinuxUser/2016/01/Expressversand

- Website für Apache
<br>Verweis: https://httpd.apache.org/

 	
### MS-IIS Internet Information Server (Microsoft)

- The Official Microsoft IIS Site
<br>Verweis: https://www.iis.net/


### NGINX

Webserver-Software, Reverse Proxy und E-Mail-Proxy (POP3/IMAP)

- Website für NGINX
<br>Verweis: https://nginx.org/en/


## 02 -- Fileserver

- Apache Module mod_dav
<br>Verweis: https://httpd.apache.org/docs/2.4/mod/mod_dav.html


## 03 -- Mailserver

- CommuniGate Pro (Windows)
<br>E-Mail-Server
<br>Verweis: https://www.communigate.com/de/

- Dovecot (Linux)
<br>IMAP und POP3-Server für Linux
<br>Verweis: http://https://www.dovecot.org/

- Exim4 (für alle Plattformen)
<br>SMTP-Server
<br>Verweis: http://www.exim.org/

- Exim4 einrichten Mail-Server unter Linux
<br>Verweis: http://www.pcwelt.de/ratgeber/Den_eigenen_Mailserver_einrichten_-_so_geht_s-Linux_als_Server-8716159.html

- HMailServer (Windows)
<br>Open Source Mail-Server für MS Windows (siehe auch unter "Downloaded Tools")
<br>Verweis: https://www.hmailserver.com/


## 04 -- Groupware und CMS

- Die Verbreitung der Content Management Systeme CMS 	 	
<br>Verweis: http://www.webkalkulator.com/cmsvergleich

- Wordpress
<br>Verweis: https://de.wordpress.org/
	
- Typo3
<br>Verweis: https://typo3.org/
	
- Joomla 	 	
<br>Verweis: https://www.joomla.org/announcements/release-news/5664-joomla-3-6-is-here.html

- Drupal, ein Open Source CMS 	 	
<br>wird bspw. verwendet von BlackSocks.ch
<br>Verweis: https://www.drupal.org/
 	 	 	 	 	 	
- BSCW
<br>Verweis: http://www.bscw.de/

## 05 -- Proxy-Server

- Aufstellung verschiedener Proxy-Server
<br>Verweis: http://dmoztools.net/Computers/Internet/Proxying_and_Filtering/Products_and_Tools/Software/

- TCP Tunnel
<br>Sendet alle Nachrichten, welche auf einem lokalen Port eintreffen, an einen Remote Host weiter
<br>Verweis: http://www.vakuumverpackt.de/tcptunnel/

