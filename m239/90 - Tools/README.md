# 90 - Tools

Zusammengestellt 2020 von Th. Kälin


## Vorinstallierte Tools

- Bereits standardmässig vorhandene Tools	 	
<br>Beschreibung div. CMD-Befehle: ARP, GETMAC, IPCONFIG, NBTSTAT, NET, NETSH, NETSTAT, NSLOOKUP, PATHPING, PING, TRACERT, WHOAMI,
<br>Verweis: https://kompendium.infotip.de/netzwerkbefehle-der-windows-kommandozeile.html

	
- ARP	 	
<br>Address Resolution Protocol - Zuordnung IP-Adresse zu MAC-Adresse
<br>Verweis: https://www.tummy.com/articles/networking-basics-how-arp-works/

	
- IpConfig	 	
<br>Zeigt Hardware-Adressen und IP-Adressen von verwendeten Geräte
<br>Verweis: https://de.wikipedia.org/wiki/Ipconfig

- NetStat	 	
<br>Anfrage nach geöffneten Ports und dafür verantwortlichen Programmen (netstat -a -b)
<br>Verweis: https://de.wikipedia.org/wiki/Netstat

- NsLookup
<br>Für die Anfrage nach MailServer (Type=MX), oder Webserver (Type=A), etc.
<br>Verweis: https://de.wikipedia.org/wiki/Nslookup

- Ping	 	
<br>Ist Host erreichbar?
<br>Verweis: https://de.wikipedia.org/wiki/Ping_(Daten%C3%BCbertragung)

- Route	 	
<br>Zeigt Einträge der lokalen IP Routing Tabelle an und lässt diese ändern.
Verweis: https://technet.microsoft.com/en-us/library/bb490991.aspx

- TraceRt	 	
<br>Ermittelt Router und Internet-Knoten, über welches die IP-Datenpakete zum Zielrechner gelangen
Verweis: https://de.wikipedia.org/wiki/Traceroute

## Online-Tools

- Base64ImageEncoder
<br>Bilder direkt in Webseite/Mail einbinden
<br>Verweis: https://www.base64encode.net/base64-image-encoder

- MX-Toolbox
<br>Diverse Mail-Tools
<br>Verweis: https://mxtoolbox.com/

## Tools zum Download

- [./downloaded-tools](./downloaded-tools)

## Weitere

- Fiddler - Tool zur Analyse von HTTP-Nachrichten
<br>Verweis: http://www.telerik.com/fiddler
	
- ab - Apache HTTP server benchmarking tool
<br>"ab" ist ein Tool für Benchmark-Tests eines HTTP-Apache-Servers
<br>Verweis: https://httpd.apache.org/docs/2.4/programs/ab.html
	
- CertMgr.msc - Zertifikate verwalten
<br>Zertifikate unter Windows verwalten
<br>Verweis: https://docs.microsoft.com/en-us/windows-hardware/drivers/devtest/certmgr
	
- CURL - command line tool and library for transferring data with URLs
<br>Zum Dateien transferieren über FTP, HTTP, IMAP, LDAP, POP3, SMTP, Telnet etc.
<br>Verweis: https://curl.haxx.se/
	
- Nagios - Zur Überwachung von Webservern
<br>Verweis: https://www.nagios.org/
	
- OpenSSL - Toolkit für TLS-Unterstützung
<br>Interessanter Link http://commandlinefanatic.com/cgi-bin/showarticle.cgi?article=art030
<br>Verweis: https://www.openssl.org/
	
- Webalizer
<br>Zur Analyse von Logfiles
<br>Verweis: http://www.webalizer.org/
	
- Wireshark 
<br>Netzwerkprotokoll-Analyzer
<br>Verweis: https://www.wireshark.org/


--

- Netcat
<br>Arbeitet als Server oder Client mit den Protokollen TCP und UDP (TCP/IP swiss army knife)
<br>Verweis: https://de.wikipedia.org/wiki/Netcat
 	 	 	 

- SysInternals - Div. Tools (Netzwerk, Dateien, Prozesse, etc.)
<br>Ausgesprochen nützliche Tools!!! (v.a. ProcExp und ProcMon)
<br>Verweis: https://technet.microsoft.com/en-us/sysinternals/bb795532