# 60 - Zertifikate

## Was sind Zertifikate

 https://www.youtube.com/watch?v=LRMBZhdFjDI

### SSL, TLS und Zertifikate

- 10 - SSL, TLS und Zertifikate	 	
<br>Guter Überblick über TLS
<br>Verweis: http://www.elektronik-kompendium.de/sites/net/0902281.htm
 		
- 11 - Understanding SSL Certificates
<br>Gute Beschreibung (en)
<br>Verweis: http://pierrelx.com/understanding-ssl-certificate-and-ssl-encryption/


- Digitale Signatur erstellen mit OpenSSL
<br>Verweis: http://www.soft-ware.net/tipps/tipp67/Digitale-Signatur-erstellen-pfx-Datei.asp
 	 	 	 	 	 	 

## Zertifikat gratis erstellen

- Zertifikat gratis erstellen (Lets Encrypt)
<br>Eine andere Möglichkeite wäre "Cloudfare"
<br>Verweis: https://letsencrypt.org/


## Zertifikate installieren

- Beschreibt die Installation von Zertifikaten für unterschiedliche Webserver
<br>Verweis: https://de.wikihow.com/Ein-SSL-Zertifikat-installieren

## Zwertifikatsfehler forcieren

- Fehlermeldung forcieren, welche bei abgelaufenem Zertifikat (Certificate expired) angezeigt wird
<br>Verweis: https://badssl.com/


## Zertifikatsprüfung manuell

- TLS-Tests
<br>Feststellen, ob SSL korrekt angewandt (vom Browser/Server) wird und Dokumentation darüber
<br>(Alternative: https://www.ssllabs.com/)
<br>Verweis: https://tls-ssl-test.internet-sicherheit.de/

- Zertifikatsprüfung manuell (ist Absender vertrauenswürdig)
<br>Beschreibt Vorgehen mit verschiedenen Browsern (Überprüfung Fingerprint)
<br>Verweis: https://www.ebas.ch/de/ihr-sicherheitsbeitrag/zertifikatspruefung
 	
	
## 'man in the middle'-Angriffe auf HTTPS

- Man in the middle-Angriffe auf HTTPS
<br>Sehr guter Artikel über MitM-Angriffe. Übrigens macht Fiddler genau das (aber natürlich nicht als Angriff)
<br>Verweis: https://www.ceilers-news.de/serendipity/207-Man-in-the-Middle-Angriffe-auf-HTTPS.html
 	 	 	 	 	 
