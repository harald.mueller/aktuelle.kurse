[TOC]

# M162 Daten analysieren und modellieren

## Einf&uuml;hung in Daten und Diagramme

### Daten, Wissen, Information?
  
#### Was ist Information?

<br>

![divorce.jpeg](00_divorce.jpeg)

<br>
<br>
<br>
<br>

![yes-no-poll.jpeg](00_yes-no-poll.jpeg)

<br>
<br>
<br>
<br>

#### Wissenstreppe


[![Wissenstreppe-klein.jpg](Wissenstreppe-klein.jpg)<br>Wissenstreppe.jpg](Wissenstreppe.jpg)
  
- [Wissenstreppe.docx](Wissenstreppe.docx)

<br>
<br>
<br>
<br>

### Diagramme

- zur Datendarstellung?
- zur Informationsdarstellung?
- zur Wissensdarstellung?

<br>
<br>
<br>
<br>


[![HansRoslings200YearsIn4min.jpg](HansRoslings200YearsIn4min.jpg)<br>(4:47 min) Hans Rosling's 200 Countries, 200 Years, 4 Minutes - The Joy of Stats - BBC Four](https://www.youtube.com/watch?v=jbkSRLYSojo)

- Ist das gut?
- Wo sind die Probleme?

<br>
<br>
<br>
<br>

#### Datagramm-Typen

[Einfache Diagramm-Typen](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Auswertungen_Excel/Diagrammtypen.md)

<br>
<br>

#### Diagramme erstellen

- Merkblatt [Diagramme-Erstellen](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Auswertungen_Excel/Diagramme-Erstellen.md)

<br>
<br>

#### Einsatz von Diagrammen

- [01_Einsatz-von-Diagrammtypen.pptx](01_Einsatz-von-Diagrammtypen.pptx)

<br>
<br>

#### So lügt man mit Charts

- [02_So-luegt-man-mit-Charts.pptx](02_So-luegt-man-mit-Charts.pptx)

<br>
<br>

[
![Absicht oder Lüge?](absicht-oder-luege.png)
<br>Fehler oder Absicht bzw. Wunschdenken der <br>Journalisten von ARD zum Wahlsieg der AfD<br> in Thüringen und Sachsen am 1.9.2024?<br> YouTube-Short/TicToc 09.2024](https://youtube.com/shorts/xexmPdapHY4?si=1bYGq5gDbybuIpCe)