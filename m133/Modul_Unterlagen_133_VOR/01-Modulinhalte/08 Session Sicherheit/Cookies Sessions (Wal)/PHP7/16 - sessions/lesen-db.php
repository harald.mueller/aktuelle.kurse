<?php
  require_once "sessions.inc.php";
?>
<html>
<head>
  <title>Sessions</title>
</head>
<body>
<p>Programmiersprache: 
<?php
  if (isset($_SESSION["Programmiersprache"])) {
    echo(htmlspecialchars($_SESSION["Programmiersprache"]));
  }
?>
</p>
<p>Sprachversion: 
<?php
  if (isset($_SESSION["Sprachversion"])) {
    echo(htmlspecialchars($_SESSION["Sprachversion"]));
  }
?>
</p>
<p><a href="<?php echo($_SERVER["PHP_SELF"]); ?>">Neu laden</a></p>
</body>
</html>
