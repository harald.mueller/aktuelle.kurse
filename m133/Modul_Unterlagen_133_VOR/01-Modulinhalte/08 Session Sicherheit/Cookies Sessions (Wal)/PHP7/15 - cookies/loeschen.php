<?php
  if (isset($_GET["kill"])) {
    setcookie($_GET["kill"], "", 0, "/");
  }
?>
<html>
<head>
  <title>Cookies</title>
<?php
  if (isset($_GET["kill"])) {
    echo("<meta http-equiv=\"refresh\" " . 
      "content=\"0;url=" . 
      htmlspecialchars($_SERVER["PHP_SELF"]) . 
      "\">");
  }
?>
</head>
<body>
<table>
  <tr><th>Name</th><th>Wert</th>
      <th>L&ouml;schen?</th></tr>
<?php
  foreach (array_keys($_COOKIE) as $name) {
    echo("<tr><td>" . htmlspecialchars($name) . 
         "</td>");
    echo("<td>" . 
         htmlspecialchars($_COOKIE[$name]) . 
         "</td>");
    echo("<td><a href=\"" . 
         htmlspecialchars($_SERVER["PHP_SELF"]) .
         "?kill=" . urlencode($name) . 
         "\">Ja</a></td></tr>");
  }
?>
</table>
</body>
</html>
