<html>
<head>
  <title>Cookies</title>
</head>
<body>
<table>
  <tr><th>Name</th><th>Wert</th></tr>
  <tr><td>Programmiersprache</td><td>
<?php
  if (isset($_COOKIE["Programmiersprache"])) {
    echo htmlspecialchars(
      $_COOKIE["Programmiersprache"]);
  }
?>
  </td></tr>
  <tr><td>Sprachversion</td><td>
<?php
  if (isset($_COOKIE["Sprachversion"])) {
    echo htmlspecialchars(
      $_COOKIE["Sprachversion"]);
  }
?>
  </td></tr>
  <tr><td>Session</td><td>
<?php
  if (isset($_COOKIE["Session"])) {
    echo htmlspecialchars(
      $_COOKIE["Session"]);
}
?>
  </td></tr>
</table>
</body>
</html>
