<?php
/*************************************************************/
/**  Modul: 	Global (M-133)                              **/
/**  Filename:	control_global.php                          **/
/**  Author:	VOM                                         **/
/**  Version:	1.0                                         **/
/**                                                         **/
/*************************************************************/
session_start();
// Button Save Global data was selected
if (isset($_REQUEST['saveglobal']))
{
  $Inhalt_Text = $_REQUEST['globaldata'];
  $GLOBALS['GlobalText']= $Inhalt_Text;
  $_SESSION['GlobalText']= $Inhalt_Text;
  echo ("<meta http-equiv='refresh' content='0; url=http://localhost/M_133/Global_Variable_Aufg2/GlobalVariable.html' />");  
  exit;
}
// Button Show Global data was selected
if (isset($_REQUEST['showglobal']))
{ 
    $html_out = ("<b>Show Session Global Data</b><br>");
	$html_out .=$_SESSION['GlobalText'];
    echo $html_out;
}	

?>