<?php
/**
 * Created by PhpStorm.
 * User: mvorelli
 * Date: 15.11.2017
 * Time: 12:13
 */

// http://localhost:8080/pdo/pdo_prepared.php
// Documentation
// http://php.net/manual/en/pdostatement.fetch.php 
 
echo "PDO ODBC Prepared Demo File<br>";

  $user="root";
  $pw = "";
  $dsn ="odbc:PDO_Connection";
 
try 
{
	$dbconn  = new PDO($dsn,$user,$pw);
	$stmt = $dbconn->prepare('SELECT id,Ortname from tbl_orte');
    $stmt->execute();	

/* Exercise PDOStatement::fetch styles */
print("PDO::FETCH_ASSOC: ");
print("Return next row as an array indexed by column name\n");
$result = $stmt->fetch(PDO::FETCH_ASSOC);
print_r($result);
print("<br>");

print("PDO::FETCH_BOTH: ");
print("Return next row as an array indexed by both column name and number\n");
$result = $stmt->fetch(PDO::FETCH_BOTH);
print_r($result);
print("<br>");

print("PDO::FETCH_LAZY: ");
print("Return next row as an anonymous object with column names as properties\n");
$result = $stmt->fetch(PDO::FETCH_LAZY);
print_r($result);
print("<br>");

print("PDO::FETCH_OBJ: ");
print("Return next row as an anonymous object with column names as properties\n");
$result = $stmt->fetch(PDO::FETCH_OBJ);
print $result->Ortname;
print("<br>");


die();
	
    foreach ($dbconn->query('SELECT * from tbl_orte') as $row) {
        print_r($row);
   }
	// close DB connection
    $dbconn  = null;
} 
catch (PDOException $e) {
    print "ODBC Error!: " . $e->getMessage() . "<br/>";
    die();
}

?>



