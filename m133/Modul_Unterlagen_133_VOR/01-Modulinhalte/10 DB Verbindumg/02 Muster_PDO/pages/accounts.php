<?php

// Read Aktiven/Passiven
$query = '
SELECT
	id, kontostand, bezeichnung, kontotyp, kontonr
FROM
	konto
WHERE
	kontotyp=1 OR kontotyp=2
ORDER BY
	kontonr ASC';
$result = $dbconnection->query($query);

?>

<h1>Kontenplan</h1>

<h3>Aktiven/Passiven</h3>

<table cellpadding="0" cellspacing="0">
	<tr><th>Nr.</th><th>Bezeichung</th><th align="right">Saldo</th><th align="right">Saldo</th></tr>
<?php while($row = $result->fetch()) { ?>
	<tr><td><a href="index.php?page=account_details&amp;konto=<?= $row['id'] ?>"><?= $row['kontonr'] ?></a></td>
	<td><?= $row['bezeichnung'] ?></td>
	<?php if($row['kontotyp'] == 1) { ?>
		<td align="right"><?= number_format($row['kontostand'], 2) ?></td><td>&nbsp;</td></tr>
	<?php } else { ?>
		<td align="right">&nbsp;</td><td><?= number_format($row['kontostand'], 2) ?></td></tr>
	<?php } ?>
<?php } ?>
</table>

<?php

// Read Aufwand/Ertrag
$query = '
SELECT
	id, kontostand, bezeichnung, kontotyp, kontonr
FROM
	konto
WHERE
	kontotyp=3 OR kontotyp=4
ORDER BY
	kontonr ASC';
$result = $dbconnection->query($query);

?>

<h3>Aufwand/Ertrag</h3>

<table cellpadding="0" cellspacing="0">
	<tr><th>Nr.</th><th>Bezeichung</th><th align="right">Saldo</th><th align="right">Saldo</th></tr>
<?php while($row = $result->fetch()) { ?>
	<tr><td><a href="index.php?page=account_details&amp;konto=<?= $row['id'] ?>"><?= $row['kontonr'] ?></a></td>
	<td><?= $row['bezeichnung'] ?></td>
	<?php if($row['kontotyp'] == 4) { ?>
		<td align="right"><?= number_format($row['kontostand'], 2) ?></td><td>&nbsp;</td></tr>
	<?php } else { ?>
		<td align="right">&nbsp;</td><td><?= number_format($row['kontostand'], 2) ?></td></tr>
	<?php } ?>
<?php } ?>
</table>