#########################################
#
# SQL-Dump f�r Modulpr�fung 151	
#	
# 03.10.2006
#
# Sandro Ropelato
#
#########################################





# Testeintr�ge f�r Tabelle Buchungen:
#####################################

INSERT INTO buchungen VALUES (8, NULL, 200.00, '2006-10-03 14:24:17', 0, 0, 1500, 4000);
INSERT INTO buchungen VALUES (7, NULL, 5000.00, '2006-10-03 14:24:08', 0, 0, 3000, 2000);
INSERT INTO buchungen VALUES (6, NULL, 20000.00, '2006-10-03 14:22:40', 0, 0, 1000, 2500);



# Testeintr�ge f�r Tabelle Konten:
##################################

INSERT INTO konten VALUES (1000, ENCODE('Kasse', 'lol'), NULL, 0);
INSERT INTO konten VALUES (1500, ENCODE('Debitoren', 'lol'), NULL, 0);
INSERT INTO konten VALUES (2000, ENCODE('Kreditoren', 'lol'), NULL, 2);
INSERT INTO konten VALUES (2500, ENCODE('Eigenkapital', 'lol'), NULL, 2);
INSERT INTO konten VALUES (3000, ENCODE('Warenaufwand', 'lol'), NULL, 4);
INSERT INTO konten VALUES (3000, ENCODE('Warenertrag', 'lol'), NULL, 5);





