CREATE TABLE Konti (
  Konten_ID INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  Konten_Nummer INTEGER UNSIGNED NULL,
  Konten_Bezeichnung VARCHAR(50) NULL,
  Konten_Beschreibung TEXT NULL,
  Konten_Typ INTEGER UNSIGNED NULL,
  Konten_Saldo FLOAT NULL,
  PRIMARY KEY(Konten_ID)
)
TYPE=InnoDB;

CREATE TABLE Buchungen (
  Buchungen_ID INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  Buchungen_Beschreibung TEXT NULL,
  Buchungen_Betrag DECIMAL(10,2) NULL,
  Buchungen_Datum DATETIME NULL,
  Buchungen_Sollkonto_FK INTEGER UNSIGNED NOT NULL,
  Buchungen_Habenkonto_FK INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(Buchungen_ID),
  FOREIGN KEY(Buchungen_Sollkonto_FK)
    REFERENCES Konti(Konten_ID)
      ON DELETE NO ACTION
      ON UPDATE RESTRICT,
  FOREIGN KEY(Buchungen_Habenkonto_FK)
    REFERENCES Konti(Konten_ID)
      ON DELETE NO ACTION
      ON UPDATE RESTRICT
)
TYPE=InnoDB;


