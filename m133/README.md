# M133 Web-Applikation mit Session-Handling realisieren

[> **Modulidentifikation** ](https://www.modulbaukasten.ch/module/133/3/de-DE?title=Web-Applikation-mit-Session-Handling-realisieren)

- 1. Vorgabe analysieren, Funktionalität entwerfen und Realisierungskonzept festlegen.
- 2. Spezifische Funktionalität einer Web-Applikation mit Session-Handling, Authentifizierung und Formularüberprüfungen realisieren.
- 3. Web-Applikation mit einer Programmiersprache unter Berücksichtigung sicherheitsrelevanter Anforderungen programmieren.
- 4. Web-Applikation gemäss Testplan auf Funktionalität und Sicherheit überprüfen, Testergebnisse festhalten und allenfalls erforderliche Korrekturen vornehmen.

<hr>

## Leistungsbeurteilung

- LB1 (35%) Theorie- und Praxisprüfung (Ecolm 30 min + 60 min Programmieren)
<br>(am Tag 5) 

- LB2 (65%) Projekt ([Abgabe am letzten Modultag]())
<br>[M133_LB2_Projekt_Vorgabe_Bewertung](./1_Beurteilungskriterien-und-Projektauftrag/M133_LB2_Projekt_Vorgabe_Bewertung_V21.pdf)


## Ablaufplan 2024-Q4

### Klasse <mark>BI22a</mark> am Dienstag nachmittags


|Tag |Datum|Thema, Auftrag, Übung |
|----|-----|--------------------- |
|  1 | Di 18.02. | Einführung & Installationen, Input PHP & HTML <br>Beginn Projekt |
|  2 | Di 25.02. | Input Formulare und Werteübermittlung <br> Weiterarbeit am Projekt |
|  3 | Di 04.03. | Input nach Bedarf <br> Weiterarbeit am Projekt  |
|  4 | Di 11.03. | Weiterarbeit am Projekt  |
|  5 | Di 18.03. | **LB1** Theorie- und Praxisprüfung 15:10-16:30 |
|  6 | Di 25.03. | Weiterarbeit am Projekt  |
|  7 | Di 01.04. | Weiterarbeit am Projekt  |
|  8 | Di 08.04. | Weiterarbeit am Projekt  |
|  9 | Di 15.04. | Weiterarbeit am Projekt  |
| 10 | Di 06.05. | [Projektabgaben Terminplan](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/EerWr-7uzrVIpU2g_I6ek_IBdbAbi4XK5s4yCTLSubDJ6g?e=fcZFvf) |



## Aufträge

- Tag 1: 
[Auftrag_Tag1_M133_ApplikationsentwicklerInnen.pdf](./1_Beurteilungskriterien-und-Projektauftrag/Auftrag_Tag1_M133_ApplikationsentwicklerInnen.pdf)


## Clone

```
git clone https://gitlab.com/harald.mueller/m133demo.git

```