<?php
  if (isset($_GET["test"])) {
    $temp = array_key_exists("CookieTemp", 
                             $_COOKIE);
    $perm = array_key_exists("CookiePerm", 
                             $_COOKIE);
    setcookie("CookieTemp", "", 0);
    setcookie("CookiePerm", "", 0);
  } else {
    setcookie("CookieTemp", "ok");
    setcookie("CookiePerm", "ok", time() + 60*60*24);
  }
?>
<html>
<head>
  <title>Cookies</title>
<?php
  if (!isset($_GET["test"])) {
    echo "<meta http-equiv=\"refresh\" " . 
      "content=\"0;url=" . 
      htmlspecialchars($_SERVER["PHP_SELF"]) . 
      "?test=ok\">";
  }
?>
</head>
<body>
<?php
  if (isset($_GET["test"])) {
    echo "Tempor&auml;re Cookies werden " .
      ($temp ? " " : "nicht ") .
      "unterst&uuml;tzt.<br />";
    echo "Permanente Cookies werden " .
      ($perm ? " " : "nicht ") .
      "unterst&uuml;tzt.";
  } else {
    echo "Cookies werden gesetzt ... ";
  }
?>
</body>
</html>
