<?php
  session_start();

  if (!isset($_SESSION["login"]) || $_SESSION["login"] != "ok") {
    $url = $_SERVER["SCRIPT_NAME"];
    if (isset($_SERVER["QUERY_STRING"])) {
      $url .= "?" . $_SERVER["QUERY_STRING"];
    }
    header("Location: login.php?url=" . urlencode($url));
  }
?>
