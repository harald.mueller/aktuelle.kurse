<?php
/**
 * Created by PhpStorm.
 * User: mvorelli
 * Date: 15.11.2017
 * Time: 12:13
 */

// http://localhost:8080/pdo/pdo.php
 
 
echo "PDO ODBC Demo File";

  $user="root";
  $pw = "";
  $dsn ="odbc:PDO_Connection";

//  $dsn ="odbc:lokal_MysqlServer"; PHP 5.x only supported
// if (!($myConn = odbc_connect('PDO_Connection','root',''))) {
//        echo "No ODBC connection<br />";
//    }
//	else echo "DB Connection ok<br>";  
  
try 
{
	$dbconn  = new PDO($dsn,$user,$pw);	
    foreach ($dbconn->query('SELECT * from tbl_orte') as $row) {
        print_r($row);
   }
	// close DB connection
    $dbconn  = null;
} 
catch (PDOException $e) {
    print "ODBC Error!: " . $e->getMessage() . "<br/>";
    die();
}



/**
$pdo = new PDO('pgsql:host=192.168.137.1;port=5432;dbname=anydb', 'anyuser', 'pw');
sleep(5);
$stmt = $pdo->prepare('SELECT * FROM sometable');
$stmt->execute();
$pdo = null;
sleep(60);

**/

?>



