<?php

// Include configuration and connect to the database
require_once('include/config.inc.php');
require_once('include/db.inc.php');

// Start session (might be required later);
session_start();

// Avoid errors
if(!isset($_GET['page']))
{
	$_GET['page'] = '';
}

// Now find the requested page
$page_title = '';
$page_file = '';
switch($_GET['page'])
{
	case 'accounts':
	{
		// Accounts
		$page_title = 'Kontenplan';
		$page_file = 'accounts.php';
		break;
	}
	case 'account_details':
	{
		// Accounts
		$page_title = 'Kontenplan';
		$page_file = 'account_details.php';
		break;
	}
	case 'book':
	{
		// Accounts
		$page_title = 'Hauptbuch';
		$page_file = 'book.php';
		break;
	}
	case 'booking':
	{
		// Accounts
		$page_title = 'Buchung';
		$page_file = 'booking.php';
		break;
	}
	default:
	{
		// Fallback to home
		$page_title = 'Home';
		$page_file = 'home.php';
		break;
	}
}

// Include page parts
require_once('layout/header.php');
require_once('layout/navigation.php');
require_once('pages/'.$page_file);
require_once('layout/footer.php');

// Close database connection
//$dbconnection->close();

?>
