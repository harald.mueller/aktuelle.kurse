-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 19. Dezember 2012 um 09:07
-- Server Version: 5.5.8
-- PHP-Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `loginsystem`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzerdaten`
--

CREATE TABLE IF NOT EXISTS `benutzerdaten` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nickname` varchar(50) NOT NULL DEFAULT '',
  `Kennwort` varchar(50) NOT NULL DEFAULT '',
  `Nachname` varchar(50) NOT NULL DEFAULT '',
  `Vorname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `benutzerdaten`
--

INSERT INTO `benutzerdaten` (`Id`, `Nickname`, `Kennwort`, `Nachname`, `Vorname`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Mustermann', 'Max'),
(2, 'test', '900150983cd24fb0d6963f7d28e17f72', 'Kunze', 'Martin');
