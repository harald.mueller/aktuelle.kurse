<?PHP

function setBit(&$bitField,$n) {
	
	// Ueberprueft, ob der Wert zwischen 0-33 liegt
	// $n ist hier der Wert der aktivierten Checkbox, z.B. 15
	// Somit waere hier die 15. Checkbox aktiviert
	if(($n < 0) or ($n > 32)) return false; 
	
	
	// Bit Shifting
	// Hier wird nun der Binaerwert fuer die aktuelle Checkbox gesetzt.
	// In unserem Beispiel wird hier nun die 15. Stelle von rechts auf 1 gesetzt
	// 100000000000000 <-- Dieses entspricht der Zahl 16384
	// | ist nicht das logische ODER sondern das BIT-oder
	$bitField |= (0x01 << ($n-1));
	return true;
	
}

function clearBit(&$bitField,$n) {
	
	// Loescht ein Bit oder ein Bitfeld
	// & ist nicht das logische UND sondern das BIT-and
	$bitField &= ~(0x01 << ($n-1));
	return true;
	
}

function isBit($bitField,$n) {
	
	return (($bitField & (0x01 << ($n-1))));
	
}

/*
 * Die Menge der Checkboxen sollten hier angegeben werden
 */
if( $_POST["checkboxen"] < 1 || $_POST["checkboxen"] > 32)
	$checkboxen = 32;
else
	$checkboxen = $_POST["checkboxen"];


/*
 * Hier sollte der Wert aus einer Datenbank geholt werden
 * Dies sollte aber nur beim ersten Aufruf geschehen
 */
 
if( !isset($_POST["checkbox_name"]) && !is_array($_POST["checkbox_name"]) && !isset($_POST["CheckboxenBinaer"]) ){
	// Select der Datenbankabfrage hier einfuegen und den Wert
	// in $wertAbfrage speichern 
	$wertAbfrage = 0;
	
	// Ueberpruefung ob ein Wert vorhanden ist 
	if( !empty($wertAbfrage) )
		$bitmask = $wertAbfrage;
	else
		$bitmask = 0x0;
		
	// Durchlaeuft schrittweise alle Elemente von $bitmask.
	foreach(range(1,$checkboxen) as $position){
		
		// Ueberprueft, ob die x-te Stelle eine 1 ist und speichert bei Erfolg
		// ein "checked" in das Arrayelement fuer die x-te Position.
		// Beispiel: $position ist 6. Nun wird die 6.Stelle von rechts innerhalb
		// der Variablen $bitmask auf eine 1 hin ueberprueft. Steht an dieser Stelle
		// eine 1, so wird in $formCheck[6] ein "checked" gespeichert.
		// Somit wird die sechste Checkbox aktiviert.
		if(isBit($bitmask,$position)) {
			$formCheck[$position] = " checked";
		}
		
	}
	
}
else{
	
	// Das Formular wurde versendet und die Checkboxen muessen
	// nun konvertiert werden.
	
	// Wurde ueberhaupt eine Checkbox aktiviert?
	If( isset($_POST["checkbox_name"]) ){	
		
		foreach($_POST["checkbox_name"] as $position){ 
		
			// Setzen der einzelnen Checkboxen in der Binaerstruktur
			setBit($bitmask,$position);
			
			// Ist die Checkbox aktiviert?
			if(isBit($bitmask,$position)) {
				$formCheck[$position] = " checked";
			}
		
		}
					
	}
	
	// Hier kann nun auch der Wert wieder in die Datenbank geschrieben werden
		$neuerWert = $bitmask;
		
}

?>

<form name="form" method="post" action="<?PHP echo $PHP_SELF; ?>">
<?PHP
	echo 'Der Dezimalwert f�r diese Anordnung ist: <b>' . $bitmask . '</b><br>';
	echo 'Der Bin�rwert f�r diese Anordnung ist: <b>' . decbin($bitmask) . '</b><br>';
	for($x=1;$x<=$checkboxen;$x++){
?>

<input type="checkbox" name="checkbox_name[]" value="<?PHP echo $x; ?>" <?PHP echo $formCheck[$x]; ?>>

<?PHP
	}
?>
  <br>Menge der Checkboxen <input type="text" name="checkboxen" value="<?PHP echo $_POST["checkboxen"]; ?>">
  <br><input type="submit" name="CheckboxenBinaer" value="Send"><br>
</form>