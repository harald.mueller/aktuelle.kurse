<?php
/***********************************************/
/** M133 Theorie Session			**/
/** Author: 	M. von Orelli			**/
/** Datum:	11. 1.2006			**/
/** Version:	1.0				**/
/** Applikation:Demo Session				**/
/***********************************************/


/*********************************************************/
/** Layer informationen					**/
/** ===================					**/
/**							**/
/** PL:	IE						**/
/** PL: Session Theorie					**/
/** DL: Kein						**/
/**							**/
/** Input Parameter:					**/
/**							**/
/**							**/
/*********************************************************/

/*********************************************************/
/* Datum   	� Aenderung            			**/
/* 11. 1.06	�                              		**/
/*         	�                       		**/
/*         	�                       		**/
/*         	�                 			**/
/*         	�                 			**/
/*********************************************************/
/** Deklaration **/


  Define ("CRLF", "\n<br>");

/** Eigene Session wird definiert **/
  session_name("M133_Theorie");

/** Session wird gestartet **/
  session_start();


/** Abfrage des Session-Names **/
echo ("SessionName: " .session_Name() .CRLF);

/** Abfrage der Session-ID **/
echo ("SessionID: " .session_id() .CRLF);

/** Session Array wird ein Wert zugewiesen
    und ausgegeben. **/
$_SESSION["API_PHPDIR"] = "C:\PHP\sessiondata";
echo $_SESSION["API_PHPDIR"] .CRLF;

/** Session auslesen **/
$_SESSION["AUTHORISATION"] = "Orelli";
echo $_SESSION["AUTHORISATION"] .CRLF;

/** Session komplett auslesen **/
foreach($_SESSION as $value)
{
  echo $value. "<br>";
}

echo '<pre>';
  print_r($_SESSION);
echo '</pre>';

// Bestimmte Session l�schen
// session_unset($_SESSION["AUTHORISATION"]);
// Session komlett l�schen
session_unset($_SESSION);
echo "Session auslesen: ". $_SESSION["AUTHORISATION"] .CRLF;

// print_r($_SERVER);
 echo "<br><br>";
 foreach ($_SERVER as $value)
 {
 	echo key($_SERVER) . ":  ". $value ."<br>";
 	next($_SERVER);
 }




?>

