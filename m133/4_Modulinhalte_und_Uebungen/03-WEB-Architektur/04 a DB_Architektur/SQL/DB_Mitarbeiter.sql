-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 23. Nov 2015 um 20:31
-- Server Version: 5.6.16
-- PHP-Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `test`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_personen`
--

CREATE TABLE IF NOT EXISTS `tbl_personen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nachname` varchar(50) NOT NULL,
  `fk_hobby` int(11) NOT NULL,
  `fk_firma` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hobby` (`fk_hobby`),
  KEY `fk_firma` (`fk_firma`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Daten für Tabelle `tbl_personen`
--

INSERT INTO `tbl_personen` (`id`, `Nachname`, `fk_hobby`, `fk_firma`) VALUES
(1, 'Meier', 1, 7),
(6, 'Ulmer', 2, 8),
(7, 'Müller', 2, 7);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `tbl_personen`
--
ALTER TABLE `tbl_personen`
  ADD CONSTRAINT `tbl_personen_ibfk_1` FOREIGN KEY (`fk_hobby`) REFERENCES `tbl_hobbies` (`id`),
  ADD CONSTRAINT `fk_tbl_personen_tbl_firmen1` FOREIGN KEY (`fk_firma`) REFERENCES `tbl_firmen` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
