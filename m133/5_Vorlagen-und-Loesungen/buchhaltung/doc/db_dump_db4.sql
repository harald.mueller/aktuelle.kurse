CREATE TABLE Buchungen (
  Buchungen_ID INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  Buchungen_Habenkonto_FK INTEGER UNSIGNED NOT NULL,
  Buchungen_Sollkonto_FK INTEGER UNSIGNED NOT NULL,
  Buchungen_Beschreibung TEXT NULL,
  Buchungen_Betrag DECIMAL(10,2) NULL,
  Buchungen_Datum DATETIME NULL,
  PRIMARY KEY(Buchungen_ID),
  INDEX Buchungen_Habenkonto_FK(Buchungen_Sollkonto_FK),
  INDEX Buchungen_Sollkonto_FK(Buchungen_Habenkonto_FK)
)
TYPE=InnoDB;

CREATE TABLE Konti (
  Konten_ID INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  KOnten_Bezeichnung VARCHAR(50) NULL,
  Kontenn_Beschreibung TEXT NULL,
  KOnten_Typ INTEGER UNSIGNED NULL,
  PRIMARY KEY(Konten_ID)
)
TYPE=InnoDB;


