#########################################
#
# SQL-Dump f�r Modulpr�fung 151	
#	
# 03.10.2006
#
# Sandro Ropelato
#
#########################################



# Datenbank mit Testdaten erstellen:
####################################

--
-- Tabellenstruktur:
--

CREATE TABLE buchungen
(
  Buchungen_ID int(11) NOT NULL auto_increment,
  Buchungen_Beschreibung text collate latin1_general_ci,
  Buchungen_Betrag decimal(10,2) default NULL,
  Buchungen_Datum datetime default NULL,
  Buchungen_Sollkonto_Typ int(11) NOT NULL,
  Buchungen_Habenkonto_Typ int(11) NOT NULL,
  Buchungen_Sollkonto int(11) default NULL,
  Buchungen_Habenkonto int(11) default NULL,
  PRIMARY KEY  (Buchungen_ID),
  KEY IDX_Buchungen1 (Buchungen_Sollkonto),
  KEY IDX_Buchungen2 (Buchungen_Habenkonto)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;


CREATE TABLE konten
(
  Konten_ID int(11) NOT NULL default '0',
  Konten_Bezeichnung varchar(50) collate latin1_general_ci default NULL,
  Konten_Beschreibung text collate latin1_general_ci,
  Konten_Typ int(11) default NULL,
  PRIMARY KEY  (Konten_ID),
  UNIQUE KEY IDX_Konten1 (Konten_ID)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


--
-- Beispieldaten:
--

INSERT INTO buchungen VALUES (1, NULL, 20000.00, '2006-10-02 14:01:24', 0, 0, 10000, 3000);
INSERT INTO buchungen VALUES (2, NULL, 3600.00, '2006-10-02 14:02:11', 0, 0, 10000, 4000);
INSERT INTO buchungen VALUES (3, NULL, 800.00, '2006-10-02 14:03:00', 0, 0, 1000, 11000);
INSERT INTO buchungen VALUES (4, NULL, 8300.00, '2006-10-02 14:03:11', 0, 0, 2000, 11000);

INSERT INTO konten VALUES (1000, 'Kasse', NULL, 0);
INSERT INTO konten VALUES (2000, 'Debitoren', NULL, 0);
INSERT INTO konten VALUES (3000, 'Hypothek', NULL, 2);
INSERT INTO konten VALUES (4000, 'Kreditoren', NULL, 2);
INSERT INTO konten VALUES (10000, 'Warenaufwand', NULL, 4);
INSERT INTO konten VALUES (11000, 'Warenertrag', NULL, 5);




# Benutzer mit Schreibberechtigung:
###################################

CREATE USER 'benutzer1'@ '%' IDENTIFIED BY 'helloWorld';

GRANT ALL PRIVILEGES ON * . *
TO 'benutzer1'@ '%'
IDENTIFIED BY '**********'
WITH GRANT OPTION
MAX_QUERIES_PER_HOUR 0
MAX_CONNECTIONS_PER_HOUR 0
MAX_UPDATES_PER_HOUR 0
MAX_USER_CONNECTIONS 0 ;



# Benutzer nur mit Leseberechtigung:
####################################

CREATE USER 'benutzer2'@ '%' IDENTIFIED BY 'phpBuch';

GRANT SELECT ON * . *
TO 'benutzer2'@ '%'
IDENTIFIED BY '*******'
WITH MAX_QUERIES_PER_HOUR 0
MAX_CONNECTIONS_PER_HOUR 0
MAX_UPDATES_PER_HOUR 0
MAX_USER_CONNECTIONS 0 ;



# Summe der Betr�ge auf der Soll-Seite eines Kontos:
####################################################

# SELECT SUM(Buchungen_Betrag) FROM Buchungen WHERE Buchungen_Sollkonto = {bitte Kontennummer einf�gen};


# Summe der Betr�ge auf der Haben-Seite eines Kontos:
#####################################################

# SELECT SUM(Buchungen_Betrag) FROM Buchungen WHERE Buchungen_Habenkonto = {bitte Kontennummer einf�gen};


# Falsche Buchungen (Sollkonto == Habenkonto):
##############################################

# SELECT * FROM Buchungen WHERE Buchungen_Sollkonto = Buchungen_Habenkonto;


