<div class="standard">

	<b>Kontenplan</b>
	<br><br>

	<a href="index.php?seite=1100">Konto hinzuf&uuml;gen</a>

	<br><br>

	<table border="0" cellpadding="0" cellspacing="0">
		<tr height="30">
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="left">Aktiv</td>
			<td colspan="5" align="center">Bilanz</td>
			<td align="right">Passiv</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>	
		<tr>
			<td background="./bilder/linie_h.bmp" colspan="4"/>
			<td><img src="./bilder/linie_k.bmp"></td>
			<td background="./bilder/linie_k.bmp" colspan="4"/>
		</tr>


		<?php

			include("encode.inc");

			$aktiv_array[1000][2];
			$passiv_array[1000][2];

			$aktiv_summe = 0;
			$passiv_summe = 0;;

			$aktiv_id = 0;
			// Verschlüsselt
			// $sql = 'select Konten_ID, decode(Konten_Bezeichnung, "'.$encode_passwort.'") from Konten where Konten_Typ like 0 order by Konten_ID';
			$sql = 'select Konten_ID, Konten_Bezeichnung from Konten where Konten_Typ like 0 order by Konten_ID';
			$result = mysql_query($sql);
			while($row = mysql_fetch_array($result))
			{
				//$aktiv_array[$aktiv_id][0] = $row['decode(Konten_Bezeichnung, "'.$encode_passwort.'")'];
				$aktiv_array[$aktiv_id][0] = $row['Konten_Bezeichnung'];
				$aktiv_array[$aktiv_id][1] = 0;

				$sql2 = 'select * from Buchungen where Buchungen_Sollkonto like '.$row[Konten_ID];
				$result2 = mysql_query($sql2);
				while($row2 = mysql_fetch_array($result2))
				{
					$aktiv_array[$aktiv_id][1] += $row2[Buchungen_Betrag];
				}
				$sql2 = 'select * from Buchungen where Buchungen_Habenkonto like '.$row[Konten_ID];
				$result2 = mysql_query($sql2);
				while($row2 = mysql_fetch_array($result2))
				{
					$aktiv_array[$aktiv_id][1] -= $row2[Buchungen_Betrag];
				}

				$aktiv_summe += $aktiv_array[$aktiv_id][1];

				$aktiv_id++;
			}

			$passiv_id = 0;
			// $sql = 'select Konten_ID, decode(Konten_Bezeichnung, "'.$encode_passwort.'") from Konten where Konten_Typ like 2 order by Konten_ID';
			$sql = 'select Konten_ID, Konten_Bezeichnung from Konten where Konten_Typ like 2 order by Konten_ID';
			$result = mysql_query($sql);
			while($row = mysql_fetch_array($result))
			{
				// $passiv_array[$passiv_id][0] = $row['decode(Konten_Bezeichnung, "'.$encode_passwort.'")'];
				$passiv_array[$passiv_id][0] = $row['Konten_Bezeichnung'];
				$passiv_array[$passiv_id][1] = 0;

				$sql2 = 'select * from Buchungen where Buchungen_Habenkonto like '.$row[Konten_ID];
				$result2 = mysql_query($sql2);
				while($row2 = mysql_fetch_array($result2))
				{
					$passiv_array[$passiv_id][1] += $row2[Buchungen_Betrag];
				}
				$sql2 = 'select * from Buchungen where Buchungen_Sollkonto like '.$row[Konten_ID];
				$result2 = mysql_query($sql2);
				while($row2 = mysql_fetch_array($result2))
				{
					$passiv_array[$passiv_id][1] -= $row2[Buchungen_Betrag];
				}
				
				$passiv_summe += $passiv_array[$passiv_id][1];

				$passiv_id++;
			}

			$anz_bilanz_zeilen = max($aktiv_id, $passiv_id);

			for($i=0; $i<$anz_bilanz_zeilen; $i++)
			{
				echo '<tr valign="center">';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="left" width="100">'.$aktiv_array[$i][0].'</td>';
					echo '<td align="right" width="100">'.nachkommastellen($aktiv_array[$i][1]).'</td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td><img src="./bilder/linie_v.bmp"/></td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="left" width="100">'.$passiv_array[$i][0].'</td>';
					echo '<td align="right" width="100">'.nachkommastellen($passiv_array[$i][1]).'</td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '</tr>';
			}

			echo '<tr>';
				echo '<td background="./bilder/linie_h.bmp" colspan="4"/>';
				echo '<td><img src="./bilder/linie_k.bmp"></td>';
				echo '<td background="./bilder/linie_k.bmp" colspan="4"/>';
			echo '</tr>';

			if($aktiv_summe >= $passiv_summe)
			{
				$saldo = $aktiv_summe-$passiv_summe;

				echo '<tr>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="left" width="100">&nbsp;</td>';
					echo '<td align="right" width="100">&nbsp;</td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td><img src="./bilder/linie_v.bmp"/></td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="right" width="100" colspan="2"><b>Saldo: '.nachkommastellen($saldo).'</b></td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '</tr>';

				$passiv_summe += $saldo;
			}
			else
			{
				$saldo = $passiv_summe-$aktiv_summe;

				echo '<tr>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="right" width="100" colspan="2"><b>Saldo: '.nachkommastellen($saldo).'</b></td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td><img src="./bilder/linie_v.bmp"/></td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="left" width="100">&nbsp;</td>';
					echo '<td align="right" width="100">&nbsp;</td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '</tr>';

				$aktiv_summe += $saldo;
			}

			echo '<tr>';
				echo '<td background="./bilder/linie_h.bmp" colspan="4"/>';
				echo '<td><img src="./bilder/linie_k.bmp"></td>';
				echo '<td background="./bilder/linie_k.bmp" colspan="4"/>';
			echo '</tr>';

			echo '<tr>';
				echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '<td align="left" width="100">&nbsp;</td>';
				echo '<td align="right" width="100">'.nachkommastellen($aktiv_summe).'</td>';
				echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '<td><img src="./bilder/linie_v.bmp"/></td>';
				echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '<td align="left" width="100">&nbsp;</td>';
				echo '<td align="right" width="100">'.nachkommastellen($passiv_summe).'</td>';
				echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
			echo '</tr>';

		?>


		 <!--<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="left" width="100">Kasse</td>
			<td align="right" width="100">1000.00</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><img src="./bilder/linie_v.bmp"/></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="left" width="100">Kasse</td>
			<td align="right" width="100">1000.00</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>--> 

		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="left" width="100">&nbsp;</td>
			<td align="right" width="100">&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><img src="./bilder/linie_v.bmp"/></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="left" width="100">&nbsp;</td>
			<td align="right" width="100">&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
	</table>

	<br><br>

	<table border="0" cellpadding="0" cellspacing="0">
		<tr height="30">
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="left">Aufwand</td>
			<td colspan="5" align="center">Erfolgsrechnung</td>
			<td align="right">Ertrag</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>	
		<tr>
			<td background="./bilder/linie_h.bmp" colspan="4"/>
			<td><img src="./bilder/linie_k.bmp"></td>
			<td background="./bilder/linie_k.bmp" colspan="4"/>
		</tr>


		<?php

			$aufwand_array[1000][2];
			$ertrag_array[1000][2];

			$aufwand_summe = 0;
			$passiv_summe = 0;

			$aufwand_id = 0;
			// $sql = 'select Konten_ID, decode(Konten_Bezeichnung, "'.$encode_passwort.'") from Konten where Konten_Typ like 4 order by Konten_ID';
			$sql = 'select Konten_ID, Konten_Bezeichnung from Konten where Konten_Typ like 4 order by Konten_ID';
			$result = mysql_query($sql);
			while($row = mysql_fetch_array($result))
			{
				// $aufwand_array[$aufwand_id][0] = $row['decode(Konten_Bezeichnung, "'.$encode_passwort.'")'];
				$aufwand_array[$aufwand_id][0] = $row['Konten_Bezeichnung'];
				$aufwand_array[$aufwand_id][1] = 0;

				$sql2 = 'select * from Buchungen where Buchungen_Sollkonto like '.$row[Konten_ID];
				$result2 = mysql_query($sql2);
				while($row2 = mysql_fetch_array($result2))
				{
					$aufwand_array[$aufwand_id][1] += $row2[Buchungen_Betrag];
				}
				$sql2 = 'select * from Buchungen where Buchungen_Habenkonto like '.$row[Konten_ID];
				$result2 = mysql_query($sql2);
				while($row2 = mysql_fetch_array($result2))
				{
					$aufwand_array[$aufwand_id][1] -= $row2[Buchungen_Betrag];
				}

				$aufwand_summe += $aufwand_array[$aufwand_id][1];

				$aufwand_id++;
			}

			$ertrag_id = 0;
			// $sql = 'select Konten_ID, decode(Konten_Bezeichnung, "'.$encode_passwort.'") from Konten where Konten_Typ like 5 order by Konten_ID';
			$sql = 'select Konten_ID, Konten_Bezeichnung from Konten where Konten_Typ like 5 order by Konten_ID';
			$result = mysql_query($sql);
			while($row = mysql_fetch_array($result))
			{
				// $ertrag_array[$ertrag_id][0] = $row['decode(Konten_Bezeichnung, "'.$encode_passwort.'")'];
				$ertrag_array[$ertrag_id][0] = $row['Konten_Bezeichnung'];
				$ertrag_array[$ertrag_id][1] = 0;

				$sql2 = 'select * from Buchungen where Buchungen_Habenkonto like '.$row[Konten_ID];
				$result2 = mysql_query($sql2);
				while($row2 = mysql_fetch_array($result2))
				{
					$ertrag_array[$ertrag_id][1] += $row2[Buchungen_Betrag];
				}
				$sql2 = 'select * from Buchungen where Buchungen_Sollkonto like '.$row[Konten_ID];
				$result2 = mysql_query($sql2);
				while($row2 = mysql_fetch_array($result2))
				{
					$ertrag_array[$ertrag_id][1] -= $row2[Buchungen_Betrag];
				}

				$ertrag_summe += $ertrag_array[$ertrag_id][1];

				$ertrag_id++;
			}

			$anz_erfolgsrechnung_zeilen = max($aufwand_id, $ertrag_id);

			for($i=0; $i<$anz_erfolgsrechnung_zeilen; $i++)
			{
				echo '<tr valign="center">';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="left" width="100">'.$aufwand_array[$i][0].'</td>';
					echo '<td align="right" width="100">'.nachkommastellen($aufwand_array[$i][1]).'</td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td><img src="./bilder/linie_v.bmp"/></td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="left" width="100">'.$ertrag_array[$i][0].'</td>';
					echo '<td align="right" width="100">'.nachkommastellen($ertrag_array[$i][1]).'</td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '</tr>';
			}

			echo '<tr>';
				echo '<td background="./bilder/linie_h.bmp" colspan="4"/>';
				echo '<td><img src="./bilder/linie_k.bmp"></td>';
				echo '<td background="./bilder/linie_k.bmp" colspan="4"/>';
			echo '</tr>';

			if($aufwand_summe >= $passiv_summe)
			{
				$saldo = $aufwand_summe-$ertrag_summe;

				echo '<tr>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="left" width="100">&nbsp;</td>';
					echo '<td align="right" width="100">&nbsp;</td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td><img src="./bilder/linie_v.bmp"/></td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="right" width="100" colspan="2"><b>Saldo: '.nachkommastellen($saldo).'</b></td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '</tr>';

				$ertrag_summe += $saldo;
			}
			else
			{
				$saldo = $ertrag_summe-$aufwand_summe;

				echo '<tr>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="right" width="100" colspan="2"><b>Saldo: '.nachkommastellen($saldo).'</b></td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td><img src="./bilder/linie_v.bmp"/></td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
					echo '<td align="left" width="100">&nbsp;</td>';
					echo '<td align="right" width="100">&nbsp;</td>';
					echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '</tr>';

				$aufwand_summe += $saldo;
			}

			echo '<tr>';
				echo '<td background="./bilder/linie_h.bmp" colspan="4"/>';
				echo '<td><img src="./bilder/linie_k.bmp"></td>';
				echo '<td background="./bilder/linie_k.bmp" colspan="4"/>';
			echo '</tr>';

			echo '<tr>';
				echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '<td align="left" width="100">&nbsp;</td>';
				echo '<td align="right" width="100">'.nachkommastellen($aufwand_summe).'</td>';
				echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '<td><img src="./bilder/linie_v.bmp"/></td>';
				echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				echo '<td align="left" width="100">&nbsp;</td>';
				echo '<td align="right" width="100">'.nachkommastellen($ertrag_summe).'</td>';
				echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
			echo '</tr>';

			
			function nachkommastellen($zahl)
			{
				$zahl_neu = '';

				$komma_position = strpos($zahl, ".");
				if($komma_position==strlen($zahl)-2)
				{
					$zahl_neu = $zahl."0";
				}
				elseif($komma_position==false)
				{
					$zahl_neu = $zahl.".00";
				}
				else
				{
					$zahl_neu = $zahl;
				}

				if(strlen($zahl)==0)
				{
					$zahl_neu = "";
				}

				return $zahl_neu;
			}
		?>


		<!-- <tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="left" width="100">Kasse</td>
			<td align="right" width="100">1000.00</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><img src="./bilder/linie_v.bmp"/></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="left" width="100">Kasse</td>
			<td align="right" width="100">1000.00</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr> -->

		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="left" width="100">&nbsp;</td>
			<td align="right" width="100">&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><img src="./bilder/linie_v.bmp"/></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="left" width="100">&nbsp;</td>
			<td align="right" width="100">&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
	</table>










</div>



