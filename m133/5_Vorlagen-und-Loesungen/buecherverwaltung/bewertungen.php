<?php
	/*Program: Book site from Youngster Library*/
	header('Content-Type: text/html;charset=utf-8;');
	include 'general/session_start_loggedin.php';
?>

<!DOCTYPE HTML>

<html>
	<head>
		<?php include 'general/head.php'; ?>
		<title>Youngster Library - Bewertete Bücher</title>
	</head>

	<body>
		<div id="content">
			<?php include 'general/header.php'; ?>
			
			<div id="main">
				<h1>Meine bewerteten Bücher</h1>
				<?php
					$bookDetails = 'buch_details.php';
				
					define ( 'MYSQL_HOST', 'localhost:3306' );
					define ( 'MYSQL_BENUTZER', 'root' );
					define ( 'MYSQL_KENNWORT', '' );
					define ( 'MYSQL_DATENBANK', 'youngster_library' );
					
					$db_link = @mysqli_connect (
												MYSQL_HOST,
												MYSQL_BENUTZER,
												MYSQL_KENNWORT,
												MYSQL_DATENBANK);
					 
					if ( ! $db_link ){
						echo 'keine Verbindung zur Zeit möglich - später probieren ';
					}
					
					mysqli_set_charset($db_link, 'utf8');
				
					$sql = "
								SELECT *
								FROM buch, bewertung, konto
								WHERE email = '" . $_SESSION['user'] . "' 
								AND buch_id = buch_idfs 
								AND konto_id = konto_idfs 
								ORDER BY jahr
							";
					
					$db_erg = mysqli_query( $db_link, $sql );
					
					while ($daten = mysqli_fetch_array( $db_erg, MYSQL_ASSOC)){
						if($daten['reihe'] == NULL){
							$reihe = "Keine Reihe";
						}else{
							$reihe = $daten['reihe'];
						}
						
						if(strlen($daten['titel']) > 40){
							$kurz = substr($daten['titel'], 0, 40);
							$kurz = trim($kurz);
							if(strlen($kurz) - strrpos($kurz, ' ') < 3){
								$titel = substr($kurz, 0, strrpos($kurz, ' '));
							}else{
								$titel = substr($kurz, 0, strrpos($kurz, ' '));
							}
							$titel .= "...";
						}else{
							$titel = $daten['titel'];
						}
						
						echo '<form name="bearbeiten" action="' . $bookDetails . '" method="POST" accept-charset="utf-8" enctype="multipart/form-data">';
							echo '<table>';
								echo '<tr>';
									echo '<th rowspan="5"><label for="bildbutton[' . $daten['buch_id'] . ']"><img src="' . $daten['bild'] . '" alt="' . $daten['titel'] . '" class="listEditBooks"/></label></th>';
									echo '<th colspan="2"><input type="submit" id="bildbutton[' . $daten['buch_id'] . ']" name="bearbeiten[' . $daten['buch_id'] . ']" value="' . $titel . '" class="bildbutton"></th>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Verfasser:</td>';
									echo '<td>' . $daten['verfasser'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Jahr:</td>';
									echo '<td>' . $daten['jahr'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Verlag:</td>';
									echo '<td>' . $daten['verlag'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Reihe:</td>';
									echo '<td>' . $reihe . '</td>';
								echo '</tr>';
								echo '<tr>';
									if($daten['verfuegbar'] == "ja"){
										echo '<td class="bezeichnungrechts" colspan="2"><img src="images/haekchen.png" alt="Häkchen"/></td>';
										echo '<td>Verfügbar</td>';
									}else{
										echo '<td class="bezeichnungrechts" colspan="2"><img src="images/kreuz.png" alt="Kreuz"/></td>';
										echo '<td>Nicht verfügbar</td>';
									}
								echo '</tr>';
							echo '</table>';
						echo '</form><br><br>';
					}
				?>
			</div>
			<?php include 'general/footer.php'; ?>
		</div>
	</body>
	<?php include 'general/scroll_up.php'; ?>
</html>