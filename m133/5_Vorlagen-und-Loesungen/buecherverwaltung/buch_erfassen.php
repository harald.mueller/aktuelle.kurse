<?php

header('Content.Type: text/html; charset=utf-8');
include 'general/session_start_loggedin.php';
?>
<!DOCTYPE HTML>
<html>
	<head>
		<?php include 'general/head.php';?>
	</head>
	<body>
		<div id="content">
			<?php include 'general/header.php';?>
			<div id="mainnav">
				<?php
					//Muss noch eingefügt werden
				?>
			</div>
			<div id="main">
				<?php
					$titelerror = "";
					$verfassererror = "";
					$jahrerror = "";
					$verlagerror = "";
					$isbnerror = "";
					$isbn2error = "";
					$bilderror = "";
					$beschreibungerror = "";
					$mediengruppeerror = "";
					
					$titel = "";
					$verfasser = "";
					$jahr = "";
					$verlag = "";
					$isbn = "";
					$isbn2 = "";
					$interessenkreis = "";
					$reihe = "";
					$beschreibung = "";
					$mediengruppe = "";
					$verfuegbar = "";
					$done = false;
					
					if(isset($_POST['erfassen']) && $_POST['titel'] != "" && $_POST['verfasser'] != "" && $_POST['jahr'] != "" && $_POST['verlag'] != "" && $_POST['isbn'] != "" && $_POST['beschreibung'] != "" && $_POST['mediengruppe'] != "" && $_FILES['bild']['name'] != "" && preg_match("/^[\d][\d\-]{8,}[\d]$/", $_POST['isbn']) && (preg_match("/^[\d][\d\-]{8,}[\d]$/", $_POST['isbn2']) || $_POST['isbn2'] == "" || $_POST['isbn2'] == "Keine")){
						function eingabebereinugung(&$value, $key){
						
						  $value = strip_tags($value);
						 
						  $value = htmlspecialchars($value, ENT_QUOTES);
						 
						  $value = trim($value);
						}
						
						array_walk ($_POST, 'eingabebereinugung');
						array_walk ($_GET, 'eingabebereinugung');
						array_walk ($_REQUEST, 'eingabebereinugung');
						
						$dateiname = 'images/' . $_FILES['bild']['name'];
						$name = $_FILES['bild']['name'];
						$tmp_name = $_FILES['bild']['tmp_name'];
						$type = $_FILES['bild']['type'];
						
						if($type == "image/jpeg" || $type == "image/png") {
							move_uploaded_file($tmp_name, $dateiname);
						}
						
						$sql = " INSERT INTO buch ";
						$sql .= " SET ";
						$sql .= " titel ='". $_POST['titel'] ."', ";
						$sql .= " verfasser ='". $_POST['verfasser'] ."', ";
						$sql .= " jahr ='". $_POST['jahr'] ."', ";
						$sql .= " verlag ='". $_POST['verlag'] ."', ";
						if($_POST['reihe'] != "" && $_POST['reihe'] != "Keine Reihe"){
							$sql .= " reihe ='". $_POST['reihe'] ."', ";
						}
						$sql .= " interessenkreis_idfs ='". $_POST['interessenkreis'] ."', ";
						$sql .= " isbn ='". $_POST['isbn'] ."', ";
						if($_POST['isbn2'] != "" && $_POST['isbn2'] != "Keine"){
							$sql .= " isbn_2 ='". $_POST['isbn2'] ."', ";
						}
						$sql .= " beschreibung ='". $_POST['beschreibung'] ."', ";
						$sql .= " mediengruppe ='". $_POST['mediengruppe'] ."', ";
						if($_POST['verfuegbar'] == "ja"){
							$sql .= " verfuegbar ='ja', ";
						}else{
							$sql .= " verfuegbar ='nein', ";
						}
						$sql .= " bild ='". $dateiname ."'; ";
						
						define ( 'MYSQL_HOST', 'localhost' );
						define ( 'MYSQL_BENUTZER', 'root' );
						define ( 'MYSQL_KENNWORT', '' );
						define ( 'MYSQL_DATENBANK', 'youngster_library' );
						 
						$db_link = @mysqli_connect (
													MYSQL_HOST,
													MYSQL_BENUTZER,
													MYSQL_KENNWORT,
													MYSQL_DATENBANK);
						
						if ( ! $db_link ){
							die('Keine Verbindung zur Zeit möglich - später probieren ');
						}
						
						$db_erg = mysqli_query( $db_link, $sql );
						
						if ( ! $db_erg ){
							echo 'Ein unerwarteter Fehler ist aufgetreten.';
						}
						
						echo 'Das Buch "' . $_POST['titel'] . '" wurde erfasst.';
						
						$done = true;
					}else if(isset($_POST['erfassen'])){
						
						if($_POST['titel'] == ""){
							$titelerror = "Geben Sie den Titel ein!";
						}else{
							$titel = $_POST['titel'];
						}
						
						if($_POST['verfasser'] == ""){
							$verfassererror = "Geben Sie den Verfasser an!";
						}else{
							$verfasser = $_POST['verfasser'];
						}
						
						if($_POST['jahr'] == ""){
							$jahrerror = "Geben Sie das Jahr an!";
						}else{
							$jahr = $_POST['jahr'];
						}
						
						if($_POST['verlag'] == ""){
							$verlagerror = "Geben Sie den Verlag an!";
						}else{
							$verlag = $_POST['verlag'];
						}
						
						if($_POST['isbn'] == ""){
							$isbnerror = "Geben Sie den ISBN an!";
						}else if(!preg_match("/^[\d][\d\-]{8,}[\d]$/", $_POST['isbn'])){
							$isbnerror = "Geben Sie eine gültige ISBN an!";
							$isbn = $_POST['isbn'];
						}else{
							$isbn = $_POST['isbn'];
						}
						
						if(!preg_match("/^[\d][\d\-]{8,}[\d]$/", $_POST['isbn2']) && $_POST['isbn2'] != "" && $_POST['isbn2'] != "Keine"){
							$isbn2error = "Geben Sie eine gültige ISBN an!";
							$isbn2 = $_POST['isbn2'];
						}else{
							$isbn2 = $_POST['isbn2'];
						}
						
						if($_FILES['bild']['name'] == ""){
							$bilderror = "Geben Sie den Pfad zum Bild an!";
						}
						
						if($_POST['beschreibung'] == ""){
							$beschreibungerror = "Schreiben Sie eine kurze Beschreibung zum Buch!";
						}else{
							$beschreibung = $_POST['beschreibung'];
						}
						
						if($_POST['mediengruppe'] == ""){
							$mediengruppeerror = "Geben Sie die Mediengruppe an!";
						}else{
							$mediengruppe = $_POST['mediengruppe'];
						}
						
						$verfuegbar = $_POST['verfuegbar'];
						$interessenkreis = $_POST['interessenkreis'];
						$reihe = $_POST['reihe'];
					}
					if($done == false){
				?>
				<form name="erfassen" action="<?php echo $_SERVER['PHP_SELF']?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
					<table>
						<tr>
							<td><label class="filterbutton">Bild auswählen<input type="file" data-target="bild" class="input" name="bild"/></label></td>
							<td colspan="3" class="error"><?php echo $bilderror; ?></td>
						</tr>
						<tr>
							<th class="bildtabelle" rowspan="10"><img id="bild" width="200px" src="images/logo_biblio.png" alt="<?php echo $daten['titel'] ?>" class="editDetails"/></th>
							<td>* Titel:</td>
							<td><input autofocus autocomplete="off" type="text" name="titel" value="<?php echo $titel; ?>"/></td>
							<td class="error"><?php echo $titelerror; ?></td>
						</tr>
						<tr>
							<td>Reihe:</td>
							<td><input autocomplete="off" type="text" name="reihe" value="<?php if($reihe == ""){ echo 'Keine Reihe';}else{ echo $reihe;}?>"/></td>
						</tr>
						<tr>
							<td>* Verfasser:</td>
							<td><input autocomplete="off" type="text" name="verfasser" value="<?php echo $verfasser;?>"/></td>
							<td class="error"><?php echo $verfassererror; ?></td>
						</tr>
						<tr>
							<td>* Interessenkreis:</td>
							<td>
								<select name="interessenkreis" class="editDropdown">
									<?php
										define ( 'MYSQL_HOST', 'localhost' );
										define ( 'MYSQL_BENUTZER', 'root' );
										define ( 'MYSQL_KENNWORT', '' );
										define ( 'MYSQL_DATENBANK', 'youngster_library' );
										
										$db_link = @mysqli_connect (
																	MYSQL_HOST,
																	MYSQL_BENUTZER,
																	MYSQL_KENNWORT,
																	MYSQL_DATENBANK);
										 
										if ( ! $db_link ){
											die('keine Verbindung zur Zeit möglich - später probieren ');
										}
										
										$sql = "
													SELECT *
													FROM interessenkreis
													ORDER BY name
												";
										
										$db_erg = mysqli_query( $db_link, $sql );
										
										while ($daten = mysqli_fetch_array( $db_erg, MYSQL_ASSOC)){
											echo '<option ';
											if($interessenkreis == $daten['interessenkreis_id']){
												echo 'selected ';
											}
											echo 'value="' . $daten['interessenkreis_id'] . '">' . $daten['name'] . ' ';
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>* Mediengruppe:</td>
							<td><input autocomplete="off" type="text" name="mediengruppe" value="<?php echo $mediengruppe;?>"/></td>
							<td class="error"><?php echo $mediengruppeerror; ?></td>
						</tr>
						<tr>
							<td>* Jahr:</td>
							<td><input autocomplete="off" type="text" name="jahr" value="<?php echo $jahr;?>"/></td>
							<td class="error"><?php echo $jahrerror; ?></td>
						</tr>
						<tr>
							<td>* Verlag:</td>
							<td><input autocomplete="off" type="text" name="verlag" value="<?php echo $verlag;?>"/></td>
							<td class="error"><?php echo $verlagerror; ?></td>
						</tr>
						<tr>
							<td>* ISBN:</td>
							<td><input autocomplete="off" type="text" name="isbn" value="<?php echo $isbn;?>"/></td>
							<td class="error"><?php echo $isbnerror; ?></td>
						</tr>
						<tr>
							<td>2. ISBN:</td>
							<td><input autocomplete="off" type="text" name="isbn2" value="<?php if($isbn2 == ""){ echo 'Keine';}else{ echo $isbn2;}?>"/></td>
							<td class="error"><?php echo $isbn2error; ?></td>
						</tr>
						<tr>
						<?php
							echo '<td>* Verfügbar:</td>';
							echo '<td><select name="verfuegbar" class="editDropdown">';
								echo '<option value="ja">Verfügbar';
								echo '<option ';
									if($verfuegbar == "nein"){
										echo 'selected ';
									}
								echo 'value="nein">Nicht verfügbar';
							echo '</td>';
						?>
						</tr>
						<tr>
							<td colspan="3"><textarea name="beschreibung" class="editDetails"><?php echo $beschreibung; ?></textarea></td>
							<td class="error"><?php echo $beschreibungerror; ?></td>
						</tr>
					</table>
					
					<?php echo '<input type="submit" name="erfassen" value="Speichern" class="save">'; ?>
				</form>
				<?php } ?>
			</div>
			<?php include 'general/footer.php';?>
		</div>
	</body>
	<?php include 'general/scroll_up.php'; ?>
	<script>
		function previewImage(input) {
		  if (input.files && input.files[0]) {
			var reader = new FileReader(),
				preview = $('#' + $(input).data('target'));

			reader.onload = function(event) {
			  preview.attr('src', event.target.result);
			}

			reader.readAsDataURL(input.files[0]);

		  }
		}

		$(function() {
		  $('.input').on('change', function() {
			previewImage(this);
		  });
		});
	</script>
</html>