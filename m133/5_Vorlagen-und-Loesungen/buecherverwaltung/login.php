<?php
	/*Program: login site from Youngster Library for the clients
	18.06.2015, Robin Göppert*/
	header('Content-Type: text/html;charset=utf-8;');
	include 'general/session_start.php';
?>

<!DOCTYPE HTML>

<html>
<head>
	<?php include 'general/head.php'; ?>
	<title>Youngster Library - Login</title>
</head>

<body>
	<div id="content">
		<?php include 'general/header.php'; ?>
		
		<div id="main">
			<h1 class="login">Login</h1>
			
			<div class="loginfield">
				<form action="verify.php" method="POST">
					<input type="text" name="user" placeholder="E-Mail *" class="login" autocomplete="off" required autofocus/>
					<input type="password" name="password" placeholder="Passwort *" class="login" required/>
					<?php
						if (isset($_POST['backlink'])){
							$backlink = $_POST['backlink'];?>
							<input type="hidden" name="backlink" value="<?php echo $backlink; ?>"/><?php
						}
						else if (isset($_GET['backlink'])){
							$backlink = $_GET['backlink'];?>
							<input type="hidden" name="backlink" value="<?php echo $backlink; ?>"/><?php
						}
					?>
					<p class="pflicht">*) Pflichtfelder</p>
					<input type="submit" value="LOGIN" class="login"/>
				</form>
			</div>
			
			<div class="loginfield">
				<button onclick="location.href='registrieren.php<?php if (isset($backlink)){echo '?backlink='.$backlink; } ?>'" class="registrieren">Jetzt Registrieren!</button>
			</div>
		</div>
		
		<?php include 'general/footer.php'; ?>
	</div>
</body>
</html>