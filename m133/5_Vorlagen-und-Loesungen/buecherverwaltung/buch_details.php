<?php
	/*Program: Book Details site from Youngster Library */
	header('Content-Type: text/html;charset=utf-8;');
	include 'general/session_start.php';
?>

<!DOCTYPE HTML>

<html>
	<head>
		<?php include 'general/head.php'; ?>
		<title>Youngster Library - Buch Details</title>
	</head>

	<body>
		<div id="content">
			<?php include 'general/header.php'; ?>
			
			<div id="main">
				<?php
					if (isset($_POST['bearbeiten']) || isset($_POST['positiv']) || isset($_POST['negativ']) || isset($_GET['bild'])) {
						define ( 'MYSQL_HOST', 'localhost:3306' );
						define ( 'MYSQL_BENUTZER', 'root' );
						define ( 'MYSQL_KENNWORT', '' );
						define ( 'MYSQL_DATENBANK', 'youngster_library' );
						
						$db_link = @mysqli_connect (
													MYSQL_HOST,
													MYSQL_BENUTZER,
													MYSQL_KENNWORT,
													MYSQL_DATENBANK);
						 
						if ( ! $db_link ){
							echo 'keine Verbindung zur Zeit möglich - später probieren ';
						}
						
						mysqli_set_charset($db_link, 'utf8');
						
						if(isset($_SESSION['user']) && $_SESSION['user'] != 'admin'){
							$sql = "
										SELECT konto_id 
										FROM konto 
										WHERE email = '" . $_SESSION['user'] . "'
									";
							
							$db_erg = mysqli_query( $db_link, $sql );
							
							$konto_id = mysqli_fetch_array( $db_erg, MYSQL_ASSOC);
							$konto_id = $konto_id['konto_id'];
						}
						
						if(isset($_POST['bearbeiten'])){
							$id = key($_POST['bearbeiten']);
							
							if(!isset($_SESSION['user']) || $_SESSION['user'] != "admin"){
								$sql = "
											UPDATE buch 
											SET so_oft_gelesen = so_oft_gelesen + 1 
											WHERE buch_id = " . $id . " 
										";
								
								$db_erg = mysqli_query($db_link, $sql);
							}
						}else if(isset($_POST['positiv'])){
						
							$id = key($_POST['positiv']);
							
						}else if(isset($_GET['bild'])){
						
							$id = $_GET['bild'];
							
						}else{
						
							$id = key($_POST['negativ']);
							
						}
						
						if(isset($_POST['positiv']) || isset($_POST['negativ'])){
							$sql = "
										INSERT INTO bewertung(konto_idfs, buch_idfs, bewertung) 
										VALUES (" . $konto_id . ", " . $id . ", ";
							
							if(isset($_POST['positiv'])){
								$sql .= "'Positiv')";
							}else{
								$sql .= "'Negativ')";
							}
							
							$db_erg = mysqli_query( $db_link, $sql );
						}
						
						$sql = "
									SELECT *
									FROM buch
									WHERE buch_id = '" . $id . "'
								";
						
						$db_erg = mysqli_query( $db_link, $sql );
						
						while ($daten = mysqli_fetch_array( $db_erg, MYSQL_ASSOC)){
							
							$sql = "
										SELECT COUNT(*) as positiv 
										FROM bewertung 
										WHERE buch_idfs = " . $daten['buch_id'] . " 
										AND bewertung = 'Positiv' 
									";
							
							$db_ergpos = mysqli_query( $db_link, $sql );
							$positiv = mysqli_fetch_array($db_ergpos, MYSQL_ASSOC);
							
							$sql = "
										SELECT COUNT(*) as negativ 
										FROM bewertung 
										WHERE buch_idfs = " . $daten['buch_id'] . " 
										AND bewertung = 'Negativ' 
									";
							
							$db_ergneg = mysqli_query( $db_link, $sql );
							$negativ = mysqli_fetch_array($db_ergneg, MYSQL_ASSOC);
							
							if($daten['reihe'] == NULL){
								$reihe = "Keine Reihe";
							}else{
								$reihe = $daten['reihe'];
							}
							
							echo '<table class="details">';
								echo '<tr>';
									echo '<th rowspan="8" colspan="4"><img src="' . $daten['bild'] . '" alt="' . $daten['titel'] . '" class="bookDetails"/></th>';
									echo '<th colspan="2" class="bezeichnung"><h1>' . $daten['titel'] . '</h1></th>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Verfasser:</td>';
									echo '<td>' . $daten['verfasser'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Jahr:</td>';
									echo '<td>' . $daten['jahr'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Verlag:</td>';
									echo '<td>' . $daten['verlag'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Reihe:</td>';
									echo '<td>' . $reihe . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Interessenkreis:</td>';
									echo '<td>';
									$sql = "
												SELECT name
												FROM interessenkreis
												WHERE interessenkreis_id = " . $daten['interessenkreis_idfs'] . "
											";
									
									$db_erg2 = mysqli_query( $db_link, $sql );
									
									$interessenkreis = mysqli_fetch_array( $db_erg2, MYSQL_ASSOC);
									echo $interessenkreis['name'];
									echo '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Mediengruppe</td>';
									echo '<td>' . $daten['mediengruppe'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									if($daten['verfuegbar'] == "ja"){
										echo '<td class="bezeichnungrechts"><img src="images/haekchen.png" alt="Häkchen"/></td>';
										echo '<td>Verfügbar</td>';
									}else{
										echo '<td class="bezeichnungrechts"><img src="images/kreuz.png" alt="Kreuz"/></td>';
										echo '<td>Nicht verfügbar</td>';
									}
								echo '</tr>';
									
								echo '<tr>';
									echo '<td width="45px"><img src="images/up.png" alt="Daumen rauf"/></td>';
									echo '<td>' . $positiv['positiv'] . '</td>';
									echo '<td width="45px"><img src="images/down.png" alt="Daumen runter"/></td>';
									echo '<td>' . $negativ['negativ'] . '</td>';
										
								if(isset($_SESSION['user']) && $_SESSION['user'] != 'admin'){
									$sql = "
												SELECT COUNT(*) as bewertet, bewertung 
												FROM bewertung 
												WHERE konto_idfs = " . $konto_id . " 
												AND buch_idfs = " . $id . "
											";
									
									$db_erg2 = mysqli_query( $db_link, $sql );
									
									$bewertung = mysqli_fetch_array( $db_erg2, MYSQL_ASSOC);
									$bewertet = $bewertung['bewertet'];
									$bewertung = $bewertung['bewertung'];
									
										if($bewertet != 0){
											echo '<td class="bezeichnung">Bewertet:</td>';
											if($bewertung == "Positiv"){
												echo '<td colspan="3"><img src="images/up.png" alt="Daumen rauf"/></td>';
											}else{
												echo '<td colspan="3"><img src="images/down.png" alt="Daumen runter"/></td>';
											}
										}else{
											echo '<td class="bezeichnung">Bewerten:</td>';
											echo '<td colspan="3">';
												echo '<form name="bewerten" action="' . $_SERVER['PHP_SELF'] . '" method="POST" accept-charset="utf-8" enctype="multipart/form-data">';
													echo '<input class="bewertenbutton" type="submit" name="positiv[' . $id . ']" value=" " id="positiv"/>';
													echo '<label class="bewertenlabel" for="positiv"><img src="images/up.png" alt="Daumen rauf"/></label>';
													echo '<input class="bewertenbutton" type="submit" name="negativ[' . $id . ']" value=" " id="negativ"/>';
													echo '<label class="bewertenlabel" for="negativ"><img src="images/down.png" alt="Daumen runter"/></label>';
												echo '</form>';
											echo '</td>';
										}
									echo '</tr>';
								}
							echo '</table>';
							echo '<br><br>';
							?>
								<div id="beschreibung">
									<h1>Beschreibung</h1>
									<p><?php echo $daten['beschreibung']; ?></p>
								</div>
							<?php					
						}
					}
					
					else {
						header("Location: buecher.php");
					}
				?>
			</div>
			<?php include 'general/footer.php'; ?>
		</div>
	</body>
	<?php include 'general/scroll_up.php'; ?>
</html>