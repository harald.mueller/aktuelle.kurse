<?php

header('Content.Type: text/html; charset=utf-8');
?>
<!DOCTYPE HTML>
<html>
	<head>
		<?php include 'general/head.php';?>
	</head>
	<body>
		<div id="content">
			<?php include 'general/header.php';?>
			<div id="mainnav">
				<?php
					
				?>
			</div>
			<div id="main">
				<h1>Lage</h1>
				<p>Austellungsstrasse 60<br/>8005 Zürich</p>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1721.9488097352305!2d8.535242809034049!3d47.38297950691926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x8fed983c8d9df33c!2sZ%C3%BCrcher+Hochschule+der+K%C3%BCnste!5e1!3m2!1sde!2sus!4v1434721507712" width="600" height="450" frameborder="0" style="border:0"></iframe>
			</div>
			<?php include 'general/footer.php';?>
		</div>
	</body>
	<?php include 'general/scroll_up.php'; ?>
</html>