<?php
	/*Program: login site from Youngster Library for the clients*/
	header('Content-Type: text/html;charset=utf-8;');
	include 'general/session_start.php';
?>

<!DOCTYPE HTML>

<html>
<head>
	<?php include 'general/head.php'; ?>
	<title>Youngster Library - Registrieren</title>
</head>

<body>
	<div id="content">
		<?php
			include 'general/header.php';
			if (isset($_GET['backlink'])) {
				$backlink = $_GET['backlink'];
			}
		?>
		
		<div id="main">
			<h1 class="login">Registrieren</h1>
			
			<div class="loginfield">
				<form action="verifyNew.php" method="POST">
					<h2>Allgemein</h2>
					<input type="text" name="vorname" placeholder="Vorname *" class="login" autocomplete="off" required autofocus/>
					<input type="text" name="nachname" placeholder="Nachname *" class="login" autocomplete="off" required/>
					<input type="date" name="gebdat" placeholder="Geburtsdatum *" class="login"/>
					<input type="text" name="tel" placeholder="Telefonnummer *" class="login" autocomplete="off" required/>
					<h2>Zugangsdaten</h2>
					<input type="text" name="newUser" placeholder="E-Mail *" class="login" autocomplete="off" required/>
					<input type="password" name="newPassword" placeholder="Passwort *" class="login" required/>
					<input type="password" name="newPassword2" placeholder="Passwort wiederholen *" class="login" required/>
					<p class="pflicht">*) Pflichtfelder</p>
					<?php
						if (isset($backlink)){
							echo '<input type="hidden" name="backlink" value="'.$backlink.'"/>';
						}
					?>
					<input type="submit" value="REGISTRIEREN" class="login"/>
				</form>
			</div>
		</div>
		<?php include 'general/footer.php'; ?>
	</div>
</body>
</html>