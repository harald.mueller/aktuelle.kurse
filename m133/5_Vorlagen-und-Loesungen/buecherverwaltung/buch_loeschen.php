<?php

header('Content.Type: text/html; charset=utf-8');
?>
<!DOCTYPE HTML>
<html>
	<head>
		<?php include 'general/head.php';?>
	</head>
	<body>
		<div id="content">
			<?php include 'general/header.php';?>
			<div id="mainnav">
				<?php
					//Muss noch eingefügt werden
				?>
			</div>
			<div id="main">
				<?php
					define ( 'MYSQL_HOST', 'localhost:3306' );
					define ( 'MYSQL_BENUTZER', 'root' );
					define ( 'MYSQL_KENNWORT', '' );
					define ( 'MYSQL_DATENBANK', 'youngster_library' );
					
					$db_link = @mysqli_connect (
												MYSQL_HOST,
												MYSQL_BENUTZER,
												MYSQL_KENNWORT,
												MYSQL_DATENBANK);
					 
					if ( ! $db_link ){
						echo 'keine Verbindung zur Zeit möglich - später probieren ';
					}
					
					mysqli_set_charset($db_link, 'utf8');
					
					//
					//Löschen
					//
					if(isset($_POST['loeschen'])){
						$id = key($_POST['loeschen']);
						
						$sql = "
									DELETE FROM buch 
									WHERE buch_id = '" . $id . "' ";
						
						$db_erg = mysqli_query($db_link, $sql);
					}
					//
					//Daten vom zu löschenden Datensatz anzeigen
					//
					if(isset($_POST['anzeigen']) && is_array($_POST['anzeigen'])){
						
						$id = key($_POST['anzeigen']);
						
						$sql = '
									SELECT *
									FROM buch
									WHERE buch_id = ' . $id . '
								';
						
						$db_erg = mysqli_query( $db_link, $sql );
						
						$daten = mysqli_fetch_array( $db_erg, MYSQL_ASSOC);
						
						$sql = '
									SELECT *
									FROM interessenkreis
									WHERE interessenkreis_id = ' . $daten['interessenkreis_idfs'] . '
								';
						
						$db_erg = mysqli_query( $db_link, $sql );
						
						$interessenkreis = mysqli_fetch_array( $db_erg, MYSQL_ASSOC);
						
						if($daten['reihe'] == NULL){
							$reihe = "Keine Reihe";
						}else{
							$reihe = $daten['reihe'];
						}
						
						if($daten['isbn_2'] == NULL){
							$isbn2 = "Keine";
						}else{
							$isbn2 = $daten['isbn_2'];
						}
						
						echo '<form name="aendern" action="' . $_SERVER['PHP_SELF'] . '" method="POST" accept-charset="utf-8" enctype="multipart/form-data">';
							echo '<table>';
								echo '<tr>';
									echo '<th rowspan="5"><img src="' . $daten['bild'] . '" alt="' . $daten['titel'] . '"/></th>';
									echo '<th colspan="4">' . $daten['titel'] . '</th>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Verfasser:</td>';
									echo '<td colspan="3">' . $daten['verfasser'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Jahr:</td>';
									echo '<td colspan="3">' . $daten['jahr'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Verlag:</td>';
									echo '<td colspan="3">' . $daten['verlag'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Reihe:</td>';
									echo '<td colspan="3">' . $reihe . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<th colspan="4">Details</th>';	//Ab hier Details
								echo '</tr>';
								echo '<tr>';
									echo '<td>Titel:</td>';
									echo '<td colspan="2">' . $daten['titel'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Verfasser:</td>';
									echo '<td colspan="2">' . $daten['verfasser'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Jahr:</td>';
									echo '<td colspan="2">' . $daten['jahr'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Verlag:</td>';
									echo '<td colspan="2">' . $daten['verlag'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Interessenkreis:</td>';
									echo '<td colspan="2">';
															$sql = "
																		SELECT name
																		FROM interessenkreis
																		WHERE interessenkreis_id = " . $daten['interessenkreis_idfs'] . "
																	";
															
															$db_erg = mysqli_query( $db_link, $sql );
															
															$interessenkreis = mysqli_fetch_array( $db_erg, MYSQL_ASSOC);
															
															echo $interessenkreis['name'];
									echo '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>ISBN:</td>';
									echo '<td colspan="2">' . $daten['isbn'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>2. ISBN:</td>';
									echo '<td colspan="2">' . $isbn2 . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Beschreibung:</td>';
									echo '<td colspan="2">' . $daten['beschreibung'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Reihe:</td>';
									echo '<td colspan="2">' . $reihe . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Mediengruppe:</td>';
									echo '<td colspan="2">' . $daten['mediengruppe'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<th colspan="3"><input type="submit" name="loeschen[' . $id . ']" value="Löschen"></th>';
								echo '</tr>';
							echo '</table>';
						echo '</form>';
					}else{
						//
						//Alle Bücher anzeigen
						//
						$sql = "
									SELECT *
									FROM buch
									ORDER BY jahr
								";
						
						$db_erg = mysqli_query( $db_link, $sql );
						
						while ($daten = mysqli_fetch_array( $db_erg, MYSQL_ASSOC)){
							if($daten['reihe'] == NULL){
								$reihe = "Keine Reihe";
							}else{
								$reihe = $daten['reihe'];
							}
							
							echo '<form name="bearbeiten" action="' . $_SERVER['PHP_SELF'] . '" method="POST" accept-charset="utf-8" enctype="multipart/form-data">';
								echo '<table>';
									echo '<tr>';
										echo '<th rowspan="5"><label for="bildbutton[' . $daten['buch_id'] . ']"><img src="' . $daten['bild'] . '" alt="' . $daten['titel'] . '"/></label></th>';
										echo '<th colspan="2"><input type="submit" id="bildbutton[' . $daten['buch_id'] . ']" name="anzeigen[' . $daten['buch_id'] . ']" value="' . $daten['titel'] . '"></th>';
									echo '</tr>';
									echo '<tr>';
										echo '<td>Verfasser:</td>';
										echo '<td>' . $daten['verfasser'] . '</td>';
									echo '</tr>';
									echo '<tr>';
										echo '<td>Jahr:</td>';
										echo '<td>' . $daten['jahr'] . '</td>';
									echo '</tr>';
									echo '<tr>';
										echo '<td>Verlag:</td>';
										echo '<td>' . $daten['verlag'] . '</td>';
									echo '</tr>';
									echo '<tr>';
										echo '<td>Reihe:</td>';
										echo '<td>' . $reihe . '</td>';
									echo '</tr>';
								echo '</table>';
							echo '</form>';
						}
					}
				?>
			</div>
			<?php include 'general/footer.php';?>
		</div>
	</body>
</html>