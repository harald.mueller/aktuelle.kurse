<?php
	session_start();
	
	if (isset($_SESSION["user"]) and (time() - $_SESSION["timestamp"] < 600)){
		$_SESSION["timestamp"] = time();
	}
	
	else if (isset($_SESSION['user'])) {
		$user = $_SESSION['user'];
		unset($_SESSION["user"], $_SESSION["timestamp"]);
		$conn = mysqli_connect("localhost", "root", "", "youngster_library") OR
			die("Error " . mysqli_error($conn));
		mysqli_set_charset($conn, 'utf8');
		mysqli_query($conn, "UPDATE konto SET angemeldet = '0' WHERE email = '$user'");
		$backlink = basename($_SERVER['SCRIPT_NAME']);
		header("Location: login.php?backlink=$backlink");
		exit;
	}
	
	else {
		header("Location: buecher.php");
		exit;
	}
?>