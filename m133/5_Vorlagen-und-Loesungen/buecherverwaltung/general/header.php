<?php
	/*Program: header content of the website Youngster Library */
?>

<div>
	<img src="images/buecher2.png" alt="Logo Youngster Library" class="logo"/>
	<h1 class="title">Youngster Library</h1>
</div>

<div id="mainnav">
	<ul class="top">
		<li>
			<a href="index.php" <?php if (basename($_SERVER['SCRIPT_NAME']) == 'index.php'){echo 'class="active"';} ?> >Home</a>
		</li>
		<li>
			<a href="buecher.php" <?php if (basename($_SERVER['SCRIPT_NAME']) == 'buecher.php' OR basename($_SERVER['SCRIPT_NAME']) == 'buch_details.php' OR basename($_SERVER['SCRIPT_NAME']) == 'buch_bearbeiten.php' OR basename($_SERVER['SCRIPT_NAME']) == 'buch_erfassen.php'){echo 'class="active"';} ?> >Bücher</a>
		</li>
		<?php if(isset($_SESSION['user']) && $_SESSION['user'] != "admin"){ ?>
		<li>
			<a <?php if (basename($_SERVER['SCRIPT_NAME']) == 'ausleihen.php' OR basename($_SERVER['SCRIPT_NAME']) == 'bewertungen.php'){echo 'class="active"';} ?> >MyLibrary</a>
			<ul>
				<li class="submenue">
					<a href="bewertungen.php">Meine Bewertungen</a>
				</li>
			</ul>
		</li>
		<?php }?>
		<li>
			<a href="buch_suchen.php" <?php if (basename($_SERVER['SCRIPT_NAME']) == 'buch_suchen.php'){echo 'class="active"';} ?> >Buch suchen</a>
		</li>
		
		<?php
			if (!isset($_SESSION['user'])){
				?>
					<li class="user">
						<a href="login.php?backlink=<?php echo basename($_SERVER['SCRIPT_NAME']); ?>" <?php if (basename($_SERVER['SCRIPT_NAME']) == 'login.php'){echo 'class="active"';} ?> >Login</a>
					</li>
				<?php
			}
			
			else {
				$conn = mysqli_connect("localhost:3306", "root", "", "youngster_library") OR
					die("Error " . mysqli_error($conn));
				mysqli_set_charset($conn, 'utf8');
				
				$sessionuser = $_SESSION['user'];
				
				$vorname = current($conn->query("SELECT vorname FROM konto WHERE email='".$sessionuser."'")->fetch_assoc());
				
				echo '<li class="user">';
				echo '<a>'.$vorname.'</a>';
				?>
					<ul>
						<li class="submenue">
							<a href="logout.php?backlink=<?php echo basename($_SERVER['SCRIPT_NAME']) ?>">Logout</a>
						</li>
					</ul>
				<?php
				echo '</li>';
			}
		?>
	</ul>
</div>
