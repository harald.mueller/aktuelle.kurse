<?php
	/*Program: Book site from Youngster Library*/
	header('Content-Type: text/html;charset=utf-8;');
	include 'general/session_start.php';
?>

<!DOCTYPE HTML>

<html>
	<head>
		<?php include 'general/head.php'; ?>
		<title>Youngster Library - Bücher</title>
	</head>

	<body>
		<div id="content">
			<?php include 'general/header.php'; ?>
			
			<div id="main">
				<h1>Unsere Bücher</h1>
				<?php
					$filtern = "";
					
					if(isset($_SESSION['user']) && $_SESSION['user'] == 'admin' && isset($_POST['filtern'])){
						$filtern = $_POST['filternnach'];
					}
					
					if (isset($_SESSION['user']) && $_SESSION['user'] == 'admin'){
						echo '<a href="buch_erfassen.php"><img src="images/web_plus.png" alt="Buch hinzufügen" class="plus"/></a>';
					}
			
					if (isset($_SESSION['user'])){
						if ($_SESSION['user'] == 'admin') {
							$bookDetails = 'buch_bearbeiten.php';
						}
						else {
							$bookDetails = 'buch_details.php';
						}
					}
					else {
						$bookDetails = 'buch_details.php';
					}
					
				
					define ( 'MYSQL_HOST', 'localhost:3306' );
					define ( 'MYSQL_BENUTZER', 'root' );
					define ( 'MYSQL_KENNWORT', '' );
					define ( 'MYSQL_DATENBANK', 'youngster_library' );
					
					$db_link = @mysqli_connect (
												MYSQL_HOST,
												MYSQL_BENUTZER,
												MYSQL_KENNWORT,
												MYSQL_DATENBANK);
					 
					if ( ! $db_link ){
						echo 'keine Verbindung zur Zeit möglich - später probieren ';
					}
					
					mysqli_set_charset($db_link, 'utf8');
				
					$sql = "
								SELECT *
								FROM buch 
							";
					if(isset($_SESSION['user']) && $_SESSION['user'] == 'admin' && isset($_POST['filtern']) && $_POST['filternnach'] != "alle"){
						$sql .= "
									WHERE verfuegbar = '" . $filtern . "'
								";
					}
					$sql .= "
								ORDER BY jahr
							";
					
					$db_erg = mysqli_query( $db_link, $sql );
					
					if(isset($_SESSION['user']) && $_SESSION['user'] == 'admin'){
						echo '<form name="filtern" action="' . $_SERVER['PHP_SELF'] .'" method="POST" accept-charset="utf-8" enctype="multipart/form-data">';
							echo '<table>';
								echo '<tr>';
									echo '<th>';
										echo '<select name="filternnach" class="filternDropdown">';
											echo '<option ';
												if($filtern == "alle"){
													echo 'selected ';
												}
											echo 'value="alle">Alle';
											echo '<option ';
												if($filtern == "nein"){
													echo 'selected ';
												}
											echo 'value="nein">Ausgeliehen';
											echo '<option ';
												if($filtern == "ja"){
													echo 'selected ';
												}
											echo 'value="ja">Nicht ausgeliehen';
										echo '</select>';
									echo'</th>';
									echo '<th><input type="submit" name="filtern" value="Filtern" class="filterbutton"></th>';
								echo '</tr>';
							echo '</table>';
						echo '</form><br/>';
					}
					
					while ($daten = mysqli_fetch_array( $db_erg, MYSQL_ASSOC)){
						
						$sql = "
									SELECT COUNT(*) as positiv 
									FROM bewertung 
									WHERE buch_idfs = " . $daten['buch_id'] . " 
									AND bewertung = 'Positiv' 
								";
						
						$db_ergpos = mysqli_query( $db_link, $sql );
						$positiv = mysqli_fetch_array($db_ergpos, MYSQL_ASSOC);
						
						$sql = "
									SELECT COUNT(*) as negativ 
									FROM bewertung 
									WHERE buch_idfs = " . $daten['buch_id'] . " 
									AND bewertung = 'Negativ' 
								";
						
						$db_ergneg = mysqli_query( $db_link, $sql );
						$negativ = mysqli_fetch_array($db_ergneg, MYSQL_ASSOC);
						
						if($daten['reihe'] == NULL){
							$reihe = "Keine Reihe";
						}else{
							$reihe = $daten['reihe'];
						}
						
						if(strlen($daten['titel']) > 40){
							$kurz = substr($daten['titel'], 0, 40);
							$kurz = trim($kurz);
							if(strlen($kurz) - strrpos($kurz, ' ') < 3){
								$titel = substr($kurz, 0, strrpos($kurz, ' '));
							}else{
								$titel = substr($kurz, 0, strrpos($kurz, ' '));
							}
							$titel .= "...";
						}else{
							$titel = $daten['titel'];
						}
						
						echo '<form name="bearbeiten" action="' . $bookDetails . '" method="POST" accept-charset="utf-8" enctype="multipart/form-data">';
							echo '<table>';
								echo '<tr>';
									echo '<th rowspan="5" colspan="4"><label for="bildbutton[' . $daten['buch_id'] . ']"><img src="' . $daten['bild'] . '" alt="' . $daten['titel'] . '" class="listEditBooks"/></label></th>';
									echo '<th colspan="2"><input type="submit" id="bildbutton[' . $daten['buch_id'] . ']" name="bearbeiten[' . $daten['buch_id'] . ']" value="' . $titel . '" class="bildbutton"></th>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Verfasser:</td>';
									echo '<td>' . $daten['verfasser'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Jahr:</td>';
									echo '<td>' . $daten['jahr'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Verlag:</td>';
									echo '<td>' . $daten['verlag'] . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td class="bezeichnung">Reihe:</td>';
									echo '<td>' . $reihe . '</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td><img  class="daumen"src="images/up.png" alt="Daumen rauf"/></td>';
									echo '<td>' . $positiv['positiv'] . '</td>';
									echo '<td><img class="daumen" src="images/down.png" alt="Daumen runter"/></td>';
									echo '<td>' . $negativ['negativ'] . '</td>';
									if($daten['verfuegbar'] == "ja"){
										echo '<td class="bezeichnungrechts"><img src="images/haekchen.png" alt="Häkchen"/></td>';
										echo '<td>Verfügbar</td>';
									}else{
										echo '<td class="bezeichnungrechts"><img src="images/kreuz.png" alt="Kreuz"/></td>';
										echo '<td>Nicht verfügbar</td>';
									}
								echo '</tr>';
							echo '</table>';
						echo '</form><br><br>';
					}
				?>
			</div>
			<?php include 'general/footer.php'; ?>
		</div>
	</body>
	<?php include 'general/scroll_up.php'; ?>
</html>