# m319 Applikationen entwerfen und implementieren

## Unterlagen

- Schweiz 
	- [https://www.modulbaukasten.ch/](https://www.modulbaukasten.ch/)
	- [Moduldefinition](https://www.modulbaukasten.ch/module/c6b67ea8-5e85-eb11-a812-0022486f644f/de-DE?title=Applikationen-entwerfen-und-implementieren)

- Kanton ZH 
	- [https://gitlab.com/modulentwicklungzh/cluster-api/m319](https://gitlab.com/modulentwicklungzh/cluster-api/m319)

- TBZ 
	- für Lehrpersonen [https://gitlab.com/ch-tbz-it/TE/m319](https://gitlab.com/ch-tbz-it/TE/m319)
	- für Schüler [https://gitlab.com/ch-tbz-it/Stud/m319](https://gitlab.com/ch-tbz-it/Stud/m319)
	
- Arbeitsbereiche (Miro-Boards)
	- [Klasse PE23a](https://miro.com/app/board/uXjVN4TSRiU=/?share_link_id=997638238801) Di Mo
	- [Klasse PE23b](https://miro.com/app/board/uXjVN4TSRpU=/?share_link_id=232198517250) Di Na
	- [Klasse PE23c](https://miro.com/app/board/uXjVN4TSR1c=/?share_link_id=803921773170) Do Mo
	- [Klasse PE23d](https://miro.com/app/board/uXjVN4TSREg=/?share_link_id=843875990674) Fr Na

- Projekte zum selber programmieren (Niveau 4)
	- [N4-Ideen-Zum-Selber-Programmieren](https://gitlab.com/ch-tbz-it/Stud/m319/-/blob/main/N4-Ideen-Zum-Selber-Programmieren/)


## Ablaufplan 2024-Q3 (Dienstag)

|Tag  |PE23a, PE23b| Thema, Auftrag, Übung |
|---- |----        |----                   |
|  1  | 20.2.2024  | Einführung<br>- Modul und Spielfeld<br>- Miro & Tandems<br>- Ecolm (Tests)<br>- SOL vorstellen & Rahmen/Regeln<br>- Bearbeitung Topic 1 (IDE)<br>- GitLab (optional) |
|  2  | 27.2.2024  | Tagesablauf & Organisation SOL<br>- Kompetenznachweise KN<br>- Einführung [ECOLM](https://ecolm.com) |
|  3  | 05.3.2024  | Selbstorgansisiertes Lernen (SOL) und Kompetenznachweise (KN) |
|  4  | 12.3.2024  | SOL & KN |
|  5  | 19.3.2024  | SOL & KN |
|  6  | 26.3.2024  | SOL & KN |
|  7  | 02.4.2024  | SOL & KN |
|  8  | 09.4.2024  | SOL & KN |
|  9  | 16.4.2024  | SOL & KN |
|  -  | 23.4.2024  | - Ferien -  |
|  -  | 30.4.2024  | - Ferien -  |
| 10  | 07.5.2024  | SOL & KN und Abschluss |


## Ablaufplan 2024-Q3 (Donnerstag morgens)

|Tag  |PE23c       | Thema, Auftrag, Übung |
|---- |----        |----                   |
|  1  | 22.2.2024  | Einführung<br>- Modul und Spielfeld<br>- Miro & Tandems<br>- Ecolm (Tests)<br>- SOL vorstellen & Rahmen/Regeln<br>- Bearbeitung Topic 1 (IDE)<br>- GitLab (optional) |
|  2  | 29.2.2024  | Tagesablauf & Organisation SOL<br>- Kompetenznachweise KN<br>- Einführung [ECOLM](https://ecolm.com) |
|  3  | 07.3.2024  | Selbstorgansisiertes Lernen (SOL) und Kompetenznachweise (KN) |
|  4  | 14.3.2024  | SOL & KN |
|  5  | 21.3.2024  | SOL & KN |
|  6  | 28.3.2024  | SOL & KN |
|  7  | 04.4.2024  | SOL & KN |
|  8  | 11.4.2024  | SOL & KN |
|  9  | 18.4.2024  | SOL & KN und Abschluss |
|  -  | 25.4.2024  | - Ferien -  |
|  -  | 02.5.2024  | - Ferien -  |
|  -  | 09.5.2024  | - Auffahrt - |


## Ablaufplan 2024-Q3 (Freitag nachmittags)

|Tag  |PE23d       | Thema, Auftrag, Übung |
|---- |----        |----                   |
|  1  | 23.2.2024  | Einführung<br>- Modul und Spielfeld<br>- Miro & Tandems<br>- Ecolm (Tests)<br>- SOL vorstellen & Rahmen/Regeln<br>- Bearbeitung Topic 1 (IDE)<br>- GitLab (optional) |
|  2  | 01.3.2024  | Tagesablauf & Organisation SOL<br>- Kompetenznachweise KN<br>- Einführung [ECOLM](https://ecolm.com) |
|  3  | 08.3.2024  | Selbstorgansisiertes Lernen (SOL) und Kompetenznachweise (KN) |
|  4  | 15.3.2024  | SOL & KN |
|  5  | 22.3.2024  | SOL & KN |
|  -  | 29.3.2024  | - Karfreitag - |
|  6  | 05.4.2024  | SOL & KN |
|  7  | 12.4.2024  | SOL & KN |
|  8  | 19.4.2024  | SOL & KN und Abschluss |
|  -  | 26.4.2024  | - Ferien -  |
|  -  | 03.5.2024  | - Ferien -  |
|  -  | 10.5.2024  | - Freitag nach Auffahrt - |



![learning map](./learning-map.png) 

