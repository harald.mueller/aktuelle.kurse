# M150 - E-Business-Applikationen anpassen

## Identifikation

[> **Modulidentifikation** ](https://www.modulbaukasten.ch/modul/1875c9da-716c-eb11-b0b1-000d3a830b2b)
 - 1 Aufbau der Applikation, Transaktionskonzept, Applikationsumgebung und Rahmenbedingungen (Sicherheit, Performance, Verfügbarkeit, Transaktionsvolumen, usw.) erfassen.
 - 2 Vorgabe analysieren, clientseitigen, serverseitigen und datenbankseitigen Änderungsbedarf formulieren.
 - 3 Auswirkungen der Änderungen auf Sicherheit und Schutzwürdigkeit der Informationen bei allen beteiligten Komponenten wie Client, Webserver, Applikationsserver und Datenbankserver überprüfen und dokumentieren
 - 4 Änderungen inklusive Implementierung und Test (funktional und nicht-funktional) gemäss einem vordefinierten Änderungsprozess planen.
 - 5 Änderungen realisieren, testen und dokumentieren.


<hr>
![wertschoepfungkette](material/wertschoepfungkette.jpg)
<br>
[Folien Wertschöpfungskette)](https://www.tbzwiki.ch/images/4/4c/Folien_Wertsch%C3%B6pfungskette.pdf)
<br>


[TOC]

<br>
<hr>


## Ablaufplan <mark>AP20c</mark> (Donnerstag nachmittags)

| Tag| Datum | Thema                            |
|----| ----- | -----                            |
|  1 | 16.11.| Einführung und Start LB1         |
|  2 | 23.11.| Arbeit für/auf LB1               |
|----| 30.11 | --- fällt aus, Klausurtagung     |
|  4 | 07.12.| Arbeit für/auf LB1 / Beginn/Arbeit an LB3, <br>(**LB1**)-Fachgespräche 1 einzeln    |
|  5 | 14.12.| Arbeit an LB3, <br>(**LB1**)-Fachgespräche 2 einzeln               |
|  6 | 21.12.| Arbeit an LB3                    |
|----| ----- | -- Ferien                        |
|  7 | 11.01.| Schriftlicher Auftrag für 4 Lektionen (**LB2**a oder **LB2**b)   |
|  8 | 18.01.| Arbeit an LB3                    |
|  9 | 25.01.| Arbeit an LB3                    |
| 10 | 01.02.| Abgabe und Bewertung **LB3**     |

<br>

## Leistungsbeurteilungen

### LB1 (30%, Fachgespräch, ca. 12 min) - **Tag 4** und **Tag 5**
- Wie sind e-Business-Applikationen aufgebaut (Architektur, Aufteilung horizontal / vertikal)?
- Wie laufen die (internen) Transaktionen ab (Produkt einpflegen, Zahlungsablauf, ...)?
- Was muss man bezüglich Sicherheit, Performance, Verfügbarkeit, Transaktionsvolumen beachten und was sind die technischen Antworten dafür/dagegen?
- Was muss beachtet werden bei Änderungsbedarf (Release) "von gut besuchten" (oder gar von hoch kritischen) e-Business-Appl.?
- Wie macht man Änderungen (Syst. Update) bei "kritischen" oder "hoch verfügbaren" e-Business-Appl. bezüglich DB, Frontend, Backend / Server?
- [Fragenkatalog](./docs/M150_LB1_Fragenkatalog.pdf)

**_Vor oder nach der mündlichen Prüfung kann/soll schon mit LB4 begonnen werden_**

### LB2 (30%, Schriftlicher Auftrag, Aufwand ca. 200 min) - **Tag 7**

Einzelauftrag/Einzelabgabe (Zusammenarbeit bei der Faktenbeschaffung möglich)

- Auftrag: "**Web-Shop Vergleich**"
<br>- Vergleich zweier konkurrenzierender und etwa gleich grosser 
<br>  e-Commerce-Systeme wie z.B. (Coop/Migros, Brack/Digitec, Nike/Adidas, Alibaba/Amazon, ...)

[**LB2a Arbeitsauftrag**](./docs/M150_LB2_Arbeitsauftrag.pdf) 


***ODER***

Als Einzelauftrag/Einzelabgabe (Zusammenarbeit bei der Faktenbeschaffung möglich)

- Vertieftes Studium eines,
<br> oder der Vergleich zweier 
<br> oder einen Überblick über 
<br> verschiedene auf dem Markt existierende und **_einbindbare_** 
<br>	-	Zahlungs-,
<br>	-	eCommerce-,
<br>	-	eShop- oder
<br>	-	Standardwebshop-Systeme.

[**LB2b Arbeitsauftrag**](./docs/M150_LB3_Arbeitsauftrag.pdf) 

<https://www.tbzwiki.ch/index.php?title=EPayment>
<br><https://www.tbzwiki.ch/index.php?title=Paynet>
<br><https://www.sellxed.com/de/start>
<br><https://www.payrexx.com/de/home>
<br>Siehe auch die Linkliste in [Vertiefungsarbeit-Payment-Systeme.md](./Vertiefungsarbeit-Payment-Systeme.md)
<br>

### LB3 (40%, frei wählbare Vertiefungsarbeit als Dokument oder Produkt, 1-3 Personen) 
- [Ziel/Anforderung/Beschreibung für das Vertiefungsthema](./docs/M150_LB4_Vertiefungsthema.pdf).
- [Themenliste](https://www.tbzwiki.ch/index.php?title=Themenliste_Modul_150)
- Weitere:
<br>	-- [Paymentsysteme in Web-Shops](./Vertiefungsarbeit-Payment-Systeme.md)
<br>	-- 
<br>	-- 

## Unterlagen

### Wiki

<https://www.tbzwiki.ch/index.php?title=Modul_150>

### Bücher

[Bücher zum E-Commerce](https://tbzedu-my.sharepoint.com/:f:/g/personal/harald_mueller_tbz_ch/ErB2x1XS5oxAgyVXZ5888x8BrMg0p8lj0Bx0zxmiDO3nkQ?e=RK7iWb)

(nur mit Microsoft-TBZ-Account)

[![Deges_Grundlagen-des-E-Commerce](material/Deges_Grundlagen-des-E-Commerce.jpg)](https://tbzedu-my.sharepoint.com/:b:/g/personal/harald_mueller_tbz_ch/EfkoF5d8VCtNpXTGFrlLOQgBbhptKszKS9r62jD-ylIzoA?e=T4qHrI)
[![Heinemann_Der-neue-Online-Handel](material/Heinemann_Der-neue-Online-Handel.jpg)](https://tbzedu-my.sharepoint.com/:b:/g/personal/harald_mueller_tbz_ch/ESnBmyHDIGxEj3_N7smbpIgBSUkgy9LRSKN4pJRSxZZY9g?e=weJKGh)
[![Schluesselfaktoren-im-E-Commerce](material/Grosse-Holtforth_Schluesselfaktoren-im-E-Commerce.jpg)](https://tbzedu-my.sharepoint.com/:b:/g/personal/harald_mueller_tbz_ch/EVLGOZSAAohBkLQmKgJdfx4BrV82S7tJ1bJEEKCkBshfsw?e=k0tghC)

