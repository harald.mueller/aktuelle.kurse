#UX/UI Design

![Ein Bild, das Text enthält. Automatisch generierte Beschreibung](media/c32ed33bf50dd9bdef9d94a2827bd558.png) 3 min, D, 2019 https://www.youtube.com/watch?v=LC_sgNFINVg

![](media/4c9486e122f962e5128191647c461d47.png) 12 min, D, 2018 https://www.youtube.com/watch?v=qTU5XelOIWo

**Weiteres:**

[Friedemann - YouTube](https://www.youtube.com/c/FriedemannUX) / https://www.youtube.com/c/FriedemannUX

![](media/eff38d7c2c4cdd8a4094f4cb5ecee5fd.png) 3 min, D, 2022 https://www.youtube.com/watch?v=zeyxamckbtk

![Ein Bild, das Text, Zeitung, Screenshot, Schild enthält. Automatisch generierte Beschreibung](media/e18a731097cd718927fcb44886b781c3.png) 30min, E, 2022 https://www.youtube.com/watch?v=1PHnzrhAnfw

![](media/5ba19808442fe2d35c55583d46c2ceec.png) 10 min, E, 2022 https://www.youtube.com/watch?v=5CxXhyhT6Fc

**Weiteres:**

[CareerFoundry - YouTube](https://www.youtube.com/user/careerfoundry) / https://www.youtube.com/user/careerfoundry

![Ein Bild, das Text enthält. Automatisch generierte Beschreibung](media/23fd2ba3c396623cb09f7cb83760ddc1.png) https://www.youtube.com/playlist?list=PL4GEkVtNYGlLyMjkNBDBxC6YFLxgOV28t
