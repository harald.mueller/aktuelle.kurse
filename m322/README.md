# M322 Benutzerschnittstellen entwerfen und implementieren

## Unterlagen

- schweizweit:
[https://www.modulbaukasten.ch](https://www.modulbaukasten.ch),
-> [Moduldefinition](https://www.modulbaukasten.ch/module/322/1/de-DE?title=Benutzerschnittstellen-entwerfen-und-implementieren)

- kantonal (ZH): 
[https://gitlab.com/modulentwicklungzh/cluster-api/m322](https://gitlab.com/modulentwicklungzh/cluster-api/m322)

- TBZ: 
  **Schüler** [gitlab.com/ch-tbz-it/Stud/m322](https://gitlab.com/ch-tbz-it/Stud/m322)
, Lehrpersonen [gitlab.com/ch-tbz-it/TE/m322](https://gitlab.com/ch-tbz-it/TE/m322)
	
## Literatur

- [Steimle_Collaborative-UX-Design.pdf](./literatur/Steimle_Collaborative-UX-Design.pdf)
- [Kuhn_UX-Design Definition-und-Grundlagen.epub](./literatur/Kuhn_UX-Design---Definition-und-Grundlagen.epub)
- [Whalen_Think-Human_-Kundenzentriertes_UX.pdf](./literatur/Whalen_Think-Human_-Kundenzentriertes_UX.pdf)

# Modulablauf

<https://miro.com/app/board/uXjVOj5dQiU=/?share_link_id=853850984798>

<https://miro.com/app/board/uXjVOj5dQiU=/?share_link_id=878451205410>


# Weitere Unterlagen
<https://teams.microsoft.com/l/channel/19%3a44549f5671fb4b039e2e2643d20e6f3d%40thread.tacv2/m322?groupId=d87b9fcd-1e7d-483b-9e5e-7c34337e9e7e&tenantId=f733fdc5-1255-4d8d-b793-2bc7aca0f214>


# Einleitung
[einleitung](./einleitung.md)