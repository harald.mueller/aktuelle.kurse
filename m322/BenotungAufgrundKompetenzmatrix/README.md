# Kompetenzmatrix - Modul 322 - Benutzerschnittstellen entwerfen und implementieren


## Kompetenz

**Entwirft und implementiert Benutzerschnittstellen für eine Applikation. Beachtet dabei Standards und ergonomische Anforderungen.**

## Matrix

| Kompetenzband: | HZ | Grundlagen      | Fortgeschritten | Erweitert      |
| -------------- | -- | --------------- | --------------- | -------------- |
| Band A<br />**Analysiert Benutzereigenschaften und Nutzungsumfeld bezüglich der mit dem System zu lösender Aufgabe und dokumentiert sie** |  **1**  |   |   |   |
| Anforderungsanalyse verstehen | 1.1 | A1G:<br/>Ich kann die Bestandteile einer Anforderungsanalyse nennen (z.B.  User, Aufgaben, Kontext und Technologie) | A1F:<br/>Ich kann die Ergebnisse einer Anforderungsanalyse erläutern (z.B. User, Aufgaben, Kontext und Technologie). | A1E:<br/>Ich kann eine bestehende Anforderungsanalyse hinterfragen und Verbesserungen vorschlagen. |
| Vorgehensmodell wählen | 1.2 | A2G:<br/>Ich kann ein geeignetes Vorgehensmodell zur Erhebung von Anforderung nennen und seine Phasen aufzählen (z.B. Design Thinking, User Centered Design). | A2F:<br/>Ich kann den Sinn und Zweck eines Vorgehensmodells erklären und seine Phasen erläutern (z.B. Design Thinking, User Centered Design). | A2E:<br/>Ich kann ein iteratives Vorgehensmodell begründet auswählen. |
| Nutzungskontext verstehen | 1.3 | A3G:<br/>Ich kann eine Methode zur Analyse von Nutzungsumfeld, Aufgaben und Benutzerverhalten erklären (z.B. Beobachtung, Interview, Loganalyse). | A3F:<br/>Ich kann eine Methode zur Analyse von Nutzungsumfeld, Aufgaben und Benutzerverhalten anwenden (z.B. Beobachtung, Interview, Loganalyse). | A3E:<br/>Ich kann die gewonnen Erkenntnisse aus der Anwendung einer Methode sinnvoll in Anforderungen überführen. |
| Benutzereigenschaften erfassen | 1.4 | A4G<br/>Ich kann den Zweck einer methodischen Erfassung von Benutzereigenschaften, Zielen und Bedürfnissen begründen. | A4F:<br/>Ich kann aus Benutzereigenschaften, Zielen und Bedürfnissen die wichtigsten Benutzerprofile ableiten und methodisch erfassen (z.B. Persona, Empathy Map). | A4E:<br/>Ich kann Benutzereigenschaften, Ziele und Bedürfnisse hinterfragen und Benutzerprofile dahingehend optimieren. |
| Nutzungsanforderungen spezifizieren | 1.5 | A5G:<br/>Ich kann eine Methode zur Dokumentation und Spezifikation von Nutzungsanforderungen erklären (z.B. Use Cases, User Stories). | A5F:<br/>Ich kann eine Methode zur Dokumentation und Spezifikation von Nutzungsanforderungen anwenden (z.B. Use Cases, User Stories). | A5E:<br/>Ich kann eine Methode zur Bewertung und Priorisierung von Nutzungsanforderungen anwenden (z.B. Backlog). |
|  |  |  |  |  |
| Band B<br />**Entwirft Varianten einer Benutzerschnittstelle (Maske und Abfolge) anhand vorgegebener Standards und Ergonomieanforderungen** |  **2**  |   |   |   |
| Grundsätze der Dialoggestaltung verstehen | 2.1 | B1G:<br />Ich kann das Konzept der Gebrauchstauglichkeit (Usability) erklären (Effektivität, Effizienz, Zufriedenstellung). | B1F:<br />Ich kann die Interaktionsprinzipien einer bestehenden Benutzerschnittstelle erklären (z.B. nach ISO 9241-110). | B1E:<br />Ich kann Interaktionsprinzipien für eine geforderte Benutzerschnittstelle festlegen. |
| Benutzerschnittstelle entwerfen | 2.2 | B2G:<br />Ich kann einzelne Dialoge (Screens, z.B. als Wireframe) skizzieren. | B2F:<br />Ich kann eine Abfolge (Flow) von Dialogen (Screens) als Papierprototyp präsentieren. | B2E:<br />Ich kann eine Abfolge (Flow) einer Benutzerschnittstelle als Klickprototypen entwerfen. |
| Interaktionsprinzipien anwenden | 2.3 | B3G:<br />Ich kann verschiedene Interaktionselemente einer bestehenden Benutzerschnittstelle identifzieren (z.B. Menu, Navigation, Orientierung, Ordnung). | B3F:<br />Ich kann für eine Benutzerschnittstelle passende Interaktionselemente nach entsprechenden Konventionen auswählen und sinnvoll anordnen (z.B. semantischer Aufbau, Darstellung, Benennung, Struktur) | B3E:<br />Ich kann eine bestehende Benutzerschnittstelle in Bezug auf die Interaktionselemente analysieren und optimieren. |
| Eingabeformate kennzeichnen | 2.4 | B4G:<br />Ich kann Regeln zur Kennzeichnung von Pflichtfeldern nennen (z.B. Sternchen). | B4F:<br />Ich kann Regeln zur Kennzeichnung von erwarteten Eingabeformaten nennen (z.B. Datum, E-Mail, Telefon). | B4E:<br />Ich kann Pflichtfelder und erwartete Eingabeformate einer Benutzerschnittstelle selbsterklärend kennzeichnen. |
| Hilfe und Feedback integrieren | 2.5 | B5G:<br />Ich kann geeignete Hilfefunktionen für eine Benutzerschnittstelle identifizieren und vorschlagen. | B5F:<br />Ich kann geeignete Feedbackinformationen für eine Benutzerschnittstelle identifizieren und vorschlagen. | B5E:<br />Ich kann sämtliche Hilfefunktionen und Feedbackinformationen einer Benutzerschnittstelle abbilden. |
|  |  |  |  |  |
| Band C<br />**Implementiert eine Benutzerschnittstelle gemäss Entwurf und überprüft problematische Teile auf Machbarkeit** |  **3**  |   |   |   |
| Elemente einer Benutzerschnittstelle kennen und anwenden | 3.1 | C1G:<br />Ich kenne einfache Controls und Widgets und deren Eigenschaften für Benutzerschnittstellen und kann diese benennen und erklären. | C1F:<br />Ich kann verschiedene Controls und Widgets für ein Interaktionsziel auswählen und sinnvoll platzieren.  | C1E:<br />Ich kann komplexere und zusammengesetzte Interaktionselemente entwerfen und in eine logische Abfolge bringen. |
| Benutzerschnittstelle erstellen und testen | 3.2 | C2G:<br />Ich setze die entworfenen Skizzen in einen klickbaren Prototyp um. | C2F:<br />Ich kann ein Vorhaben mit einem klickbaren Prototyp testen und mögliche Probleme identifizieren | C2E:<br />Ich kann Probleme analysieren, Verbesserungsvorschläge anbringen und den Prototypen überarbeiten |
| Benutzerfreundlichkeit verbessern | 3.3 | C3G:<br/>Bezüglich der Benutzerfreundlichkeit erkenne ich schwierig umzusetzende Elemente | C3F:<br/>Ich kann Vorschläge zur Verbesserung Benutzerferundlichkeit unterbreiten | C3E:<br/>Ich kann aus einem Element bezüglich Benutzbarkeit eine bessere, einfachere und intuitive Lösung umsetzen (erarbeiten) |
|  |  |  |  |  |
| Band D<br />**Überprüft eine Benutzerschnittstelle auf Ergonomie** |  **4**  |   |   |   |
| Usability testen | 4.1 | D1G:<br />Ich kann einen Walktrough einer Benutzerschnittstellen-Abfolge mit jemandem durchführen | D1F:<br />Ich kann einen Usability-Test für eine Abfolge von Benutzerschnittstellen bezüglich Benutzerfreundlichkeit durchführen | D1E:<br />Ich kann einen Usability-Test für eine Abfolge von Benutzerschnittstellen bezüglich Benutzerfreundlichkeit und auch Business-Effizienz planen und durchführen |
| Usablility quantifizieren und verbessern | 4.2 | D2G:<br />Ich kann einen standardisierten Fragebogen (SUS, HEART) erklären.  | D2F:<br />Ich kann anhand des Fragebogens (SUS, HEART) eine Nachbefragung durchführen und auswerten. | D1E:<br />Ich kann Ergebnisse (Metrik) analysieren und Verbesserungsvorschläge anbringen. |
|  |  |  |  |  |
| Band E<br />**Implementiert eine Benutzerschnittstelle barrierefrei und überprüft sie** |  **5**  |   |   |   |
| Barrierefreiheit umsetzen | 5.1 | E1G:<br />Ich kann die Anforderungen an eine barrierefreie Benutzerschnittstelle nennen und kann erklären worauf es beim Entwurf einer Anwendung ankommt. | E1F:<br />Ich kann Elemente einer Benutzerschnittstelle barrierefrei umsetzen (Labels, Tab-Navigation, Kontrast, Alternativtext usw.). | E1E:<br />Ich kann geeignete Elemente für eine barrierefreie Benutzerschnittstelle vorschlagen. |
| Barrierefreiheit prüfen |5.2| E2G:<br />Ich kann die Möglichkeiten zur Überprüfung der Barrierefreiheit erläutern (z.B. Checkliste). | E2F:<br />Ich kann eine Benutzerschnittstelle gemäss Checkliste auf Barrierefreiheit prüfen. | E2E:<br />Ich kann die Barrierefreiheit einer Benutzerschnittstelle nachweisen. |



## Kompetenzstufen

### Grundlagen | Der Anfänger | Stufe 1 

Diese Stufe ist als Einstieg ins Thema gedacht. Der Fokus liegt hier auf dem Verstehen von Begriffen und Zusammenhängen. 

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 3.5.*

### Fortgeschritten | Der Kompetente | Stufe 2 

Diese Stufe definiert den Pflichtstoff, den alle Lernenden am Ende des Moduls möglichst beherrschen sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 4.75*

### Erweitert | Der Gewandete | Stufe 3 

Diese Lerninhalte für Lernende gedacht, die schneller vorankommen und einen zusätzlichen Lernanreiz erhalten sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 6*


