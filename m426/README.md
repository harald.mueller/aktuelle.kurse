# M426 Software mit agilen Methoden entwickeln

**Moduldefinition**
[.html](https://www.modulbaukasten.ch/module/426/1/de-DE?title=Software-mit-agilen-Methoden-entwickeln)
/ [.pdf](https://modulbaukasten.ch/Module/426_1_Software%20mit%20agilen%20Methoden%20entwickeln.pdf)

[scrumguides.org]( https://scrumguides.org )


[TOC]


# Konzept

Die Idee ist, dass die Lernenden das agile Verhalten und die agilen Techniken in einem Softwareentwicklungsprozess üben und selbständig praktisch durchführen. Die Klasse wird in Scrum-Teams eingeteilt und bearbeiten eine Aufgabe (Software) selbstorganisiert. In einem ersten Sprint (0) wird das Produkt in der "Vision" bestimmt und die ersten "User Stories" als Backlog erarbeitet. Am 3. Tag gibt es einen benoteten BasicCheck. Es werden in den 3 Sprints jeweils alle Meetings gemacht wo alle ihre Rollen ausprobieren, üben und trainieren können. Die weiteren Themen gemäss Modulidentifikation werden in Fachvorträgen erarbeitet und angehört. Ab Tag 4 beginnen die Fachvorträge (3-5 pro Modultag). Bewertet werden **BasicCheck**, **Fachvortrag** das **Verhalten** und der **Produktfortschritt** der das Team leistet.

    Tag 1 - Einführung, Basics
    Tag 2 - Grundlagen, Start Sprint 0 (Vorbereitungen zum Projekt)
    Tag 3 - BasicCheck
    Tag 4 - Start Sprint 1 (Entwicklungsstart) 
    Tag 5 - 
    Tag 6 - Start Sprint 2
    Tag 7 - 
    Tag 8 - Start Sprint 3
    Tag 9 -
    Tag 10 - Abschluss    

# Bewertung

**20% Basic-Check** Kleiner schriftlicher Test (20 min) am 3. Modul-Tag über die Grundbegriffe und Grundlagen.
<br>**30% Fachvortrag** Einzeln, 7-10 min., es stehen mehrere Themen zur Auswahl.
<br>**25% Regeleinhaltung/Team-Mitarbeit/-Beteiligung** (Beobachtungen der Lehrperson in Produkt, Projektabwicklung das Verhalten in den Meetings und bei der "Arbeit")
<br>**25% Produkt-Fortschritt, Ziel-Erreichung** Das "Begleitprodukt" kann frei gewählt werden. Es ist sowohl eine Neu- wie auch eine Weiterentwicklung möglich. Aktueller Stand und die Ziele müssen am 2. Tag zusamen mit der "Vision" bekannt gemacht werden.


# Modulablauf

## Themenplan (AP23b) Montag Vormittag

**[** Liste der Schülervorträge **](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/EcYPImyz0r9MvB1DQQlO6SIBIca50ernmHor8XaREu41Nw?e=VWsXOR)**

| ~Tag | Datum     | Thema        | Unterlagen |
| ---- | -----     | ----         |----        |
|   1  | Mo 11.11. |              | Begrüssung und Einführung ins Thema<br> - Vorgehensmodelle<br> - Das Agile Manifesto<br>**SCRUM. Was ist das, wie geht das?**<br> - Mini-Scrum-Spiel[*] mit drei 2-Minuten-Sprints in ca. 7-er Teams, plus Planung/Review/Retrospective<br> - Scrum-Team-Bildung (2-3 Teams zu 5-9 Personen)<br> - Projektwahl/Projektbestimmung<br> - Bestimmung Scrum Master & Product Owner<br> - Scrum meetings<br>**Offizieller Start Projekt & Sprint 0** |
|   2  | Mo 18.11. | **Sprint 0** | - Rep.: Was ist Scrum / www.scrum.org / Lückentext<br> - **Erarbeiten der Projektbasis (Vision, User-Stories, Backlog)**<br> - «Vision»-Erarbeitung im SCRUM-Team und Product Owner<br> - Product Backlog erstellen<br> - User-Stories entwickeln    |
|   3  | Mo 25.11. |              | **Basic Check** (einzeln, handschriftlich, «closed book», 20 min)<br> - «Vision», Product-Backlog und User-Stories überprüfen und evtl. korrigieren<br> - Was passiert in den Meetings?<br>3-4 Vorträge gem. Liste<br> - Produkt-Backlog und Sprint-Backlog für Sprint 1 erarbeiten<br> - Planung für Sprint 1         |
|   4  | Mo 02.12. | **Sprint 1** | - Was passiert in den Meetings? (Repetition)<br> - Scrum-Fragen (Repetition)<br>Im Scrumteam: Sprint 0 **Review**, Sprint 0 **Retrospektive** <br> - Sprint 1 **Planning I** (was) und **Planning II** (wie)<br>3-4 Vorträge gem. Liste<br>- Daily Scrum & Bearbeitung Sprint 1<br> - Estimating-Meeting (Planning-Poker) |
|   5  | Mo 09.12. |              | - Daily Scrum & Bearbeitung Sprint 1<br>3-4 Vorträge gem. Liste<br> - Abschluss Sprint 1, Review Sprint 1, Retrospektive Sprint 1 |
|   6  | Mo 16.12. | **Sprint 2**     | - Daily Scrum <br>- Backlog Sprint 2 erarbeiten, Planung Sprint 2, Beginn Sprint 2<br>3-4 Vorträge gem. Liste |
|   -  | -Ferien-  | ----         |            |
|   7  | Mo 06.01. |              | - Daily Scrum <br> 3-4 Vorträge gem. Liste  |
|   8  | Mo 13.01. | **Sprint 3** | - Daily Scrum <br> - Backlog Sprint 3 erarbeiten, Planung Sprint 3, Beginn Sprint 3 <br>3-4 Vorträge gem. Liste  |
|   9  | Mo 20.01. |              | - Daily Scrum <br> 3-4 Vorträge gem. Liste  |
|  10  | Mo 27.01. | Abschluss    | Letzter Vortrag<br> - Abschluss/Review Sprint 3 aller Teams im Plenum<br> - Produkt-Übergabe / -Abschluss<br>- Retrospektive Sprint 3 im Plenum <br> - Retrospektive SCRUM<br> - Retrospektive Modul<br>- Modulabschluss   |


[Details siehe Miro-Board ![zu miro](x-ressources/pfeil-zu-miroboard.jpg)](https://miro.com/app/board/uXjVPD6Lhvk=/?share_link_id=159004103816)


## 1.) Vorgehensmodelle

- [Übersicht und Grundsätzliches](1_Vorgehensmodelle)


## 2.) SCRUM als Vorgehensmodell

### 2.1 Tutorials

- [VM_10 (7.51min) Introduction to SCRUM in 7 Minutes](https://www.youtube.com/watch?v=9TycLR0TqFA)
- [VM_11 (5.51min) Agiles Projektmanagement mit SCRUM (Teil 1)](https://www.youtube.com/watch?v=7UMMq8WmRNw&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=46)
- [VM_12 (4.56min) Agiles Projektmanagement mit SCRUM (Teil 2)](https://www.youtube.com/watch?v=wq3GcgZGSas&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=47)
- [VM_21 (4.29min) Agiles oder Klassisches Projektmanagement (Teil 1)](https://www.youtube.com/watch?v=JsxOhZypTu8&index=42&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO)
- [VM_22 (3.18min) Agiles oder Klassisches Projektmanagement (Teil 2)](https://www.youtube.com/watch?v=JqBZZpbwqIw&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=43)
- [VM_23 (5.38min) Agiles oder Klassisches Projektmanagement (Teil 3)](https://www.youtube.com/watch?v=MQ4pSPkLmf0&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=44)
- [VM_31_Agile Vorgehensmodelle - vom Wasserfallmodell](https://blogs.itemis.com/de/scrum-kompakt-agile-vorgehensmodelle)
- [VM_31_Agile Vorgehensmodelle - zum Extreme-Programming](https://blogs.itemis.com/de/scrum-kompakt-extreme-programming-xp)
- [VM_32_Agile Vorgehensmodelle](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/Agile-Vorgehensmodelle)
- [VM_33_Vorgehensmodelle in der Softwareentwicklung und SCRUM](https://www.itemis.com/de/agile/scrum/kompakt/grundlagen-des-projektmanagements/vorgehensmodelle-in-der-softwareentwicklung)

### 2.2 Webs und Docs

- [scrumguides.org]( https://scrumguides.org )
- [Open Assessments](https://www.scrum.org/open-assessments)
- [M426_einige_Links_zu_klassischem_u_agilem_PM.pdf](2_Vorgehensmodell_Scrum/M426_einige_Links_zu_klassischem_u_agilem_PM.pdf), [.docx](2_Vorgehensmodell_Scrum/M426_einige_Links_zu_klassischem_u_agilem_PM.docx)
- [SCRUM Schulung & Zertifizierung](https://www.mitsm.de/scrum-schulung-zertifizierung)
- [Was ist neu im Scrum-Guide 2020](https://www.theprojectgroup.com/blog/scrum-guide-2020/)
- [F. Stein, lean-agility.de, Feb.2021, Die Grundlagen-Dokumente von Scrum (Update 2021)](https://www.lean-agility.de/2021/02/die-grundlagen-dokumente-von-scrum.html)
- [M. Lenz, experte.de, Feb.2021, Agiles Projektmanagement mit Scrum](https://www.experte.de/projektmanagement/scrum)


## 3.) How to Scrum

### 3.1 Allgemeines

- [Das agile Manifesto](https://agilemanifesto.org/iso/de/manifesto.html)
- [Der Scrum-Lebenszyklus](https://openpm.pm-camp.org/WikiExport/Scrum-Lebenszyklus_13107362.html)

### 3.2 Rollen, Team

- [Der/die ScrumMaster](3_HowToScrum/Scrummaster.pptx)
- [Der/die ProductOwner](./3_HowToScrum/productowner.pptx)
- [Wie ideale Teams funktionieren - Gemeinsam ans Ziel, PodCast 22:18 min, D, 2020-11-25](https://media.neuland.br.de/file/1810761/c/feed/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.mp3) <br> ---> [didaktische Fragen](3_HowToScrum/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.txt)
  
### 3.3 Handreichungen für den Start

- [GOagile_Checkliste_fuer_Meetings_01.pdf](3_HowToScrum/GOagile_Checkliste_fuer_Meetings_01.pdf)
- [10_The_Product_Vision_Board.pdf](3_HowToScrum/10_The_Product_Vision_Board.pdf)
- [Age-of-Product-Scrum-Anti-Patterns-Guide.pdf](3_HowToScrum/Age-of-Product-Scrum-Anti-Patterns-Guide-v38-2020-03-11.pdf)
- [Definitions of Done.pdf](3_HowToScrum/Definitions%20of%20Done.pdf)
- [Remote_Agile_Guide.pdf](3_HowToScrum/Remote_Agile_Guide.pdf)

### 3.4 Meetings

- [3_HowToScrum/Scrummeeting-Planning](3_HowToScrum/Scrummeeting-Planning)
- [3_HowToScrum/Scrummeeting-Daily](3_HowToScrum/Scrummeeting-Daily)
- [3_HowToScrum/Scrummeeting-Review](3_HowToScrum/Scrummeeting-Review)
- [3_HowToScrum/Scrummeeting-Retrospektive](3_HowToScrum/Scrummeeting-Retrospektive)

## 4.) Ausgewählte Themen

Hier einige Themen zur Vertiefung oder als Unterlagen für Vorträge. 
Die Liste und der Umfang der Themen ist nicht abschliessend. 
Weitere Vorschläge sind willkommen und werden laufend erweitert.

### 4.1 Teamarbeit

- [Wie Teams funktionieren - Gemeinsam ans Ziel (mp3-PodCast 22:18 min, D, 2020-11-25)](https://media.neuland.br.de/file/1810761/c/feed/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.mp3) 
<br> ---> [didaktische Fragen](4_Erweiterungen/Teamarbeit/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.txt)

### 4.2 Entwurfsmuster

- <https://de.wikipedia.org/wiki/Entwurfsmuster_(Buch)>
- <https://de.wikipedia.org/wiki/Entwurfsmuster>
- <http://www.oodesign.com>
- <http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Softwarearchitektur>
 
### 4.3 Kontinuierliche Integration

- <https://docs.gitlab.com/ee/ci/pipelines.html>
- <https://jenkins.io/>
- <https://github.com/marketplace/category/continuous-integration>

### 4.4 Kundenzufriedenheit

- <https://de.wikipedia.org/wiki/Kano-Modell>
- PDF zur Einführung: [Kano](4_Erweiterungen/Kundenzufriedenheit/Kano.pdf), Video & Beispiel von ["studyflix"](https://studyflix.de/wirtschaft/kano-modell-1177)
- PDF Vortrag 2003 [Kano and Progress Tracking](4_Erweiterungen/Kundenzufriedenheit/Kano%20and%20Progress%20Tracking.pdf), Anbieter: [ "microtool"](https://www.microtool.de/wissen-online/was-ist-das-kano-modell/), Blog/Artikel: ["Wow"-Faktoren](https://digitaleneuordnung.de/blog/kano-modell/)

### 4.5 Code Konventionen

- <http://checkstyle.sourceforge.net>
- <http://checkstyle.sourceforge.net/cmdline.html>
- <https://eslint.org/docs/user-guide/getting-started>
- <https://github.com/StyleCop/StyleCop>

### 4.6 Agile in a Nutshell - Posters

- <https://dandypeople.com/blog/enabling-business-agility-in-a-nutshell-free-infographic-poster>
- [Posters...](4_Erweiterungen/Agile%20in%20a%20Nutshell%20-%20Posters)


## 5.) Tools

- [Scrum-Tools in GitHub](https://github.com/marketplace/category/project-management)
- <https://github.com/hermes5/websolution/issues>
- <https://www.easyproject.com/de>
- <https://about.gitlab.com>
- <https://de.atlassian.com/software/jira>
- <https://plan.io/de/>
- <https://taiga.io>
- <https://www.targetprocess.com>
- <https://www.taskworld.com>
- <https://trello.com>
- <https://www.zoho.com/>
<br/>weitere ...




## 6.)



## 7.) Begleitprodukte

Damit die agilen Methoden, die in diesem Kurs trainiert werden können, sollte zumindest ein Projekt als sog. Begleitprodukt behandelt werden, sonst kannman ja die anstehenden Herausforderungen die in der Teamarbeit entstehen, gar nicht gesehen werden.
- Grundsätzlich sollen Teams zu 4-9 Personen **eigene Projekte** Weiterführen oder neu entwickeln. 
- Wenn kein Projekt gefunden werden kann, können auch [Beispiele der Schule](7_Begleitprodukte) angegangen werden.

## 8.) 



## 9.) Vorträge 

[**Liste der Schülervorträge**](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/EcYPImyz0r9MvB1DQQlO6SIB99NhQemwvZyjacfQcxj5MA?e=XBuMfX)

- [10 Dinge für eine gute Präsentation](https://wb-web.de/material/medien/10-dinge-die-sie-bei-prasentationen-dringend-beachten-sollten.html)
- [Wie halte ich einen Vortrag](https://www.psychologie.hu-berlin.de/de/prof/perdev/faq_entw_pers/WieHalteIchEinenVortrag)

	
Bewertungs-**FORM:** <https://forms.office.com/r/0UPx68U7jA>
Bewertungs-**Papier** [Vortragsbewertung_2023(korreliert-mit-Forms).pdf](9_Vortraege/Vortragsbewertung_2023(korreliert-mit-Forms).pdf), [xlsx](9_Vortraege/Vortragsbewertung_2023(korreliert-mit-Forms).xlsx)

Alternativen:
<br> [Vortragsbewertung-MSForms.pdf](./9_Vortraege/Vortragsbewertung-MSForms.pdf)
<br> [Vortragsbewertungraster.pdf](./9_Vortraege/Vortragsbewertungraster_DistanzOnline.pdf) 
    ([.docx](./9_Vortraege/Vortragsbewertungraster_DistanzOnline.docx))



_*Themen als bewerteten*_ **Schüler-Vortrag** (7-10 min)
- [01]  Was sind die Aufgaben und die notwendigen Eigenschaften eines ScrumMaster [SM in der Praxis](9_Vortraege/Vortrag_ScrumMaster%20&%20ProductOwner%20in%20der%20Praxis.txt)
- [02]  Was sind die Aufgaben und die notwendigen Eigenschaften eines ProductOwner [PO in der Praxis](9_Vortraege/Vortrag_ScrumMaster%20&%20ProductOwner%20in%20der%20Praxis.txt)v
- [03]  Was sind die Aufgaben und die notwendigen Eigenschaften eines Entwickerteam-Mitglieds
- [04]  Was sind die Rollen anderer [Stakeholder im Projekt](9_Vortraege/Vortrag_Stakeholder_im_Projekt.txt) und dessen Aufgaben (Sponsor/Geldgeber, Besteller/Auftraggeber, Steuerungsgremium, Benutzer-vertreter, Lieferanten, "mein" Vorgesetzter) 
- [05]  [Entwicklungsumgebungen](9_Vortraege/Vortrag_Entwicklungsumgebungen.txt) Marktübersicht, Marktleader, Kosten (Eclipse, IntelliJ, VisualStudio Code, VisualStudio, ..) 
- [06]  Entwicklungsumgebungen Funktionelle Unterschiede (Eclipse, IntelliJ, VisualStudio Code, VisualStudio, ..) 
- [07]  Übersicht über [Scrumtools](9_Vortraege/Vortrag_Scrumtools.txt). Was gibt es auf dem Markt? Funktionen, Marktleader, Kosten.
- [08]  [Scrumtools](9_Vortraege/Vortrag_Scrumtools.txt)-Funktionen in GitHub, in GitLab, in BitBucket. Unterschiede, Vor-/Nachteile
- [09]  Spezialisierte [Scrumtools](9_Vortraege/Vortrag_Scrumtools.txt). Funktionen, Marktübersicht, Marktleader, Kosten (Trello, Jira, Taiga, ..)
- [10]  Git vs. Subversion. Unterschiede, V-/N-teile
- [11]  Die gängigen und seltenen Git-Befehle und die Details wie sie (im Hintergrund) funktionieren [Git und die weiteren Funktionen](9_Vortraege/Vortrag_Versionsverwaltungstools%20Git%20(detaillierte%20Funktionsweise).txt)
- [12]  Spezielle Funktionen von Git-Befehlen wie Stage, Merge, Cherry-Picking, PullRequest, u.a.m.
- [13]  Übersicht über Git-Clients (-PlugIns, -APIs, -Addons, -Console/n, -Apps/Clients
- [14]  Übersicht über Versionsverwaltungs-Systeme/-Server (CVS, SVN, GIT) [Versionsverwaltungstools, Git vs. Subversion](9_Vortraege/Vortrag_Versionsverwaltungstools%20Git%20Vs.%20Subversion.txt)
- [15]  [Versionsverwaltungs-Clouds: Vergleich GitHub / GitLab / BitBucket](9_Vortraege/Vortrag_Versionsverwaltungstools%20GitHub,%20GitLab,%20BitBucket.txt). Marktübersicht und -leader, Vor-/Nachteile
- [16]  Cont.Integration/Autom.-Inst.-Tools. [CI/CD](9_Vortraege/Vortrag_Installations-Automatisierung%20und%20Continuous%20Integration%20Tools.txt). Übersicht was es gibt
- [17]  [CI/CD](9_Vortraege/Vortrag_Installations-Automatisierung%20und%20Continuous%20Integration%20Tools.txt). Konkrete Tools. Beispiel inkl. Live-Demo
- [18]  Testing-Tools. Grundsätzliches, was gibt es alles? (Selenium, WinRunner, ...)
- [19]  [Testingtools & automatisches Testen](9_Vortraege/Vortrag_Testing-Tools,%20Automatisches%20Testen.txt) & Tools. **Backend**/Server/Funktionen
- [20]  [Testingtools & automatisches Testen](9_Vortraege/Vortrag_Testing-Tools,%20Automatisches%20Testen.txt) & Tools. **Frontend**/Funktionen (Web-/Fatclient)
- [21]  Konzepte und Beispiele für [Wiederverwendung](9_Vortraege/Vortrag_Wiederverwendbarkeit_von_Code.txt)
- [22]  CleanCode (Regeln, Beispiele, Best Practices) [CC & Refactoring](9_Vortraege/Vortrag_Anwendung%20von%20CleanCode%20und%20Refactoring.txt) /  [Checkstyle](http://checkstyle.sourceforge.net)
- [23]   [Re-Engineering und Refactoring (Regeln, Beispiele, Best Practices), Wiederverwendbarkeit](9_Vortraege/Vortrag_Wiederverwendbarkeit_von_Code.txt)
- [24]  [ExtremeProgramming](9_Vortraege/Vortrag_ExtremeProgramming.txt)
- [25]  Übersicht über DesignPatterns
- [26]  [DesignPatterns](9_Vortraege/Vortrag_DesignPatterns.txt) (2-3 Beispiele theoretisch, konzeptionell und praktisch vorgeführt)
- [27]  Wie kann man eine [Scrum-Skalierung](9_Vortraege/Vortrag_Scrum-Skalierung.txt) machen
- [28]  Wie können [Scrum-Projekte zum Fixpreis](https://www.openpm.info/display/openPM/Projekte+mit+Scrum+zum+Festpreis) angeboten werden?


![3_HowToScrum/and-we-have-tripled-storypoints.jpg](3_HowToScrum/and-we-have-tripled-storypoints.jpg)

