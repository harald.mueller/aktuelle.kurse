## 1 Vorgehensmodelle

**1.1 Allgemeines, Übersicht**
- [Vorgehensmodelle in internationalen Projekten](https://www.gulp.ch/knowledge-base/rund-ums-projekt/standards-und-vorgehensmodelle-in-internationalen-projekten.html)
- [Vorgehensmodelle, deAcademic.com](http://deacademic.com/dic.nsf/dewiki/1475124)
- [Vorgehensmodelle, Enzyklopädie der Wirtschaftsinformatik de](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell)
- [Vorgehensmodelle, Sarre 2016](http://www.davit.de/uploads/media/Sarre-Vorgehensmodelle-v004.pdf)
- [Vorgehensmodelle und standardisierte Vorgehensweisen](http://www.techsphere.de/pageID=pm03.htm)
- [Vorgehensmodelle zum Softwareentwicklungsprozess](http://www.torsten-horn.de/techdocs/sw-dev-process.htm)
- [Wann welches Vorgehensmodell Sinn macht](http://www.elektronikpraxis.vogel.de/themen/embeddedsoftwareengineering/management/articles/273975)
- [SPEM Standard](1_Vorgehensmodelle/VM_00_Vorgehensmodelle/SPEM-Standard.pptx)
- [SPEM Standard OMG Document Number: formal/2008-04-01](1_Vorgehensmodelle/VM_00_Vorgehensmodelle/SPEM-Standard-formal-08-04-01.pdf)

**1.2 Aufgabenerledigungsstruktur IPERKA**
- [IPERKA 2014](1_Vorgehensmodelle/VM_01_IPERKA/IPERKA_1_4_D2014.pdf)
- [IPERKA](1_Vorgehensmodelle/VM_01_IPERKA/iperka.pdf)

**1.3 Phasenmodell "Wasserfall"**
- [Wasserfallmodell](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/Wasserfallmodell)

**1.4 Phasenmodell "V-Modell-XT"**
- [V-Modell-XT, Enzykolpädie](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/V-Modell-XT)
- [V-Modell-XT, Blogbeitrag](http://blog.pentadoc-gruppe.com/2011/07/16/vorgehensmodelle-im-pm-teil-3)

**1.5 Phasenmodell "HERMES"**
- [HERMES – der Schweizer Standard für IKT-Projekte im Überblick](1_Vorgehensmodelle/VM_04_Phasenmodell_HERMES/HERMES.pdf)
	- [hermes.zh.ch](http://hermes.zh.ch/)
	- [hermes.admin.ch](https://www.hermes.admin.ch/), [Methodenübersicht](https://www.hermes.admin.ch/de/projektmanagement/verstehen/ubersicht-hermes/methodenubersicht.html)
	- [it-weiterbildung/hermes_scrum](https://www.stadt-zuerich.ch/fd/de/index/informatik/bildungsstadt-albis/it_weiterbildung/hermes_scrum.html)

**1.6 Phasenmodell "RUP"**
- [Rational Unified Process (RUP)](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/Rational-Unified-Process-%28RUP%29)
- [RUP_notizen Dokument.pdf](1_Vorgehensmodelle/VM_05_Phasenmodell_RUP/RUP_notizen%20Dokument.pdf), [.docx](1_Vorgehensmodelle/VM_05_Phasenmodell_RUP/RUP_notizen%20Dokument.docx)

**1.7 Prototyping**
- [Vorgehensmodell Prototyping](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/Prototyping)

**1.8 Spiralmodell**
- [Spiralmodell](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/Spiralmodell)

**1.9 Agiles XP**
- [Extreme Programming XP](https://de.wikipedia.org/wiki/Extreme_Programming)
- [XP.pdf](1_Vorgehensmodelle/VM_08_Agile_XP/xp.pdf)

