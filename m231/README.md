# M231 Datenschutz und Datensicherheit anwenden

Inhaltsverzeichnis

[TOC]


[**Modulidentifikation** ICT CH](https://www.modulbaukasten.ch/module/231/1/de-DE?title=Datenschutz-und-Datensicherheit-anwenden)

[weitere TBZ Unterlagen -> https://gitlab.com/ch-tbz-it/Stud/m231](https://gitlab.com/ch-tbz-it/Stud/m231)


[Kompetenzmatrix](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/00_kompetenzband)

Buch zum Unterricht mit TBZ-Mailadresse: [Bücher vom HERDT-Verlag](https://herdt-campus.com/apply-token/grh_7xQuPjCbc_ty-WIlBuhAFC5Y_Ca5zi5ienS5pmdZc58h) zum Download<br>[Informationstechnologie Grundlagen (Stand 2021)](https://herdt-campus.com/product/ITECH_2021)
<br>Lokal: [Informationstechnologie Grundlagen (Stand 2021)](ITECH_2021.pdf)

## Leistungsbeurteilungen (Prüfungen)

[Leistungsbeurteilungen (2 Prüfungen & 1 Dossier/Lernjournal in GitLab)..](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/99_Leistungsbeurteilung)


- Inhalt der Leistungsbeurteilung 1 (30%)"**LB1**":<br>
(Der Gebrauch des Dossiers/Lernjournals und des Internets ist erlaubt)

	- Datenschutz und Datensicherheit, Datenschutzgesetz DSG (Grundsätze)
	- Git-Konzept (grobe Funktionsweise lokal und in der Cloud), Git-(Grund-)Befehle
	- Datensicherheit


- Inhalt der Leistungsbeurteilung 2 (30%)"**LB2**":<br>
<sup>_(Der Gebrauch des Dossiers/Lernjournals und des Internets kann erlaubt werden)_</sup>
	- Backup-Konzepte und Backup-Strategien
	- Passwörter, Autorisierung
	- Verschlüsselungen

- Dossier/Lernjournal in GitLab (40%)

[Tipps für das Dossier/Lernjournal..](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/06_Tipps?ref_type=heads#lernjournal)



## Ablaufplan 2024-Q2 (Di nachmittags)

|Tag  |AP24a     |Thema, Auftrag, Übung |
|---- |----      |----                  |
| 1   | Di 12.11.| Begrüssung ... <br>[Ablagesysteme](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/04_Ablagesysteme), -> [01 Wo speichere ich meine Daten](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/04_Ablagesysteme/01_Wo%20speichere%20ich%20meine%20Daten%20ab.md)|
| 2   | Di 19.11.| [Ablagesysteme](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/04_Ablagesysteme) -> [02 Eigenes Ablagekonzept](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/04_Ablagesysteme/02_Eigenes%20Ablagekonzept.md) ,<br>[Git und Markdown](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/10_Git#einf%C3%BChrung-in-git-und-markdown) - [Eigene GIT Umgebung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/10_Git/03_Eigene%20GIT%20Umgebung.md) - [Markdown-Guide](https://www.markdownguide.org/basic-syntax/)  |
| 3   | Di 26.11.|  *"Datenschutz"* [Schutzwürdigkeit von Daten im Internet](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/01_Datenschutz)<br> &#9997; Teams-Auftrag M231-3a:<br> - [Datenschutzrecht seit 1.9.2023](https://www.admin.ch/gov/de/start/dokumentation/medienmitteilungen.msg-id-90134.html), &#9997; [Auftrag "der EDÖP"](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/01_Datenschutz/01_Leseauftrag.md)<br> - [Datenschutzgesetz DSG](https://www.fedlex.admin.ch/eli/fga/2020/1998/de) und [Datenschutzverordnung DVO](https://www.newsd.admin.ch/newsd/message/attachments/75620.pdf),<br> &#x1F39E; [SRF-Video zum Thema Datenschutz (14 min)](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/01_Datenschutz/09_Video%20SRF.md) <br> &#x1F9FB; [Videoueberwachung eingeschränkt](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/01_Datenschutz/media/2023-11-01_TA_Links-Guen-schraenkt-Videoueberwachung-stark-ein.pdf), [Bewilligungspflicht für Kameras in Zürich](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/01_Datenschutz/media/2024-12-11_TA_Bewilligungspflicht-fuer-Kameras-in-Zuerich.pdf) |
| 4   | Di 03.12.| *"Datensicherheit der eigenen Infrastruktur"*<br> **Kein [Backup](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/05_Backup#backup-), kein Mitleid**  <br> 01 &#9997; Backup-[Leseauftrag](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/01_Leseauftrag.md) (45 min) <br> Rest der Zeit: Prüfungsvorbereitung und das Dossier/Lernjournal verschönern  |
| 5   | Di 10.12.| **LB1** (30 min)<br> 02 &#9997; [Begriffe klären](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/02_Begriffe%20Kl%C3%A4ren.md) in Gruppen zu 4 Pers (30 min)  <br> 03 &#9997; [Backup-Konzept erstellen](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/03_Backupkonzept.md) (fürs Smartphone, für den Notebook)  <br> 04 &#9997; [Windows Backup](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/04_Windows%20Backup.md) <br> 05 &#9997; [Cloudbackup - aber sicher](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/05_Sicheres%20Cloud-Backup.md) |
| 6   | Di 17.12.| Weiterarbeit am "Backup" <br>(Konzept und Umsetzung) <br> &#9997; Auftrag: **Bereitstellung und Durchführung des Backups für den Notebook/Laptop** |
| - - | - - -    | Weihnachtsferien |
| 7   | Di 07.01.| 06 &#9997; [Backup prüfen](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/06_Backup%20Termin.md) <br><br> *"_Schutzwürdigkeit von Daten im Internet_"*<br>Zugangsarten/-systeme und [Passwörter](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/03_Passw%C3%B6rter),<br>[Warum Passwörter](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/02_Diskussion%20Passw%C3%B6rter.md), [Authentifizierung und Autorisierung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/03_Authentifizierung%20und%20Autorisierung.md), [Was ist Multi-Faktor-Authentisierung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/05_MFA.md), <br>&#9997; [Passwörterverwaltung, Passwortmanager](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/06_Passwortmanager.md) einrichten   |
| 8   | Di 14.01.| Einstieg in die Datenverschlüsselung<br>[Challange - Eigene Verschlüsselung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/02_Verschl%C3%BCsselung/00_Challenge.md),<br> [FIDO - Anmeldeverfahren ohne Passwörter](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/03_Passw%C3%B6rter/fido-anmeldeverfahren)<br> [Datenverschlüsselung](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/02_Verschl%C3%BCsselung) (-> [Cäsar/Vigenère-Verschlüsselung / CrypTool](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/02_Verschl%C3%BCsselung/01_Caesar.md))   |
| 9   | Di 21.01.| **LB2** (45 min)<br>*"Konsequenzen von Fehlern im Datenschutz und bei der Datensicherheit"*<br>[Problematik der Datenloeschung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/06_BackupProbleme-und-Juristisches/problematik-datenloeschung.md) sowie [Impressum, Discaimer, AGB](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/06_BackupProbleme-und-Juristisches/impressum-disclaimer-agb.md) |
| 10  | Di 28.01.| [Lizenzmodelle](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/07_Lizenzmodelle)<br>Anwendungen auf Datenschutzkonformität überprüfen <br>Abgabe **LB3** |

----

## Ablaufplan 2024-Q2 (Do nachmittags)

|Tag  |AP24c     |Thema, Auftrag, Übung |
|---- |----      |----                  |
| 1   | Do 14.11.| Begrüssung ... <br>[Ablagesysteme](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/04_Ablagesysteme), -> [01 Wo speichere ich meine Daten](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/04_Ablagesysteme/01_Wo%20speichere%20ich%20meine%20Daten%20ab.md)|
| 2   | Do 21.11.| [Ablagesysteme](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/04_Ablagesysteme) -> [02 Eigenes Ablagekonzept](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/04_Ablagesysteme/02_Eigenes%20Ablagekonzept.md) ,<br>[Git und Markdown](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/10_Git#einf%C3%BChrung-in-git-und-markdown) - [Eigene GIT Umgebung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/10_Git/03_Eigene%20GIT%20Umgebung.md) - [Markdown-Guide](https://www.markdownguide.org/basic-syntax/)  |
| - - | -- 29.11.| Lehrerkonvent LKB |
| 3   | Do 05.12.| *"Datenschutz"* [Schutzwürdigkeit von Daten im Internet](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/01_Datenschutz)<br> &#9997; Teams-Auftrag M231-3a:<br> - [Datenschutzrecht seit 1.9.2023](https://www.admin.ch/gov/de/start/dokumentation/medienmitteilungen.msg-id-90134.html), &#9997; [Auftrag "der EDÖP"](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/01_Datenschutz/01_Leseauftrag.md)<br> - [Datenschutzgesetz DSG](https://www.fedlex.admin.ch/eli/fga/2020/1998/de) und [Datenschutzverordnung DVO](https://www.newsd.admin.ch/newsd/message/attachments/75620.pdf),<br> -- [SRF-Video zum Thema Datenschutz (14 min)](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/01_Datenschutz/09_Video%20SRF.md) <br> -- [Cyber-Game](./x_ressources/cyber-game.jpg), ([.pdf](./x_ressources/Cyber-Game-Spielanleitung.pdf))  |
| 4   | Do 12.12.| *"Datensicherheit der eigenen Infrastruktur"*<br> **Kein [Backup](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/05_Backup#backup-), kein Mitleid**  <br> 01 &#9997; Backup-[Leseauftrag](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/01_Leseauftrag.md) (45 min) <br> 02 &#9997; [Begriffe klären](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/02_Begriffe%20Kl%C3%A4ren.md) in Gruppen zu 4 Pers (30 min)  <br> 03 &#9997; [Backup-Konzept erstellen](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/03_Backupkonzept.md) (fürs Smartphone, für den Notebook)<br> Rest der Zeit: Prüfungsvorbereitung und das Dossier/Lernjournal verschönern  |
| 5   | Do 19.12.| **LB1** (30 min)  <br> 04 &#9997; [Windows Backup](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/04_Windows%20Backup.md) <br> 05 &#9997; [Cloudbackup - aber sicher](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/05_Sicheres%20Cloud-Backup.md) <br> Weiterarbeit am "Backup" <br>(Konzept und Umsetzung) <br> &#9997; Auftrag: **Bereitstellung und Durchführung des Backups für den Notebook/Laptop**  |
| - - | - - -    | Weihnachtsferien |
| 6   | Di 09.01.| 06 &#9997; [Backup prüfen](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/05_Backup/06_Backup%20Termin.md) <br><br> *"_Schutzwürdigkeit von Daten im Internet_"*<br>Zugangsarten/-systeme und [Passwörter](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/03_Passw%C3%B6rter),<br>[Warum Passwörter](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/02_Diskussion%20Passw%C3%B6rter.md), [Authentifizierung und Autorisierung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/03_Authentifizierung%20und%20Autorisierung.md), [Was ist Multi-Faktor-Authentisierung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/05_MFA.md), <br>&#9997; [Passwörterverwaltung, Passwortmanager](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/06_Passwortmanager.md) einrichten   |
| 7   | Di 16.01.| Einstieg in die Datenverschlüsselung<br>[Challange - Eigene Verschlüsselung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/02_Verschl%C3%BCsselung/00_Challenge.md),<br> [FIDO - Anmeldeverfahren ohne Passwörter](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/03_Passw%C3%B6rter/fido-anmeldeverfahren)<br> [Datenverschlüsselung](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/02_Verschl%C3%BCsselung) (-> [Cäsar/Vigenère-Verschlüsselung / CrypTool](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/02_Verschl%C3%BCsselung/01_Caesar.md))   |
| 8   | Di 23.01.| **LB2** (45 min)<br>*"Konsequenzen von Fehlern im Datenschutz und bei der Datensicherheit"*<br>[Problematik der Datenloeschung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/06_BackupProbleme-und-Juristisches/problematik-datenloeschung.md) sowie [Impressum, Discaimer, AGB](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/06_BackupProbleme-und-Juristisches/impressum-disclaimer-agb.md) |
| 9   | Di 30.01.| [Lizenzmodelle](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/07_Lizenzmodelle)<br>Anwendungen auf Datenschutzkonformität überprüfen <br>Abgabe **LB3** |





## Empfehlung

Abonnieren und hören Sie ab jetzt! den wöchentlichen 
Podcast (Audio und/oder Video) von "c't uplink", 
denn da werden Sie immer mit den neuesten Trends 
und Empfehlungen zu Informatik-Themen professionell 
und in deutscher Sprache bedient<br>
https://www.heise.de/thema/ct-uplink


@ Harald Müller, Aug/Okt/Dez 2024

