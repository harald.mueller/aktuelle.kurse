# M323 Funktional Programmieren

Inhaltsverzeichnis

[TOC]


[**Modulidentifikation** ICT CH](https://www.modulbaukasten.ch/module/323/1/de-DE?title=Funktional-Programmieren)

[weitere TBZ Unterlagen -> https://gitlab.com/ch-tbz-it/Stud/m323/m323](https://gitlab.com/ch-tbz-it/Stud/m323/m323)





## Leistungsbeurteilungen (Prüfungen)

LB (100%) [Miniprojekt (-> Definition)](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/08_MiniProjekt)  (7 Std plus Präsentation desselben)

## Ablaufplan 2024-Q2 (Mo morgens)


|Tag  |AP22a     |Thema, Auftrag, Übung |
|---- |----      |----                  |
| 1   | Mo 17.02 | [Einführung](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/01_Einf%C3%BChrung) [.pptx](https://gitlab.com/ch-tbz-it/TE/m323/m323/-/raw/main/TheoriePr%C3%A4sentationen/Einf%C3%BChrungFunktionaleProgrammierung.pptx) <br> - Was sind Funktionen und Vergleich zu imperativem Code <br> - Paradigmenwechsel in der Programmierung <br> - Wieso sind Funktionen nützlich  <br> &#128196; [Unterlagen](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/blob/main/01_Einf%C3%BChrung/einf%C3%BChrung.md), &#128196; [Fachbegriffe](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/blob/main/01_Einf%C3%BChrung/Fachbegriffe.md), <br> &#9997; [Aufgaben](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/blob/main/01_Einf%C3%BChrung/Aufgaben)                  |
| 2   | Mo 24.02 | [Anforderungen](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/02_Anforderungen) <br> - Wie setzen wir das WAS statt das WIE um <br> &#9997; [Aus deklarativen Anforderungen werden Funktionen abgeleitet](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/02_Anforderungen/Aufgaben) <br> &#127879; [Lösung TravelPlanner](https://gitlab.com/ch-tbz-it/TE/m323/m323/-/blob/main/Unterlagen/Loesungen_02_Anforderungen/TravelPlanner.scala), [Lösung WordScorerApp](https://gitlab.com/ch-tbz-it/TE/m323/m323/-/blob/main/Unterlagen/Loesungen_02_Anforderungen/WordScorerApp.scala)  |
| 3   | Mo 03.03 | [Reine Funktionen](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/03_PureFunctions) <br> - Was zeichnen 'pure functions' aus? <br> - Begriff Lambda-Funktionen  <br> &#128196; [Immutable Values](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/03_PureFunctions/Immutable_Values) (Datenkopien und Rekursionen) <br> &#9997; [Aufgaben](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/03_PureFunctions/Aufgaben) <br> &#127879; [Lösung PureFunctions](https://gitlab.com/ch-tbz-it/TE/m323/m323/-/tree/main/Unterlagen/Loesungen_03_PureFunctions) |
| 4   | Mo 10.03 | [Funktionen als Werte](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/04_FunctionsAsValues) [.pptx](https://gitlab.com/ch-tbz-it/TE/m323/m323/-/raw/main/TheoriePr%C3%A4sentationen/FunctionsAsValues_16x9.pptx) <br> - No side effects <br> - Mit Rekursionen arbeiten <br> &#127879; [Lösung FunctionsAsValues](https://gitlab.com/ch-tbz-it/TE/m323/m323/-/tree/main/Unterlagen/Loesungen_03_PureFunctions) |
| 5   | Mo 17.03 | [Weitere Datenstrukturen](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/05_WeitereDatenstrukturen) <br> - Filter-map-reduce <br> - Higher Order Functions <br> - Mit Tuples arbeiten <br> &#127879; [Lösungen WeitereDatenstrukturen](https://gitlab.com/ch-tbz-it/TE/m323/m323/-/tree/main/Unterlagen/Loesungen_05_WeitereDatenstrukturen) |
| 6   | Mo 24.03 | [Weitere Algotithmen](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/06_WeitereAlgorithmen) <br> - Algorithmen fürs Suchen  <br> - Parallel-Prozesse <br> - Pipelines |
| 7   | Mo 31.03 | [Fehlerbehandlung](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/07_ErrorHandling) <br> <br> **Funktionale Programme entwerfen**                    |
| 8   | Mo 07.04 | [Miniprojekt (--> Definition)](https://gitlab.com/ch-tbz-it/Stud/m323/m323/-/tree/main/08_MiniProjekt) (zu zweit)                   |
| 9   | Mo 14.04 | Miniprojekt (Fortsetzung)     |
| -   | --       | Ostermontag und Osterferien   |
| 10  | Mo 05.05 | [Abgabe Miniprojekt gemäss Terminliste](https://tbzedu-my.sharepoint.com/:x:/g/personal/harald_mueller_tbz_ch/ERl0LOwka2ZDgk9YQj4oxrgBcrUfujx-A9ZnFx93FdTsqg?e=84CPwV) )   |

&copy; Feb 25, MUH